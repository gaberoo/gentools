#include "hts_defs.h"

int idx(char a) {
  switch (a) {
    case 'a': case 'A': return 0;
    case 'g': case 'G': return 1;
    case 't': case 'T': return 2;
    case 'c': case 'C': return 3;
    case 'n': case 'N': case '.': case '-': return 4;
    default: return -1;
  }
}

int char2int(char a) {
  switch (a) {
    case 'a': case 'A': return HTS_A;
    case 'g': case 'G': return HTS_G;
    case 't': case 'T': return HTS_T;
    case 'c': case 'C': return HTS_C;
    case 'r': case 'R': return HTS_R;
    case 'y': case 'Y': return HTS_Y;
    case 'k': case 'K': return HTS_K;
    case 'm': case 'M': return HTS_M;
    case 's': case 'S': return HTS_S;
    case 'w': case 'W': return HTS_W;
    case 'b': case 'B': return HTS_B;
    case 'd': case 'D': return HTS_D;
    case 'h': case 'H': return HTS_H;
    case 'v': case 'V': return HTS_V;
    case '-': return HTS_GAP;
    case '0': return HTS_ZERO;
    case '1': return HTS_ONE;
    default: return HTS_NONE;
  }
}

char int2char(int a) {
  switch (a) {
    case HTS_A: return 'A';
    case HTS_G: return 'G';
    case HTS_T: return 'T';
    case HTS_C: return 'C';
    case HTS_R: return 'R';
    case HTS_Y: return 'Y';
    case HTS_K: return 'K';
    case HTS_M: return 'M';
    case HTS_S: return 'S';
    case HTS_W: return 'W';
    case HTS_B: return 'B';
    case HTS_D: return 'D';
    case HTS_H: return 'H';
    case HTS_V: return 'V';
    case HTS_GAP: return '-';
    case HTS_ANY: return 'N';
    case HTS_ZERO: return '0';
    case HTS_ONE: return '1';
    default: return 'n';
  }
}

char compChar(char a) {
  switch (toupper(a)) {
    case 'A': return 'T';
    case 'T': return 'A';
    case 'G': return 'C';
    case 'C': return 'G';
    case '0': return '1';
    case '1': return '0';
    default: return a;
  }
}


