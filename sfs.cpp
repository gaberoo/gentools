#include <iostream>
#include <fstream>
#include <map>
using namespace std;

#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "cxxopts.hpp"
#include "duo.h"

cxxopts::Options options("sfs", "Estimate SFS");

uint32_t total = 0;
double tinv = 0.0;
map<uint32_t,uint16_t> db; // entries
map<uint32_t,double> ldb;  // log entries
map<uint32_t,double> lbc;  // binomial coefficients

typedef map<uint32_t,uint16_t>::const_iterator It;
typedef map<uint32_t,double>::const_iterator Lt;

void read_duo_names(istream& in) {
  string name;
  uint32_t z;
  uint16_t c;
  while (in >> name >> z >> c) {
    db[z] = c;
    ldb[z] = log(1.0*c);
    lbc[z] = gsl_sf_lnchoose(duo1(z),duo2(z));
    total += c;
  }
  tinv = 1.0/total;
}

void read_duo(istream& in) {
  uint32_t z;
  uint16_t c;
  while (in >> z >> c) {
    db[z] = c;
    ldb[z] = log(1.0*c);
    lbc[z] = gsl_sf_lnchoose(duo1(z),duo2(z));
    total += c;
  }
  tinv = 1.0/total;
}

double log_likelihood(double alpha, double beta) {
  if (alpha < 0.0 || beta < 0.0) return GSL_NEGINF;
  else {
    double loglik = 0.0;
    It c;
    uint16_t ni, ki;
    for (c = db.begin(); c != db.end(); ++c) {
      ni = duo1(c->first);
      ki = duo2(c->first);
      loglik += c->second*gsl_sf_lnbeta(alpha+ki,beta+ni-ki);
    }
    loglik -= total*gsl_sf_lnbeta(alpha,beta);
    return loglik;
  }
}

int main(int argc, char** argv) {
  string filename = "-";
  int nit = 10000;
  int thin = 100;
  double step = 0.1;
  size_t min_cov = 3;
  size_t max_cov = 1000000;

  options.add_options()
    ("f,filename",   "Input filename", cxxopts::value<string>(filename))
    ("i,nit",   "Number of iterations", cxxopts::value<int>(nit))
    ("t,thin",  "Thinning", cxxopts::value<int>(thin))
    ("s,step",  "Step size", cxxopts::value<double>(step))
    ("v,verbose", "Be verbose")
    ("nonames", "Input file doesn't have a name column")
    ("dumb",    "Dumb estimate")
    ("m,minCov", "Minimum coverage", cxxopts::value<size_t>(min_cov))
    ("M,maxCov", "Maximum coverage", cxxopts::value<size_t>(max_cov))
  ;

  vector<string> _pos = { "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  /**************************************************************************/

  istream* in = NULL;
  if (filename == "-" || filename == "") {
    in = &cin;
  } else {
    in = new ifstream(filename.c_str());
  }

  if (options.count("nonames")) read_duo(*in);
  else read_duo_names(*in);

  if (options.count("dumb")) {
    int ntot = 0;
    int ktot = 0;
    int cnt = 0;

    int singles = 0;  /* Number of sites with singletons */
    double f = 0.0;
    double H = 0.0;
    double Shannon = 0.0;
    int S = 0;        /* total number of SNP sites */
    int G = 0;        /* total number of sites */
    int TwoThirds = 0;

    uint16_t ni, ki;
    double pi;

    for (It c = db.begin(); c != db.end(); ++c) {
      if (options.count("verbose")) {
        cout << duo1(c->first) << " " << duo2(c->first) << " " << c->second << endl;
      }
      ni = duo1(c->first);
      if (ni >= min_cov && ni <= max_cov) {
        G += c->second;

        ki = duo2(c->first);
        pi = 1.*ki/ni;

        ntot += c->second*ni;
        ktot += c->second*ki;

        if (ni-ki == 1) singles += c->second;

        if (pi <= 2./3.) TwoThirds += c->second;

        if (ni > ki) {
          S += c->second;
          f += c->second*(1.0-gsl_pow_int(0.001,ni-ki));
          H += c->second*(1.0-(gsl_pow_2(pi)+gsl_pow_2(1.-pi)));
          Shannon -= c->second*(pi*log(pi)+(1.-pi)*log(1.-pi));
        }

        cnt += c->second;
      }
    }

    cout << ntot << " " << ktot << " " 
         << 1.0*ktot/ntot << " "
         << 1.0*(ktot+singles*0.001)/ntot << " "
         << f/cnt << " " 
         << 1.0-H/cnt << " "
         << G << " "
         << S << " " 
         << cnt << " " 
         << singles << " "
         << Shannon/cnt << " " 
         << 1.0*TwoThirds/cnt << endl;
    return 0;
  }

  gsl_rng* rng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(rng, time(NULL));

  // run Metropolis-Hastings
  double a = 1.0, b = 1.0;
  double lik = log_likelihood(a,b);

  double a2, b2;
  double lik2;

  double r;
  double alpha;

  for (int i = 0; i < nit; ++i) {
    a2 = gsl_ran_gaussian(rng,step) + a;
    b2 = gsl_ran_gaussian(rng,step) + b;
    lik2 = log_likelihood(a2,b2);
    alpha = lik2-lik;

    if (alpha > 0.0) {
      a = a2;
      b = b2;
      lik = lik2;
    } else {
      r = gsl_rng_uniform(rng);
      if (log(r) < alpha) {
        a = a2;
        b = b2;
        lik = lik2;
      }
    }

    if (i % thin == 0) {
      cout << i << " " << a << " " << b << " " << lik << endl;
    }
  }

  gsl_rng_free(rng);

  return 0;
}

