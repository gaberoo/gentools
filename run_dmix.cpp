#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
using namespace std;

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

void dirichlet_gibbs(int k, int n, int p, const unsigned* X,
                     const double* a, const double* b,
                     gsl_rng* rng, size_t maxit, size_t thin,
                     string prefix = "");

void make_random(int n, int p, unsigned* X, int N, gsl_rng* rng) {
  double theta_one[] = { 0.5, 0.3, 0.1, 0.05, 0.05 };
  double theta_two[] = { 0.1, 0.4, 0.1, 0.1, 0.2 };

  for (int l = 0; l < p/2; ++l) {
    gsl_ran_multinomial(rng,n,N,theta_one,X+l*n);
  }
  for (int l = p/2; l < p; ++l) {
    gsl_ran_multinomial(rng,n,N,theta_two,X+l*n);
  }
}

void read_data_matrix(int n, int p, string file, unsigned* X) {
  ifstream in(file.c_str());
  for (int m = 0; m < n; ++m) {
    for (int l = 0; l < p; ++l) {
      in >> X[l*n+m];
    }
  }
  in.close();
}

int main(int argc, char* argv[]) {

  if (argc < 6) {
    cerr << "Not enough arguments supplied!" << endl;
  }

  ostringstream pref;
  pref << "dmix_" << time(NULL);
  string prefix = pref.str();

  int k = (argc > 1) ? atoi(argv[1]) : 2;
  if (argc > 2) prefix = argv[2];
  int n = atoi(argv[3]);
  int p = atoi(argv[4]);
  string in_file = argv[5];
  int thin = (argc > 6) ? atoi(argv[6]) : 0;
 
  gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,time(NULL));

  unsigned* X = new unsigned[n*p];

  // make_random(n,p,X,10000,rng);
  read_data_matrix(n,p,in_file,X);

  double* a = new double[n];
  double* b = new double[k];

  for (int i = 0; i < k; ++i) b[i] = 1.0;
  for (int m = 0; m < n; ++m) a[m] = 100.0;

  int maxit = 1e6;
  if (thin == 0) thin = maxit/1000;

  dirichlet_gibbs(k,n,p,X,a,b,rng,maxit,thin,prefix);

  delete[] X;
  delete[] a;
  delete[] b;

  gsl_rng_free(rng);

  return 0;
}
