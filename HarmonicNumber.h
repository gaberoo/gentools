
class HarmonicNumber {
  public:
    double operator[](size_t i);

  protected:
    void expand(size_t i);
    vector<double> a = { 0.0, 1.0 };

};

double HarmonicNumber::operator[](size_t i) {
  if (i < a.size()) return a[i];
  expand(i);
  return a[i];
}

void HarmonicNumber::expand(size_t i) {
  while (a.size() <= i) {
    a.push_back(a.back() + 1.0/a.size());
  }
}

