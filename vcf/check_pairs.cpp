#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

#include "../gff/GFFFile.h"

int check_pairs(htsFile* bcf1, 
                bcf_hdr_t* header, 
                bcf_srs_t* sr, 
                bcf1_t* record, 
                const vector<string>& sample_names, 
                const cxxopts::Options& options) 
{
  int chrBegin = options["begin"].as<int>();
  int chrEnd = options["end"].as<int>();
  int verbose = options.count("verbose");

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);

  vector<size_t> cnt(nsamp,0);
  vector<size_t> identical(nsamp,0);
 
  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;

  if (verbose > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  while (record->pos < chrBegin) {
    if (verbose) cerr << "Searching..." << record->pos << "\r";
    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }
  if (verbose) cerr << endl;

  int idx;

  while (ret == 0) {
    if (chrEnd > 0 && record->pos >= chrEnd) break;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    for (int i = 0; i < nsamp; ++i) {
      int max_n = 0;
      int tot_n = 0;
      for (int a = 0; a < record->n_allele; ++a) {
        idx = i*(record->n_allele) + a;
        tot_n += ad[idx];
        if (ad[idx] > max_n) max_n = ad[idx];
      }

      int minor = tot_n - max_n;
      if (minor == 2) {
        cnt[i]++;

        int is_two = 0;
        for (int a = 0; a < record->n_allele; ++a) {
          idx = i*(record->n_allele) + a;
          if (ad[idx] == 2) {
            is_two = 1;
            break;
          }
        }
        if (is_two) identical[i]++;
      }
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  for (int i = 0; i < nsamp; ++i) {
    cout << sample_names[i] << "\t" << cnt[i] << "\t" << identical[i] << endl;
  }

  return 0;
}

