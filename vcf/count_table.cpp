#include "../hts.h"
#include "../vcf_stats.h"

#include <sstream>
#include <algorithm>
#include <numeric>
using namespace std;

#include <gsl/gsl_math.h>
#include <gsl/gsl_statistics_int.h>
#include <gsl/gsl_rng.h>

extern "C" {
  void fexact_(int* nrow, int* ncol, double* table, int* ldtab, 
               double* expect, double* percnt, double* emin, 
               double* prt, double* pre);

  void rcont2(int *nrow, int *ncol, int *nrowt, int *ncolt, int *ntotal,
              double *fact, int *jwork, int *matrix, double (*unif_rand)());

  double expect = 5.0;
  double percent = 80;
  double emin = 1;
  double fexact_B = 99999;

  gsl_rng* rng = NULL;
  double unif_rand() { return gsl_rng_uniform(rng); };
}

int count_table(htsFile* bcf1, 
                bcf_hdr_t* header, 
                bcf_srs_t* sr, 
                bcf1_t* record, 
                const vector<string>& sample_names, 
                const cxxopts::Options& options)
{
  int verbose = options.count("verbose");
  string filename = options["count-table-groups"].as<string>();
  int count_all = options.count("count-table-all");

  int nsamp = bcf_hdr_nsamples(header);
  int ret = -1;

  if (verbose > 1) cout << "Fetching first record." << endl;
  if (sr == NULL) ret = bcf_read(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);
  if (verbose > 1) cout << "Fetched first record." << endl;

  // read groups from file
  if (verbose) cerr << "Reading groups..." << flush;
  vector<string> groups(nsamp,"");
  string next_name;
  string next_grp;
  ifstream in(filename);
  string line;
  while (getline(in,line)) {
    istringstream sin(line);
    sin >> next_name >> next_grp;
    auto it = find(sample_names.begin(),sample_names.end(),next_name);
    int p = distance(sample_names.begin(),it);
    if (p < nsamp) groups[p] = next_grp;
  }
  in.close();
  if (verbose) cerr << "done." << endl;

  // find unique groups and sort
  if (verbose) cerr << "Finding unique groups..." << flush;
  vector<string> uniq_grps(groups);
  sort(uniq_grps.begin(),uniq_grps.end());
  auto uniq_end = unique(uniq_grps.begin(),uniq_grps.end());
  uniq_end = remove(uniq_grps.begin(),uniq_end,"");
  uniq_grps.resize(distance(uniq_grps.begin(),uniq_end));
  if (verbose) cerr << "done." << endl;

  // map group names to indices
  int gidx = 0;
  map<string,int> grp_idx;
  for (auto g: uniq_grps) grp_idx[g] = gidx++;

  if (verbose) {
    cerr << "Found the following groups:" << endl;
    for (auto g: uniq_grps) cerr << "  " << g << endl;
  }

  int ncol = uniq_grps.size();

  vector<double> fact(nsamp+1,0.0);
  fact[0] = fact[1] = 0.0;
  for (size_t i = 2; i < fact.size(); i++) fact[i] = fact[i-1] + log(i);

  if (rng == NULL) rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,time(NULL));

  while (ret == 0) {
    int stype = bcf_get_variant_types(record);

    if (verbose) fprintf(stderr,"%s %d %d\r",bcf_hdr_id2name(header,record->rid),record->pos,stype);
    if (verbose > 1) fprintf(stderr,"\n");

    // only continue if its a SNP
    if (stype) {
      int32_t* gt_arr = NULL;
      int32_t ngt_arr = 0;
      int ngt = bcf_get_genotypes(header,record,&gt_arr,&ngt_arr);

      if (ngt <= 0) {
        cerr << "[" << record->pos << "] Error fetching genotypes!" << endl;
        break;
      }

      int nrow = 0;
      vector<int> gt_map(record->n_allele,-1);
      if (verbose > 1) cerr << "Counting unique genotypes..." << flush;
      for (int i = 0; i < nsamp; ++i) {
        if (groups[i] == "") continue;
        int idx = bcf_gt_allele(gt_arr[i]);
        if (idx >= 0 && gt_map[idx] < 0) gt_map[idx] = nrow++;
      }

      // cerr << record->pos << " " << nrow << endl;
      // if (nrow == 0) { cerr << "  fake SNP" << endl; }

      vector<double> grp_cnt(nrow*ncol,0.0);

      if (verbose > 1) cerr << "Counting genotypes by group..." << flush;
      for (int i = 0; i < nsamp; ++i) {
        if (groups[i] == "") continue;
        int idx = bcf_gt_allele(gt_arr[i]);
        if (idx >= 0) {
          int jdx = gt_map[idx];
          if (jdx >= 0) grp_cnt[grp_idx[groups[i]]*nrow+jdx] += 1.0;
        }
      }

      int col_cnt = 0;
      vector<int> col_sums(ncol,0);
      vector<int> row_sums(nrow,0);
      for (int j = 0; j < ncol; ++j) {
        for (int k = 0; k < nrow; ++k) {
          col_sums[j] += grp_cnt[j*nrow+k];
        }

        if (col_sums[j] > 0) col_cnt++;

        for (int k = 0; k < nrow; ++k) {
          row_sums[k] += grp_cnt[j*nrow+k];
        }
      }

      int row_cnt = 0;
      for (int k = 0; k < nrow; ++k) {
        if (row_sums[k] > 0) row_cnt++;
      }
      int ntot = accumulate(row_sums.begin(),row_sums.end(),0);
      if (verbose > 1) cerr << "done." << endl;

      double prt = 1.0;
      double pre = 1.0;

      if ((col_cnt > 1 && row_cnt > 1) || count_all) {
        if (verbose > 1) {
          cerr << "Attempting to compute exact Fisher test (ntot = " << ntot << ")..." << flush;
          // for (int j = 0; j < ncol; ++j) cerr << j << " " << col_sums[j] << endl;
          // for (int k = 0; k < nrow; ++k) cerr << k << " " << row_sums[k] << endl;
        }
        fexact_(&nrow,&ncol,grp_cnt.data(),&nrow,&expect,&percent,&emin,&prt,&pre);

        if (pre >= 0.0) {
          if (verbose > 1) cerr << "Success!" << endl;
        } else {   // error in fexact
          if (options.count("verbose") > 1) cerr << "Cannot calculate exact fisher. Using simulations..." << endl;
          pre = 0.0;
          double statistic = 0.0;
          for (int j = 0; j < nrow*ncol; ++j) statistic -= fact[grp_cnt[j]];

          vector<int> observed(nrow*ncol);
          vector<int> jwork(ncol);
          for (int iter = 0; iter < fexact_B; ++iter) {
            rcont2(&nrow,&ncol,row_sums.data(),col_sums.data(),
                   &ntot,fact.data(),jwork.data(),observed.data(), 
                   &unif_rand);
            /* Calculate log-prob value from the random table. */
            double ans = 0.0;
            for (int j = 0; j < nrow*ncol; ++j) ans -= fact[observed[j]];
            if (ans <= statistic) pre += 1.0;
          }
          pre = (pre+1.0)/(fexact_B+1.0);
        }

        printf("%-8d %-6d %-16g %-16g",record->pos,nrow,prt,pre);
        for (int j = 0; j < ncol; ++j) {
          for (int k = 0; k < nrow; ++k) {
            printf(" %4d",(int) grp_cnt[j*nrow+k]);
          }
        }
        printf("\n");
        fflush(stdout);
      }
    }

    // get next record
    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (options.count("verbose") == 1) fprintf(stderr,"\n");

  gsl_rng_free(rng);
  rng = NULL;

  return 0;
}


