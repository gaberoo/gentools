#include "../hts.h"
#include "../vcf_stats.h"
#include "../FaIdx.h"

#include "edit_distance.h"

int coverage_scaled
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options)
{
  int verbose = options.count("verbose");

  string prefix = options["header"].as<string>();
  string regex_s = options["nameRegex"].as<string>();
  double scale = options["rescale-cov"].as<double>();

  int *dp = NULL;
  int *dp0 = NULL;

  int ndp = 0;
  int ndp0 = 0;

  int ret = -1;

  if (sr == NULL) ret = bcf_read(bcf1,header,record);
  else ret = bcf_sr_next_line(sr);

  const int nsamp = bcf_hdr_nsamples(header);

  vector< map<uint32_t,uint32_t> > cov(nsamp);
  for (int i = 0; i < nsamp; ++i) cov[i][0] = 0;

  vector<uint32_t> total(nsamp,0);
  uint32_t full_total = 0;
  int nsites = 0;

  while (ret == 0) {
    bcf_get_info_int32(header,record,"DP",&dp0,&ndp0);
    bcf_get_format_int32(header,record,"DP",&dp,&ndp);

    // need to get type, otherwise record is not unpacked
    bcf_get_variant_types(record);

    if (ndp0 > 0 && dp0[0] < 0) dp0[0] = 0;

    if (verbose == 1) fprintf(stderr,"%s %d %d\r",bcf_hdr_id2name(header,record->rid),record->pos,dp0[0]);

    if (ndp > 0) {
      int len = strlen(record->d.allele[0]);
      full_total += dp0[0]*len;
      nsites += len;
      for (int i = 0; i < nsamp; ++i) {
        uint32_t tot = (dp[i] > 0) ? dp[i] : 0;
        total[i] += tot*len;
        double ftot = 1.0*tot/dp0[0];
        uint32_t itot = (uint32_t) ceil(ftot*scale);
        cov[i][itot] += len;
      }
    }

    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (verbose == 1) fprintf(stderr,"\n");

  double total_mean_cov = 1.0*full_total/nsites;
  cerr << "full total = " << full_total << endl;
  cerr << "nsites = " << nsites << endl;
  cerr << "total mean cov = " << total_mean_cov << endl;

  cout << "#name\ttotal\tcnt" << endl;
  for (int i = 0; i < nsamp; ++i) {
    for (const auto& x: cov[i]) {
      double adj = 1.0*full_total/total[i];
      cout << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << "\t"
           << 1.0*x.first/scale*adj << "\t" << x.second << endl;
    }
  }

  return 0;
}
