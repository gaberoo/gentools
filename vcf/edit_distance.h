#include "../edlib/edlib.h"

const auto ed_config = edlibDefaultAlignConfig();
const auto ed_config_aln = edlibNewAlignConfig(-1,EDLIB_MODE_NW,EDLIB_TASK_PATH,NULL,0);

inline int null_snp(const char* allele) {
  if (strlen(allele) == 0) return 0;
  if (allele[0] == '<' && allele[1] == '*' && allele[2] == '>') return 1;
  return 0;
}

inline int maxlen(const char* allele1, const char* allele2) {
  int l1 = strlen(allele1);
  int l2 = strlen(allele2);
  return (l1 > l2) ? l1 : l2;
}

inline int edit_distance(const char* allele1, const char* allele2) {
  int ret = 0;
  int l1 = strlen(allele1);
  int l2 = strlen(allele2);

  if (l1 == 1 && l2 == 1) return allele1[0] != allele2[0];

  EdlibAlignResult ed_res;
  ed_res = edlibAlign(allele1,l1,allele2,l2,ed_config);

  if (ed_res.status == EDLIB_STATUS_OK) ret = ed_res.editDistance;
  else ret = -1;

  edlibFreeAlignResult(ed_res);

  return ret;
}

inline pair<int,int> edit_distance_2(const char* allele1, const char* allele2) {
  pair<int,int> ret;

  int l1 = strlen(allele1);
  int l2 = strlen(allele2);

  if (l1 == 1 && l2 == 1) return make_pair(allele1[0] != allele2[0],1);

  EdlibAlignResult ed_res;
  ed_res = edlibAlign(allele1,l1,allele2,l2,ed_config_aln);

  if (ed_res.status == EDLIB_STATUS_OK) {
    ret = make_pair(ed_res.editDistance,ed_res.alignmentLength);
  } else {
    ret = make_pair(-1,-1);
  }

  edlibFreeAlignResult(ed_res);

  return ret;
}

inline string remove_insertions(const char* ref, const char* alt) {
  string ret = "";
  int l1 = strlen(ref);
  int l2 = strlen(alt);

  EdlibAlignResult ed_res;
  ed_res = edlibAlign(ref,l1,alt,l2,ed_config_aln);

  if (ed_res.status == EDLIB_STATUS_OK) {
    int pos = 0;
    for (int i = 0; i < ed_res.alignmentLength; ++i) {
      switch (ed_res.alignment[i]) {
        case 0:
        case 3:
          ret += alt[pos++];
          break;

        case 1:
          ret += '-';
          break;

        case 2:
          pos++;
          break;

        default:
          break;
      }
    }

    for (int i = ed_res.alignmentLength; i < l1; ++i) ret += '-';
  }

  edlibFreeAlignResult(ed_res);

  return ret;
}
