#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

#include "edit_distance.h"

int pair_dist(htsFile* bcf1, 
              bcf_hdr_t* header, 
              bcf_srs_t* sr, 
              bcf1_t* record, 
              const vector<string>& sample_names, 
              const cxxopts::Options& options) 
{
  int verbose = options.count("verbose");
  int min_depth = options["min-depth"].as<int>();
  double min_qual = options["min-qual"].as<double>();
  int use_gt = options.count("use-gt");

  int nsamp = bcf_hdr_nsamples(header);
  if (verbose > 1) {
    cerr << "[pair-dist] running pair-dist..." << endl;
    cerr << "[pair-dist] nsamp = " << nsamp << endl;
  }

  // pairwise matrix
  vector<int> nsnps(nsamp*nsamp,0);
  vector<int> ncmp (nsamp*nsamp,0);
  int total_cmp = 0;

  int ret = -1;

  if (sr == NULL) ret = bcf_read(bcf1,header,record);
  else ret = bcf_sr_next_line(sr);

  // lookup table for pairwise comparisons
  vector< pair<int,int> > lookup;
  for (int a = 1; a < nsamp; ++a) {
    for (int b = 0; b < a; ++b) {
      lookup.push_back(make_pair(a,b));
    }
  }
  if (verbose) {
    cerr << "Stored " << lookup.size() << " comparisons in the lookup table." << endl;
  }

  while (ret == 0) {
    int na = record->n_allele;
    int type = bcf_get_variant_types(record);
    int len = max_allele_len(record->n_allele,record->d.allele);

    if (verbose == 1) {
      cerr << "[pair-dist] " 
           << bcf_hdr_id2name(header,record->rid) << " " 
           << record->pos << "          \r";
    } else if (verbose > 1) {
      cerr << "[pair-dist] "
           << bcf_hdr_id2name(header,record->rid) 
           << " pos = " << record->pos 
           << ", type = " << type << endl;
    }

    vector<int> edit_dist(na*na,-1);

    if (record->n_allele > 1) {
      for (int a = 1; a < na; ++a) {
        if (null_snp(record->d.allele[a])) continue;
        for (int b = 0; b < a; ++b) {
          if (null_snp(record->d.allele[b])) continue;
          edit_dist[a*na+b] = edit_distance(record->d.allele[a],record->d.allele[b]);
          edit_dist[b*na+a] = edit_dist[a*na+b];
          if (verbose > 1) {
            cerr << "   edit_distance(" << record->d.allele[a] << ","
                 << record->d.allele[b] << ") = " << edit_dist[a*na+b] << endl;
          }
        }
      }
    }

    total_cmp += len;
  
    int32_t* gt_arr = NULL;
    int32_t ngt_arr = 0;
    int ngt = 0;
    float* gq_arr = NULL;
    int32_t ngq_arr = 0;
    int ngq = 0;
      
    if (use_gt) {
      ngt = bcf_get_genotypes(header,record,&gt_arr,&ngt_arr);
      ngq = bcf_get_format_float(header,record,"GQ",&gq_arr,&ngq_arr);
      if (verbose > 1) cerr << "[pair-dist] ngt = " << ngt_arr << ", ngq = " << ngq_arr << endl;
    }

    // get per sample allelic depth
    int32_t* ad_arr = NULL;
    int32_t nad_arr = 0;
    int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);
    if (verbose > 1) cerr << "[pair-dist] nad = " << nad_arr << endl;

    vector<int> idx(nsamp,-1);

#pragma omp parallel for shared(gt_arr,ad_arr,ncmp,idx)
    for (int i = 0; i < nsamp; ++i) {
      if (use_gt && ngt > 0) {
        if (! bcf_gt_is_missing(gt_arr[i])) {
          if (ngq <= 0 || (min_qual <= 0.0 || gq_arr[i] >= min_qual)) {
            idx[i] = bcf_gt_allele(gt_arr[i]);
          } else {
            idx[i] = 0;
          }
        }
      }

      if (nad > 0 && idx[i] < 0) {
        idx[i] = array_max(ad_arr+i*(record->n_allele),record->n_allele);
        if (ad_arr[i*(record->n_allele)+idx[i]] < min_depth) idx[i] = -2;
      }

      if (idx[i] >= 0) ncmp[i*nsamp+i] += len;
    }

    if (verbose > 1) cerr << "[pair-dist] computing distances..." << endl;

#pragma omp parallel for shared(lookup,gt_arr,ncmp,nsnps,edit_dist,record,idx)
    for (size_t k = 0; k < lookup.size(); ++k) {
      int i = lookup[k].first;
      int j = lookup[k].second;

      // if (bcf_gt_is_missing(gt_arr[i]) || bcf_gt_is_missing(gt_arr[j])) continue;
      // int idxi = bcf_gt_allele(gt_arr[i]);
      // int idxj = bcf_gt_allele(gt_arr[j]);
      int idxi = idx[i];
      int idxj = idx[j];
      if (idxi < 0 || idxj < 0) continue;

      if (verbose > 1) {
        cerr << i << " <> " << j << ", " << idxi << " <> " << idxj << endl;
      }

      if (idxi != idxj) {
        nsnps[i*nsamp+j] += edit_dist[idxi*na+idxj];
        nsnps[j*nsamp+i] = nsnps[i*nsamp+j];
      }

      ncmp[i*nsamp+j] += maxlen(record->d.allele[idxi],record->d.allele[idxj]);
      ncmp[j*nsamp+i] = ncmp[i*nsamp+j];

      if (verbose > 1) {
        if (edit_dist[idxi*na+idxj] > 0) {
#pragma omp critical
          cerr << "[pair-dist] GT(" << i << ") = " << record->d.allele[idxi]
               << " <> GT(" << j << ") = " << record->d.allele[idxj]
               << ", d = " << edit_dist[idxi*na+idxj] << endl;
        }
      }
    }

    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (verbose == 1) cerr << endl;

  for (int i = 0; i < nsamp; ++i) {
    for (int j = 0; j < nsamp; ++j) {
      cout << sample_names[i] << "\t"
           << sample_names[j] << "\t"
           << total_cmp << "\t"
           << nsnps[i*nsamp+j] << "\t"
           << ncmp[i*nsamp+j] << "\t"
           << 1.0*nsnps[i*nsamp+j]/ncmp[i*nsamp+j] << "\t"
           << 1.0*ncmp[i*nsamp+j]/total_cmp << endl;
    }
  }

  return 0;
}


