#include "../cxxopts.hpp"
#include "../hts.h"
#include "../duo.h"

int zib_dp4(htsFile* bcf1, 
            bcf_hdr_t* header, 
            bcf_srs_t* sr, 
            bcf1_t* record, 
            const vector<string>& sample_names, 
            const cxxopts::Options& options)  
{
  int chrBegin  = options["begin"].as<int>();
  int chrEnd    = options["end"].as<int>();

  int verbose = options.count("verbose");

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;

  if (verbose > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  map<uint32_t,uint16_t> cnts;

  int max_n = 0;
  int tot_n = 0;
  uint32_t ix;

  while (record->pos < chrBegin) {
    if (verbose) cerr << "Searching..." << record->pos << "\r";
    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  while (ret == 0) {
    if (chrEnd > 0 && record->pos > chrEnd) break;

    ret = bcf_get_info_int32(header,record,"DP4",&ad,&ndst);
    if (! ret) continue;

    int n1 = ad[0] + ad[1];
    int n2 = ad[2] + ad[3];
    tot_n = n1+n2;
    max_n = (n1 > n2) ? n1 : n2;

    ix = duo(tot_n,tot_n-max_n);
    cnts[ix]++;

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  cout << "#name\tduo\ttotal\tnsnps\tcnt" << endl;
  for (const auto& x: cnts) {
    cout << x.first << "\t"
         << duo1(x.first) << "\t" << duo2(x.first) << "\t"
         << x.second << endl;
  }

  return 0;
}


