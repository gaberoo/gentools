#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

#include "../gff/GFFFile.h"

int stats(htsFile* bcf1, 
          bcf_hdr_t* header, 
          bcf_srs_t* sr, 
          bcf1_t* record, 
          const vector<string>& sample_names, 
          const cxxopts::Options& options) 
{
  int chrBegin = options["begin"].as<int>();
  int chrEnd = options["end"].as<int>();
  int verbose = options.count("verbose");
  int block_size = options["blockSize"].as<int>();

  int *ad0 = NULL;
  int ndst0 = -1;

  int *ad = NULL;
  int ndst = -1;

  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);

  int last_block = 0;
  int cur_block = 0;

  // sites with values
  vector<size_t> cnt(nsamp,0);

  // coverage
  vector<double> cov_tot(nsamp,0.0);
  vector<double> cov_tot2(nsamp,0.0);

  // SNP density
  vector<size_t> d_tot(nsamp,0.0);
 
  // SNP frequency
  vector<double> f_tot(nsamp,0.0);
  vector<double> f_tot2(nsamp,0.0);
  vector<double> f_var(nsamp,0.0);
  
  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;

  if (verbose > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  while (record->pos < chrBegin) {
    if (verbose) cerr << "Searching..." << record->pos << "\r";
    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  if (verbose) cerr << endl;

  cout << "# " << options["filename"].as<string>() << endl;
  cout << "# cnt = total number of informative sites in block" << endl;
  cout << "# cm  = mean coverage per block" << endl;
  cout << "# cv  = variance of coverage per block" << endl;
  cout << "# dns = density per block" << endl;
  cout << "# fm  = mean SNP frequency in block" << endl;
  cout << "# fv  = variance of SNP freq in block" << endl;

  int idx;
  double alpha;

  while (ret == 0) {
    cur_block = record->pos / block_size;

    if (chrEnd > 0 && record->pos >= chrEnd) break;

    if (cur_block > last_block) {
      for (int i = 0; i < nsamp; ++i) {
        cout << "cnt\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cnt[i] << endl;
        cout << "cm\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cov_tot[i]/cnt[i] << endl;
        cout << "cv\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cov_tot2[i]/cnt[i] - (cov_tot[i]/cnt[i])*(cov_tot[i]/cnt[i]) << endl;
        cout << "dns\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << 1.0*d_tot[i]/cnt[i] << endl;
        cout << "fm\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_tot[i]/d_tot[i] << endl;
        cout << "fv\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_tot2[i]/d_tot[i] - (f_tot[i]/d_tot[i])*(f_tot[i]/d_tot[i]) << endl;
        cout << "fs\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_var[i]/(d_tot[i]*d_tot[i]) << endl;
      }

      last_block = cur_block;

      fill(cnt.begin(),cnt.end(),0);
      fill(cov_tot.begin(),cov_tot.end(),0.0);
      fill(cov_tot2.begin(),cov_tot2.end(),0.0);
      fill(f_tot.begin(),f_tot.end(),0.0);
      fill(f_tot2.begin(),f_tot2.end(),0.0);
      fill(f_var.begin(),f_var.end(),0.0);
      fill(d_tot.begin(),d_tot.end(),0);
    }

    ret = bcf_get_info_int32(header,record,"AD",&ad0,&ndst0);

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);

    for (int i = 0; i < nsamp; ++i) {
      int max_n = 0;
      int tot_n = 0;

      for (int a = 0; a < record->n_allele; ++a) {
        idx = i*(record->n_allele) + a;
        tot_n += ad[idx];
        if (ad[idx] > max_n) max_n = ad[idx];
      }

      cov_tot[i] += tot_n;
      cov_tot2[i] += tot_n*tot_n;

      if (tot_n > 0 && max_n < tot_n) {
        d_tot[i]++;

        alpha = 1.0*(tot_n-max_n)/tot_n;
        f_tot[i] += alpha;
        f_var[i] += alpha*(1.0-alpha)/tot_n;
        f_tot2[i] += alpha*alpha;
      }

      cnt[i]++;
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  for (int i = 0; i < nsamp; ++i) {
    cout << "cnt\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cnt[i] << endl;
    cout << "cm\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cov_tot[i]/cnt[i] << endl;
    cout << "cv\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << cov_tot2[i]/cnt[i] - (cov_tot[i]/cnt[i])*(cov_tot[i]/cnt[i]) << endl;
    cout << "dns\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << 1.0*d_tot[i]/cnt[i] << endl;
    cout << "fm\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_tot[i]/d_tot[i] << endl;
    cout << "fv\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_tot2[i]/d_tot[i] - (f_tot[i]/d_tot[i])*(f_tot[i]/d_tot[i]) << endl;
    cout << "fs\t" << last_block*block_size << "\t" << sample_names[i] << "\t" << f_var[i]/(d_tot[i]*d_tot[i]) << endl;
  }

  return 0;
}

