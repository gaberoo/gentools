#include "../cxxopts.hpp"
#include "../hts.h"

int fst(htsFile* bcf1, 
        bcf_hdr_t* header, 
        bcf_srs_t* sr, 
        bcf1_t* record, 
        const vector<string>& sample_names, 
        const cxxopts::Options& options)  
{
  int verbose = options.count("verbose");
  size_t min_depth = options["min-depth"].as<int>();

  int nsamp = bcf_hdr_nsamples(header);
  int last_update = -1;
  int ret = -1;
  if (sr == NULL) {
    ret = bcf_read(bcf1,header,record);
    if (verbose) fprintf(stderr,"Stream mode: %d.\n",ret);
  } else {
    ret = bcf_sr_next_line(sr);
    if (verbose) fprintf(stderr,"Index mode: %d\n",ret);
  }

  vector<double> FST(nsamp*nsamp,0.0);
  vector<double> X2(nsamp*nsamp,0.0);
  vector<double> L1(nsamp*nsamp,0.0);
  vector<size_t> CNT(nsamp*nsamp,0);

  int nseqs = -1;
  const char** chrom_nm = bcf_hdr_seqnames(header,&nseqs);
  vector<int> chrom_len(nseqs);
  // vector<int> chrom_beg(nseqs);
  for (int i = 0; i < nseqs; ++i) {
    chrom_len[i] = header->id[BCF_DT_CTG][i].val->info[0];
    // chrom_beg[i] = (i > 0) ? chrom_beg[i-1] + chrom_len[i] : 0;
    if (verbose > 1) {
      fprintf(stderr,"%3d) %s [%d]\n",i,chrom_nm[i],chrom_len[i]);
    }
  }
  // int chrom_tot = chrom_beg[nseqs-1] + chrom_len[nseqs-1];

  // lookup table for pairwise comparisons
  vector< pair<int,int> > lookup;
  for (int a = 0; a < nsamp; ++a) {
    for (int b = 0; b <= a; ++b) {
      lookup.push_back(make_pair(a,b));
    }
  }
  if (verbose) {
    cerr << "Stored " << lookup.size() << " comparisons in the lookup table." << endl;
  }

  int chrom_id = -1;
  while (ret == 0) {
    // check if we are running into a new chromosome
    if (chrom_id != record->rid) {
      if (verbose > 1) {
        fprintf(stderr,"Updating chromosome id: %d => %d",chrom_id,record->rid);
      }
      chrom_id = record->rid;
      last_update = -1;
    }

    // verbose output
    if (verbose == 1) {
      int pct = 1000.0*((record->pos)+1)/chrom_len[record->rid];
      if (pct > last_update) {
        last_update = pct;
        fprintf(stderr,"%d | %s %4.1f%% (%d/%d)\r",
                record->rid,
                bcf_hdr_id2name(header,record->rid),
                pct/10.0,
                (int) record->pos,
                chrom_len[record->rid]);
      }
    } else if (verbose > 1) {
      fprintf(stderr,"%d | %s (%d/%d)\n",
              record->rid,
              bcf_hdr_id2name(header,record->rid),
              (int) record->pos,
              chrom_len[record->rid]);
    }

    // unpack record
    bcf_unpack(record,BCF_UN_ALL);

    // get per sample allelic depth
    int32_t* ad_arr = NULL;
    int32_t nad_arr = 0;
    int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);

    // make sure there is information on allelic depth
    if (nad > 0) {
      // compute total depth per sample
      vector<uint32_t> ad_tot(nsamp,0);
      for (int i = 0; i < nsamp; ++i) {
        ad_tot[i] = 0;
        int k = i*(record->n_allele);
        for (int j = 0; j < record->n_allele; ++j) {
          if (ad_arr[k] > 0) ad_tot[i] += ad_arr[k];
          ++k;
        }
      }

      // convert AD to probabilities
      vector<double> ap_arr(nsamp*record->n_allele);
      for (int i = 0; i < nsamp; ++i) {
        int k = i*(record->n_allele);
        for (int j = 0; j < record->n_allele; ++j) {
          if (ad_arr[k] > 0) {
            ap_arr[k] = 1.0*ad_arr[k]/ad_tot[i];
          } else {
            ap_arr[k] = 0.0;
          }
          ++k;
        }
      }

      // loop over all sample pairs
#pragma omp parallel for
      for (size_t a = 0; a < lookup.size(); ++a) {
        int i = lookup[a].first;
        int j = lookup[a].second;

        if (ad_tot[i] >= min_depth && ad_tot[j] >= min_depth) {
          vector<double> sigma(record->n_allele);
          vector<double> delta(record->n_allele);

          int i0 = i*(record->n_allele);
          int j0 = j*(record->n_allele);
          for (int k = 0; k < record->n_allele; ++k) {
            sigma[k] = ap_arr[i0+k] + ap_arr[j0+k];
            delta[k] = fabs(ap_arr[i0+k] - ap_arr[j0+k]);
          }

          double s2 = 0.0;
          double d2 = 0.0;

          for (int k = 0; k < record->n_allele; ++k) {
            s2 += sigma[k]*sigma[k];
            d2 += delta[k]*delta[k];
          }

          double F = (s2 < 4.0) ? d2/(4.0-s2) : 0.0;

          // compute the probability that two individuals are the same
          double x2 = 0.0;
          double l1 = 0.0;
          for (int k = 0; k < record->n_allele; ++k) {
            x2 += ap_arr[i0+k]*ap_arr[j0+k];
            l1 += fabs(ap_arr[i0+k]-ap_arr[j0+k]);
          }

#pragma omp critical
          {
            FST[i*nsamp+j] += F;
            X2[i*nsamp+j] += x2;
            L1[i*nsamp+j] += l1;
            CNT[i*nsamp+j]++;

            if (i != j) {
              FST[j*nsamp+i] += F;
              X2[j*nsamp+i] += x2;
              L1[j*nsamp+i] += l1;
              CNT[j*nsamp+i]++;
            }
          }
        }
      }
    }

    free(ad_arr);

    // get next record
    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (verbose == 1) {
    fprintf(stderr,"%20s\n","Done.");
    fprintf(stderr,"Outputting FST summary...\n");
  }

  for (int i = 0; i < nsamp; ++i) {
    for (int j = 0; j < nsamp; ++j) {
      cout << sample_names[i] << "\t" << sample_names[j] << "\t"
           << CNT[i*nsamp+j] << "\t"
           << FST[i*nsamp+j] << "\t"
           << X2[i*nsamp+j] << "\t"
           << 1.0 - X2[i*nsamp+j]/CNT[i*nsamp+j] << "\t"
           << L1[i*nsamp+j] << endl;
    }
  }

  free(chrom_nm);

  return 0;
}


