#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

int variable_sites_all(htsFile* bcf1, 
                       bcf_hdr_t* header, 
                       bcf_srs_t* sr, 
                       bcf1_t* record, 
                       const vector<string>& sample_names, 
                       const cxxopts::Options& options)  
{
  int min_depth = options["minDepth"].as<int>();
  int chrBegin  = options["begin"].as<int>();
  int chrEnd    = options["end"].as<int>();

  int verbose = options.count("verbose");

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;
  int na;
  double p[8];

  int nsamp = bcf_hdr_nsamples(header);

  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;

  if (verbose > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  while (ret == 0) {
    if (chrEnd > 0 && record->pos > chrEnd) break;
    na = record->n_allele;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    for (int i = 0; i < nsamp; ++i) {
      int tot = ptot(ad+i*na,p,record->n_allele,record->d.allele);
      if (tot >= min_depth) {
        int imax = array_max(p,4);
        if (p[imax] < 1.0) {
          cout << record->pos << " " 
               << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << " "
               << tot << " " << p[imax] << endl;
        }
      }
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  return 0;
}


