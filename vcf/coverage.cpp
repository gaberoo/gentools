#include "../hts.h"
#include "../vcf_stats.h"
#include "../FaIdx.h"

#include "edit_distance.h"

int coverage(htsFile* bcf1, 
             bcf_hdr_t* header, 
             bcf_srs_t* sr, 
             bcf1_t* record, 
             const vector<string>& sample_names, 
             const cxxopts::Options& options)
{
  int verbose = options.count("verbose");

  string prefix = options["header"].as<string>();
  int min_depth = options["minDepth"].as<int>();
  string regex_s = options["nameRegex"].as<string>();

  int *dp = NULL;
  int ndp = 0;
  int ret = -1;

  int tot = 0;

  if (sr == NULL) ret = bcf_read(bcf1,header,record);
  else ret = bcf_sr_next_line(sr);

  const int nsamp = bcf_hdr_nsamples(header);

  vector< map<uint32_t,uint32_t> > cov(nsamp);
  vector<uint32_t> nonzero(nsamp,0);
  int nsites = 0;

  while (ret == 0) {
    if (verbose == 1) fprintf(stderr,"%s %d\r",bcf_hdr_id2name(header,record->rid),record->pos);
    bcf_get_format_int32(header,record,"DP",&dp,&ndp);

    // need to get type, otherwise record is not unpacked
    // int type = bcf_get_variant_types(record);
    bcf_unpack(record,BCF_UN_ALL);

    if (ndp > 0) {
      int len = strlen(record->d.allele[0]);
      nsites += len;
      for (int i = 0; i < nsamp; ++i) {
        tot = (dp[i] > 0) ? dp[i] : 0;
        if (tot >= min_depth) nonzero[i] += len;
        cov[i][tot] += len;
      }
    }

    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  cout << "#name\ttotal\tcnt" << endl;
  for (int i = 0; i < nsamp; ++i) {
    for (const auto& x: cov[i]) {
      cout << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << "\t"
           << x.first << "\t" << x.second << endl;
    }
  }

  return 0;
}
