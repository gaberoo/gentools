#include "../cxxopts.hpp"
#include "../hts.h"

#include "edit_distance.h"

#include <gsl/gsl_sf.h>
#include <gsl/gsl_heapsort.h>
#include <gsl/gsl_randist.h>

inline double beta_binomial_lpmf(size_t k, size_t n, double alpha, double beta) {
  double lp = 0.0;
  lp += gsl_sf_lnchoose(n,k);
  lp += gsl_sf_lnbeta(k+alpha,n-k+beta);
  lp -= gsl_sf_lnbeta(alpha,beta);
  return lp;
}

inline int compare_ad_desc(const void* _a, const void* _b) {
  int32_t a = *(int32_t*) _a;
  int32_t b = *(int32_t*) _b;
  if (a > b) return -1;
  else if (a < b) return 1;
  else return 0;
}

inline double beta_mom_k(size_t k, double alpha, double beta, vector<double> bmk) {
  if (k == 0) return 1.0;
  while (k >= bmk.size()) {
    bmk.push_back((alpha+bmk.size())/(alpha+beta+bmk.size()));
  }
  return bmk[k-1];
}

int diversity
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options)  
{
  int verbose = options.count("verbose");

  size_t min_depth = options["min-depth"].as<int>();
  double log_cutoff = log(options["div-cutoff"].as<double>());
  double alpha = options["div-alpha"].as<double>();
  double beta = options["div-beta"].as<double>();

  int nsamp = bcf_hdr_nsamples(header);
  int chrom_id = -1;
  int last_update = -1;
  int ret = -1;

  vector<double> beta_mk;

  // read header
  if (sr == NULL) {
    ret = bcf_read(bcf1,header,record);
    if (verbose) fprintf(stderr,"Stream mode.\n");
  } else {
    ret = bcf_sr_next_line(sr);
    if (verbose) fprintf(stderr,"Index mode.\n");
  }

  // get chromosome names and lengths
  int nseqs = -1;
  const char** chrom_nm = bcf_hdr_seqnames(header,&nseqs);
  vector<int> chrom_len(nseqs);
  for (int i = 0; i < nseqs; ++i) {
    chrom_len[i] = header->id[BCF_DT_CTG][i].val->info[0];
    if (verbose > 1) {
      fprintf(stderr,"%3d) %s [%d]\n",i,chrom_nm[i],chrom_len[i]);
    }
  }

  // output table header
  cout << "pos\t" << "name\t" << "idx1\t" << "allele1\t" << "idx2\t"
       << "allele2\t" << "cnt1\t" << "cnt2\t" 
       << "p\t" << "p0\t" << "edit_dist" << endl;

  while (ret == 0) {
    // check if we are running into a new chromosome
    if (chrom_id != record->rid) {
      chrom_id = record->rid;
      last_update = -1;
    }

    // verbose output
    if (verbose == 1) {
      int pct = 1000.0*((record->pos)+1)/chrom_len[record->rid];
      if (pct > last_update) {
        last_update = pct;
        fprintf(stderr,"%s %4.1f%% (%d/%d)\r",
                bcf_hdr_id2name(header,record->rid),pct/10.0,record->pos,chrom_len[record->rid]);
      }
    } else if (verbose > 1) {
      fprintf(stderr,"%s (%d/%d)\n",
              bcf_hdr_id2name(header,record->rid),record->pos,chrom_len[record->rid]);
    }

    if (record->n_allele > 1) {
      // unpack record
      bcf_unpack(record,BCF_UN_ALL);

      // get per sample allelic depth
      int32_t* ad_arr = NULL;
      int32_t nad_arr = 0;
      int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);

      // make sure there is information on allelic depth
      if (nad > 0) {
        // compute total depth per sample
        vector<uint32_t> ad_tot(nsamp,0);
        for (int i = 0; i < nsamp; ++i) {
          ad_tot[i] = 0;
          int k = i*(record->n_allele);
          for (int j = 0; j < record->n_allele; ++j) {
            if (ad_arr[k] > 0) ad_tot[i] += ad_arr[k];
            ++k;
          }
        }

        // convert AD to probabilities
        vector<double> ap_arr(nsamp*record->n_allele);
        for (int i = 0; i < nsamp; ++i) {
          int k = i*(record->n_allele);
          for (int j = 0; j < record->n_allele; ++j) {
            if (ad_arr[k] > 0) {
              ap_arr[k] = 1.0*ad_arr[k]/ad_tot[i];
            } else {
              ap_arr[k] = 0.0;
            }
            ++k;
          }
        }

        for (int i = 0; i < nsamp; ++i) {
          if (ad_tot[i] < min_depth) continue;

          // get starting index of sample
          size_t id0 = i*(record->n_allele);

          // sort the allelic depths
          vector<size_t> p(record->n_allele,0);
          gsl_heapsort_index(p.data(),ad_arr+id0,record->n_allele,
                             sizeof(int32_t),compare_ad_desc);

          // top two AD values
          int one_n = ad_arr[id0+p[0]]; if (one_n < 0) one_n = 0;
          int two_n = ad_arr[id0+p[1]]; if (two_n < 0) two_n = 0;

          auto edist = edit_distance_2(record->d.allele[p[0]],record->d.allele[p[1]]);

          // check if SNP passes threshold
          double lp = 0.0;
          double lp0 = 0.0;
          if (edist.first == 1) {
            lp = beta_binomial_lpmf(two_n,one_n+two_n,alpha,beta);
            lp0 = M_LN2 - log(one_n+two_n);
          } else if (edist.first > 1) {
            double q = beta_mom_k(edist.first,alpha,beta,beta_mk);
            lp = log(gsl_ran_binomial_pdf(two_n,q,one_n+two_n));
            lp0 = edist.first*(M_LN2 - log(one_n+two_n));
          }


          if ((lp-lp0) < log_cutoff) {
            cout << record->pos << "\t" 
                 << sample_names[i] << "\t"
                 << p[0] << "\t" << record->d.allele[p[0]] << "\t"
                 << p[1] << "\t" << record->d.allele[p[1]] << "\t"
                 << one_n << "\t" 
                 << two_n << "\t"
                 << exp(lp) << "\t"
                 << exp(lp0) << "\t"
                 << edist.first << endl;
          }
        }
      }

      free(ad_arr);
    }

    // get next record
    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }


  return 0;
}


