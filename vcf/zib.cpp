#include "../cxxopts.hpp"
#include "../hts.h"
#include "../duo.h"

#include "edit_distance.h"

#include <gsl/gsl_heapsort.h>

inline int compare_ad_desc(const void* _a, const void* _b) {
  int32_t a = *(int32_t*) _a;
  int32_t b = *(int32_t*) _b;
  if (a > b) return -1;
  else if (a < b) return 1;
  else return 0;
}

int zib(htsFile* bcf1, 
        bcf_hdr_t* header, 
        bcf_srs_t* sr, 
        bcf1_t* record, 
        const vector<string>& sample_names, 
        const cxxopts::Options& options)  
{
  int verbose = options.count("verbose");

  int nsamp = bcf_hdr_nsamples(header);

  vector< map<uint64_t,size_t> > cnts(nsamp);
  int last_update = -1;
  string chrom = "";
  map<string,int> chrom_len;

  int ret = -1;
  if (sr == NULL) {
    ret = bcf_read(bcf1,header,record);
    if (verbose) fprintf(stderr,"Stream mode.\n");
  } else {
    ret = bcf_sr_next_line(sr);
    if (verbose) fprintf(stderr,"Index mode.\n");
  }

  while (ret == 0) {
    // check if we are running into a new chromosome
    if (chrom != bcf_hdr_id2name(header,record->rid)) {
      // update chromosome name
      chrom = bcf_hdr_id2name(header,record->rid);
      chrom_len[chrom] = header->id[BCF_DT_CTG][record->rid].val->info[0];
      // reset verbose counter
      last_update = -1;
    }

    // verbose output
    if (verbose == 1) {
      int pct = 1000.0*((record->pos)+1)/chrom_len[chrom];
      if (pct > last_update) {
        last_update = pct;
        fprintf(stderr,"%s %4.1f%% (%d/%d)\r",
                bcf_hdr_id2name(header,record->rid),pct/10.0,record->pos,chrom_len[chrom]);
      }
    } else if (verbose > 1) {
      fprintf(stderr,"%s (%d/%d)\n",
              bcf_hdr_id2name(header,record->rid),record->pos,chrom_len[chrom]);
    }

    // unpack record
    bcf_unpack(record,BCF_UN_ALL);

    // get length of reference allele
    int reflen = strlen(record->d.allele[0]);

    int ed_sum = 0;
    int ed_cnt = 0;

    vector< pair<int,int> > edit_dist((record->n_allele)*(record->n_allele),make_pair(-1,-1));

    if (record->n_allele > 1) {
      for (int a = 1; a < record->n_allele; ++a) {
        if (null_snp(record->d.allele[a])) continue;
        for (int b = 0; b < a; ++b) {
          if (null_snp(record->d.allele[b])) continue;

          auto edist = edit_distance_2(record->d.allele[a],record->d.allele[b]);

          edit_dist[a*(record->n_allele)+b] = edist;
          edit_dist[b*(record->n_allele)+a] = edist;

          ed_sum += edist.first;
          ed_cnt++;

          if (verbose > 1) {
            cerr << "[" << record->pos << "] edit_distance(" 
                 << record->d.allele[a] << ","
                 << record->d.allele[b] << ") = " 
                 << edist.first << "/"
                 << edist.second << endl;
          }
        }
      }
    }

    // get per sample allelic depth
    int32_t* ad_arr = NULL;
    int32_t nad_arr = 0;
    int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);

    if (verbose > 2) {
      cerr << record->pos << " " 
           << nad << " " 
           << record->n_allele << " " 
           << nad_arr << endl;
    }

    // make sure there is information on allelic depth
    if (nad > 0) {
      // loop over all samples
      for (int i = 0; i < nsamp; ++i) {
        // get starting index
        int id0 = i*(record->n_allele);

        if (record->n_allele > 1) {
          // sort allele array
          vector<size_t> p(record->n_allele,0);
          gsl_heapsort_index(p.data(),ad_arr+id0,record->n_allele,
                             sizeof(int32_t),compare_ad_desc);

          // top two AD values
          int one_n = ad_arr[id0+p[0]];
          if (one_n < 0) one_n = 0;

          int two_n = ad_arr[id0+p[1]];
          if (two_n < 0) two_n = 0;

          if (one_n+two_n > 10000) {
            cerr << endl << record->pos << " " 
                 << id0 << " "
                 << p[0] << " " << one_n << " " 
                 << p[1] << " " << two_n << endl;
          }

          auto edist = edit_dist[p[0]*(record->n_allele)+p[1]];

          // if there are any observed alleles, add to the counter
          if (one_n > 0) {
            if (two_n > 0) {
              int diff = edist.second - edist.first;

              if (edist.first >= 0 && edist.second > 0 && diff >= 0) {
                // account for edit distance between the two alleles
                // get duo-value (uint64_t = [uint32_t,uint32_t])
                uint64_t ix = duo(one_n+two_n,two_n);
                cnts[i][ix] += edist.first;

                // account for unedited positions
                ix = duo(one_n+two_n,0);
                cnts[i][ix] += diff;
              } else {
                // TODO: handle fully divergent sequences
              }
            } else {
              uint64_t ix = duo(one_n,0);
              cnts[i][ix] += strlen(record->d.allele[p[0]]);
            }
          }

          if (verbose > 2) {
            cerr << record->pos << " " << p[0] << " " << p[1]
                 << " " << one_n << " " << two_n << " " 
                 << edist.first << "/" << edist.second << endl;
          }
        } else if (ad_arr[id0] > 0) {
          uint64_t ix = duo(ad_arr[id0],0);
          cnts[i][ix] += reflen;
        } else {
          uint32_t ix = duo(0,0);
          cnts[i][ix] += reflen;
        }
      }

      free(ad_arr);
    }

    // get next record
    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (verbose == 1) {
    fprintf(stderr,"%20s\n","Done.");
    fprintf(stderr,"Outputting depth data...\n");
  }

  cout << "#name\tduo\ttotal\tnsnps\tcnt" << endl;

  for (int i = 0; i < nsamp; ++i) {
    for (const auto& x: cnts[i]) {
      cout << sample_names[i] << "\t" << x.first << "\t"
           << duo1(x.first) << "\t" << duo2(x.first) << "\t"
           << x.second << endl;
    }
  }

  return 0;
}


