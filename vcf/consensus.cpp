#include <fstream>
#include <set>
using namespace std;

#include <sys/ioctl.h>

#include "../hts.h"
#include "../vcf_stats.h"
#include "../FaIdx.h"

#include "edit_distance.h"

int consensus(htsFile* bcf1, 
              bcf_hdr_t* header, 
              bcf_srs_t* sr, 
              bcf1_t* record, 
              const vector<string>& sample_names, 
              const cxxopts::Options& options)
{
  struct winsize wmax;
  ioctl(0,TIOCGWINSZ,&wmax);

  char* errbuf = new char[wmax.ws_col];
  char* errmsg = new char[wmax.ws_col];
  char* erradd = new char[wmax.ws_col];

  ostream* out = &cout;
  if (options.count("output")) {
    out = new ofstream(options["output"].as<string>().c_str());
  }

  int verbose = options.count("verbose");

  string prefix = options["header"].as<string>();
  string regex_s = options["nameRegex"].as<string>();

  // int only_var  = options.count("only-variable");
  int use_gt    = options.count("use-gt");

  // int use_ref   = options.count("use-ref");
  string ref_fn = options["ref"].as<string>();

  int no_align = options.count("consensus-noalign");

  int min_depth = options["min-depth"].as<int>();
  int min_ref_depth = options["min-ref-depth"].as<int>();
  double min_qual = options["min-qual"].as<double>();
  double min_sample_qual = options["min-sample-qual"].as<double>();

  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);

  if (verbose > 1) cerr << "Fetching first record..." << endl;

  if (sr == NULL) {
    if (verbose) cerr << "Parsing in stream mode." << endl;
    ret = bcf_read(bcf1,header,record);
  } else {
    if (verbose) cerr << "Parsing in random access mode." << endl;
    ret = bcf_sr_next_line(sr);
  }

  int beg = record->pos;

  if (verbose > 1) {
    cerr << "Fetched first record: ret = " << ret << endl
         << "Starting position = " << beg << endl
         << "error code = " << record->errcode << endl;
  }

  string chrom = "";
  map<string,int> chrom_len;

  int total_len = 0;
  int total_pos = 0;
  int cur_len = 0;

  // output sequences
  vector<string> seqs(nsamp);

  // load reference FASTA file (if supplied)
  FaIdx ref;
  if (ref_fn != "") {
    if (verbose) {
      cerr << "Opening FASTA reference '" << ref_fn << "'..." << endl;
    }
    ref.open(ref_fn);
    ref.load_all();
  }

  // Start reading VCF file
  if (verbose > 1) { cerr << "Reading VCF file... (" << nsamp << " samples)" << endl; }
  
  int last_update = -1;

  map<string,int> chrom_pos;

  while (ret == 0) {
    if (chrom != bcf_hdr_id2name(header,record->rid)) {
      // check if sequence needs to be padded
      if (chrom != "") {
        int chrom_seq_len = seqs[0].length() - chrom_pos[chrom];
        for (; chrom_seq_len < chrom_len[chrom]; ++chrom_seq_len) {
          for (int i = 0; i < nsamp; ++i) seqs[i] += '.';
        }
      }
 
      // update chromosome name
      chrom = bcf_hdr_id2name(header,record->rid);
      chrom_len[chrom] = header->id[BCF_DT_CTG][record->rid].val->info[0];

      if (verbose) {
        sprintf(erradd,"| new chromosome: %s @ %lu (len = %d)",chrom.c_str(),seqs[0].length(),chrom_len[chrom]);
        sprintf(errbuf,"%s %s",errmsg,erradd);
        fprintf(stderr,"%s%*s\r",errbuf,(int)(wmax.ws_col-strlen(errbuf)),"");
      }

      // get current sequence length
      total_len = seqs[0].length();

      // get first position in the VCF file
      beg = record->pos;

      chrom_pos.insert(make_pair(chrom,total_len));

      // reset verbose counter
      last_update = -1;
    }

    if (verbose == 1) {
      int pct = 1000*(record->pos+1)/chrom_len[chrom];
      if (pct > last_update) {
        last_update = pct;
        sprintf(errmsg,"%s %4.1f%% (%d %d %lu)",
                bcf_hdr_id2name(header,record->rid),pct/10.0,
                record->pos,total_pos,seqs[0].length());
        sprintf(errbuf,"%s %s",errmsg,erradd);
        fprintf(stderr,"%s%*s\r",errbuf,(int)(wmax.ws_col-strlen(errbuf)),"");
      }
    }

    total_pos = record->pos + total_len;
    cur_len = total_pos;

    int stype = bcf_get_variant_types(record);

    if (verbose > 2) {
      *out << " " << (bcf_is_snp(record) ? '+' : '.') 
           << " " << (record->pos+1) << " " << record->n_allele << " " << stype << endl;
    }

    if ((int) seqs[0].length() < cur_len) {
      // missing entries
      if (verbose) {
        sprintf(erradd,"| missing entries @ %lu < %d (%d)",seqs[0].length(),total_pos,record->pos);
        sprintf(errbuf,"%s %s",errmsg,erradd);
        fprintf(stderr,"%s%*s\r",errbuf,(int)(wmax.ws_col-strlen(errbuf)),"");
      }
      while ((int) seqs[0].length() < cur_len) {
        for (int i = 0; i < nsamp; ++i) seqs[i] += '.';
      }
    } 
    
    if ((int) seqs[0].length() > cur_len) {
      if (verbose) {
        sprintf(erradd,"| overlap @ %lu < %d (%d)",seqs[0].length(),total_pos,record->pos);
        sprintf(errbuf,"%s %s",errmsg,erradd);
        fprintf(stderr,"%s%*s\r",errbuf,(int)(wmax.ws_col-strlen(errbuf)),"");
      }
    } else {
      int32_t* gt_arr = NULL;
      int32_t ngt_arr = 0;
      int ngt = 0;

      float* gq_arr = NULL;
      int32_t ngq_arr = 0;
      int ngq = 0;

      if (use_gt) {
        // get genotype calls
        ngt = bcf_get_genotypes(header,record,&gt_arr,&ngt_arr);
        // get genotype qualities
        ngq = bcf_get_format_float(header,record,"GQ",&gq_arr,&ngq_arr);

        if (ngt < 0 || ngq < 0) {
          cerr << "Genotype calls and genotype qualities on a per sample basis are required." << endl;
          cerr << bcf_hdr_id2name(header,record->rid) << " " << record->pos << " "
               << ngt << " " << ngt_arr << " " << ngq << " " << ngq_arr << endl;
          abort();
        }
      }

      // get per sample allelic depth
      int32_t* ad_arr = NULL;
      int32_t nad_arr = 0;
      int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);

      // get total allelic counts
      int32_t* ac_arr = NULL;
      int32_t nac_arr = 0;
      int nac = bcf_get_info_int32(header,record,"AC",&ac_arr,&nac_arr);

      // get reference allele observations
      int32_t* ro_arr = NULL;
      int32_t nro_arr = 0;
      int nro = bcf_get_format_int32(header,record,"RO",&ro_arr,&nro_arr);

      int tot_ac = 0;
      if (nac > 0) { 
        for (int idx = 0; idx < record->n_allele; ++idx) {
          tot_ac += ac_arr[idx]; 
        }
      }

      // store the aligned genotypes
      vector<string> gt_aln(record->n_allele);

      // get reference allele
      int len = strlen(record->d.allele[0]);
      gt_aln[0] = record->d.allele[0];

      if (verbose > 2) {
        cerr << bcf_hdr_id2name(header,record->rid) << " " 
             << record->pos << " " << record->n_allele << " "
             << ngt << " " << ngt_arr << " "
             << ngq << " " << ngq_arr << " "
             << nad << " " << nad_arr << " "
             << nac << " " << nac_arr << " "
             << gt_aln[0] << endl;
      }

      // only run the aligner for those genotypes that have been called
      if (use_gt) {
        // keep track of which genotypes are present
        set<int> gts;

        // insert the reference allele into the set
        gts.insert(0);
        gt_aln[0] = record->d.allele[0];

        for (int i = 0; i < nsamp; ++i) {
          // check if the genotype has been called
          if (! bcf_gt_is_missing(gt_arr[i])) {
            // get allelic index
            int idx = bcf_gt_allele(gt_arr[i]);

            // add this index to the list
            auto ret = gts.insert(idx);

            // if this is a new addition, run the aligner
            if (ret.second) {
              // check if the length is the same as the reference
              if (! no_align && ((int) strlen(record->d.allele[idx]) != len)) {
                // if no, align to the reference
                if (verbose > 1) cerr << "[" << record->pos << "] aligning allele " << idx << endl;
                gt_aln[idx] = remove_insertions(record->d.allele[0],record->d.allele[idx]);
              } else {
                // if yes, assume it's aligned
                gt_aln[idx] = record->d.allele[idx];
              }
            }
          }
        }
      } else {
        // align all alternate alleles to the reference
        for (int idx = 1; idx < record->n_allele; ++idx) {
          // check if the length is the same as the reference
          if (! no_align && ((int) strlen(record->d.allele[idx]) != len)) {
            // if no, align to the reference
            if (verbose > 1) cerr << "[" << record->pos << "] aligning allele " << idx << endl;
            gt_aln[idx] = remove_insertions(record->d.allele[0],record->d.allele[idx]);
          } else {
            // if yes, assume it's aligned
            gt_aln[idx] = record->d.allele[idx];
          }
        }
      }

      // add the genotypes to the sequences
      for (int i = 0; i < nsamp; ++i) {
        int idx = -1;
        int ref_idx = 0;

        if (use_gt && ! bcf_gt_is_missing(gt_arr[i])) {
          // check if genotype call has sufficient quality
          if (gq_arr[i] >= min_sample_qual) {
            idx = bcf_gt_allele(gt_arr[i]);

            // double check that there are not too few AD to accept the SNP
            if (nad > 0 && ad_arr[i*(record->n_allele)+idx] < min_depth) {
              idx = -11;
            }
          } else {
            idx = -10;
          }
        } else if (record->qual < min_qual) {
          // check if the total SNP quality passes the threshold
          idx = -20;
        }

        if (idx < 0 && idx > -10) {
          // get the allele with the most coverage
          idx = array_max(ad_arr+i*(record->n_allele),record->n_allele);

          // not enough coverage to keep allele
          if (nad > 0 && ad_arr[i*(record->n_allele)+idx] < min_depth) {
            idx = -12;
          }
        }

        if (idx <= -10) {
          ref_idx = 1;

          // check if there is sufficient coverage of the reference allele
          if (nro > 0 && ro_arr[i] >= min_ref_depth) idx = 0;
          else idx = -100;
        }

        if (idx >= 0) {
          if (ref_idx) {
            for (char c: gt_aln[idx]) seqs[i] += tolower(c);
          } else {
            for (char c: gt_aln[idx]) seqs[i] += toupper(c);
          }
        } else {
          for (int k = 0; k < len; ++k) seqs[i] += '.';
        }
      }

      if (ngt > 0) free(gt_arr);
      if (ngq > 0) free(gq_arr);
      if (nad > 0) free(ad_arr);
      if (ngq > 0) free(ac_arr);
      if (nro > 0) free(ro_arr);
    }

    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }

  if (verbose == 1) cerr << endl;

  // check if sequence needs to be padded
  int chrom_seq_len = seqs[0].length() - chrom_pos[chrom];
  for (; chrom_seq_len < chrom_len[chrom]; ++chrom_seq_len) {
    for (int i = 0; i < nsamp; ++i) seqs[i] += '.';
  }

  if (verbose) {
    size_t tot_len = 0;
    for (auto cl: chrom_len) tot_len += cl.second;
    cerr << "  sequence length = " << seqs[0].length() << endl;
    cerr << "  total length = " << tot_len << endl;
  }

  ostringstream name;
  regex regex_r(regex_s);
  smatch regex_m;

  DEBUG_PRINT("Starting output.\n");

  for (int i = 0; i < nsamp; ++i) {
    string name_s = bcf_hdr_int2id(header,BCF_DT_SAMPLE,i);
    DEBUG_PRINT("  %s\n",name_s.c_str());

    if (nchars(seqs[i]) > 0 || options.count("consensus-all")) {
      if (name_s == "") {
        name.str("");
        name << prefix;
        if (nsamp > 1) name << i;
        name_s = name.str();
      }

      if (regex_s != "") {
        DEBUG_PRINT("Fetching regex: %s\n", regex_s.c_str());
        if (regex_match(name_s,regex_m,regex_r)) {
          if (regex_m.size() == 2) {
            name.str(regex_m[1].str());
          }
        } else {
          DEBUG_PRINT("No match: %s\n", name_s.c_str());
        }
      }

      *out << ">" << name_s;
      *out << endl;
      *out << seqs[i] << endl;
    }
  }

  if (options.count("output")) {
    delete out;

    ofstream oidx((options["output"].as<string>() + ".chrpos").c_str());
    for (auto cp: chrom_pos) {
      oidx << cp.first << "\t" << cp.second+1 << endl;
    }
  }

  delete[] errbuf;
  return 0;
}


