#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

#include "../gff/GFFFile.h"

int gff_stats(htsFile* bcf1, 
              bcf_hdr_t* header, 
              bcf_srs_t* sr, 
              bcf1_t* record, 
              const vector<string>& sample_names, 
              const cxxopts::Options& options) 
{
  int verbose = options.count("verbose");

  string gff_fn = options["gff"].as<string>();

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);

  if (verbose) cerr << "Reading GFF file..." << flush;
  GFFFile gff(gff_fn);
  gff.read_gff();
  if (verbose) cerr << "done." << endl;

  // sites with values
  vector<size_t> cnt(nsamp,0);

  // coverage
  vector<double> cov_tot(nsamp,0.0);
  vector<double> cov_tot2(nsamp,0.0);

  // SNP density
  vector<size_t> syn_tot(nsamp,0.0);
  vector<size_t> nyn_tot(nsamp,0.0);
 
  // SNP frequency
  vector<double> f_tot(nsamp,0.0);
  vector<double> f_tot2(nsamp,0.0);
  vector<double> f_var(nsamp,0.0);
  
  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  cout << "# " << options["filename"].as<string>() << endl;
  cout << "# beg = start of block/gene" << endl;
  cout << "# end = end of block/gene" << endl;
  cout << "# cnt = total number of informative sites in block" << endl;
  cout << "# cm  = mean coverage per block" << endl;
  cout << "# cv  = variance of coverage per block" << endl;
  cout << "# dns = density per block" << endl;
  cout << "# fm  = mean SNP frequency in block" << endl;
  cout << "# fv  = variance of SNP freq in block" << endl;

  int idx;
  double alpha;

  size_t gid = 0;
  int active = 0;
  char gene_name[256];

  while (ret == 0) {
    if (active && record->pos > gff[gid]->core.end) {
      for (int i = 0; i < nsamp; ++i) {
        // cout << "beg\t" << "\t" << gid << "\t" << gff[gid]->core.start << endl;
        // cout << "end\t" << "\t" << gid << "\t" << gff[gid]->core.end << endl;
        size_t dtot = syn_tot[i] + nyn_tot[i];
        if (dtot > 0) {
          cout << "cnt\t" << "\t" << gene_name << "\t" << sample_names[i] << "\t" << cnt[i] << endl;
          cout << "snp\t" << "\t" << gene_name << "\t" << sample_names[i] << "\t" << dtot << endl;
          cout << "cm\t"  << "\t" << gene_name << "\t" << sample_names[i] << "\t" << cov_tot[i]/cnt[i] << endl;
          cout << "cv\t"  << "\t" << gene_name << "\t" << sample_names[i] << "\t" << cov_tot2[i]/cnt[i] - (cov_tot[i]/cnt[i])*(cov_tot[i]/cnt[i]) << endl;
          cout << "dns\t" << "\t" << gene_name << "\t" << sample_names[i] << "\t" << 1.0*dtot/cnt[i] << endl;
          cout << "fm\t"  << "\t" << gene_name << "\t" << sample_names[i] << "\t" << f_tot[i]/dtot << endl;
          cout << "fv\t"  << "\t" << gene_name << "\t" << sample_names[i] << "\t" << f_tot2[i]/dtot - (f_tot[i]/dtot)*(f_tot[i]/dtot) << endl;
          cout << "fs\t"  << "\t" << gene_name << "\t" << sample_names[i] << "\t" << f_var[i]/(dtot*dtot) << endl;
        }
      }

      fill(cnt.begin(),cnt.end(),0);
      fill(cov_tot.begin(),cov_tot.end(),0.0);
      fill(cov_tot2.begin(),cov_tot2.end(),0.0);
      fill(f_tot.begin(),f_tot.end(),0.0);
      fill(f_tot2.begin(),f_tot2.end(),0.0);
      fill(f_var.begin(),f_var.end(),0.0);
      fill(syn_tot.begin(),syn_tot.end(),0);
      fill(nyn_tot.begin(),nyn_tot.end(),0);

      active = 0;
      ++gid;
      if (gid >= gff.size()) break;
    } 
    else if (!active && record->pos >= gff[gid]->core.start) {
      active = 1;
      sscanf(gff[gid]->core.attribute_s, "ID=%[^;]", gene_name);
    }

    if (active) {
      ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
      if (! ret) continue;

      for (int i = 0; i < nsamp; ++i) {
        int max_n = 0;
        int tot_n = 0;

        for (int a = 0; a < record->n_allele; ++a) {
          idx = i*(record->n_allele) + a;
          tot_n += ad[idx];
          if (ad[idx] > max_n) max_n = ad[idx];
        }

        cov_tot[i] += tot_n;
        cov_tot2[i] += tot_n*tot_n;

        if (tot_n > 0 && max_n < tot_n) {
          alpha = 1.0*(tot_n-max_n)/tot_n;
          f_tot[i] += alpha;
          f_var[i] += alpha*(1.0-alpha)/tot_n;
          f_tot2[i] += alpha*alpha;
        }

        cnt[i]++;
      }
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  return 0;
}

