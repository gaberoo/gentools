#include "../hts.h"
#include "../vcf_stats.h"
#include "../FaIdx.h"

#include "edit_distance.h"

#include <deque>

int sliding_coverage
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options)
{
  int verbose = options.count("verbose");
  size_t window = options["window"].as<int>();
  int only_outliers = options.count("coverage-outliers");
  double min_freq = options["coverage-min-freq"].as<double>();

  int nsamp = bcf_hdr_nsamples(header);

  int last_update = -1;
  string chrom = "";
  map<string,int> chrom_len;

  vector< deque<int> > wqueue(nsamp);
  vector<int> wsum(nsamp,0);

  int ret = -1;
  if (sr == NULL) {
    ret = bcf_read(bcf1,header,record);
    if (verbose) fprintf(stderr,"Stream mode.\n");
  } else {
    ret = bcf_sr_next_line(sr);
    if (verbose) fprintf(stderr,"Index mode.\n");
  }

  while (ret == 0) {
    // check if we are running into a new chromosome
    if (chrom != bcf_hdr_id2name(header,record->rid)) {
      // update chromosome name
      chrom = bcf_hdr_id2name(header,record->rid);
      chrom_len[chrom] = header->id[BCF_DT_CTG][record->rid].val->info[0];
      last_update = -1;
    }

    // verbose output
    if (verbose == 1) {
      int pct = 1000.0*((record->pos)+1)/chrom_len[chrom];
      if (pct > last_update) {
        last_update = pct;
        fprintf(stderr,"%s %4.1f%% (%d/%d)\r",
                bcf_hdr_id2name(header,record->rid),pct/10.0,record->pos,chrom_len[chrom]);
      }
    } else if (verbose > 1) {
      fprintf(stderr,"%s (%d/%d)\n",
              bcf_hdr_id2name(header,record->rid),record->pos,chrom_len[chrom]);
    }


    // unpack record
    bcf_unpack(record,BCF_UN_ALL);

    int reflen = strlen(record->d.allele[0]);

    // get per sample allelic depth
    int32_t* ad_arr = NULL;
    int32_t nad_arr = 0;
    int nad = bcf_get_format_int32(header,record,"AD",&ad_arr,&nad_arr);

    if (nad > 0) {
      // loop over all samples
      for (int i = 0; i < nsamp; ++i) {
        // get starting index
        int id0 = i*(record->n_allele);

        int ad_sum = 0;
        int ad_max = 0;

        for (int k = 0; k < record->n_allele && id0+k < nad; ++k) {
          if (ad_arr[id0+k] > 0) {
            ad_sum += ad_arr[id0+k];
            if (ad_max < ad_arr[id0+k]) ad_max = ad_arr[id0+k];
          }
        }

        for (int l = 0; l < reflen; ++l) {
          wsum[i] += ad_sum;
          wqueue[i].push_back(ad_sum);

          if (wqueue[i].size() > window) {
            wsum[i] -= wqueue[i].front();
            wqueue[i].pop_front();
          }

          double wmean = 1.0*wsum[i]/wqueue[i].size();

          if ((min_freq > 0.0 && (1.0-1.0*ad_max/ad_sum) >= min_freq) ||
              (min_freq <= 0.0 && (!only_outliers || ad_sum > 1.6*wmean))) 
          {
            cout << record->pos+l << "\t" 
                 << sample_names[i] << "\t"
                 << wmean << "\t" 
                 << l << "\t"
                 << ad_sum << "\t"
                 << ad_max << "\t"
                 << 1.0*ad_max/ad_sum << endl;
          }
        }
      } 

      free(ad_arr);
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  if (verbose == 1) {
    fprintf(stderr,"%20s\n","Done.");
  }

  return 0;
}

