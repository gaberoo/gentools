#include "../cxxopts.hpp"

#include "../hts.h"
#include "../duo.h"

int true_snps(htsFile* bcf1, 
              bcf_hdr_t* header, 
              bcf_srs_t* sr, 
              bcf1_t* record, 
              const vector<string>& sample_names, 
              const cxxopts::Options& options) 
{
  int chrBegin = options["begin"].as<int>();
  int chrEnd = options["end"].as<int>();
  string name = options["name"].as<string>();
  double threshold = options["threshold"].as<double>();

  int verbose = options.count("verbose");

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;
  int na;

  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;

  if (verbose > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  int i = 0;
  if (name != "") {
    auto it = find(sample_names.begin(),sample_names.end(),name);
    if (it != sample_names.end()) i = distance(sample_names.begin(),it);
    else return 0;
  }

  while (ret == 0) {
    if (chrEnd > 0 && record->pos > chrEnd) break;
    na = record->n_allele;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    int max_n = 0;
    int tot_n = 0;
    for (int a = 0; a < record->n_allele; ++a) {
      tot_n += ad[i*na+a];
      if (ad[i*na+a] > max_n) max_n = ad[i*na+a];
    }

    double alpha = 1.0*(tot_n-max_n)/tot_n;
    if (alpha >= threshold) {
      printf("%8d %6d %6d %6f\n",record->pos,tot_n,max_n,alpha);
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  return 0;
}


