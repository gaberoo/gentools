#include "../cxxopts.hpp"
#include "../hts.h"
#include "../duo.h"

#include <string>
#include <fstream>
using namespace std;

int mean_hs(htsFile* bcf1, 
            bcf_hdr_t* header, 
            bcf_srs_t* sr, 
            bcf1_t* record, 
            const vector<string>& sample_names, 
            const cxxopts::Options& options) 
{
  int min_depth   = options["minDepth"].as<int>();
  string regex_s  = options["nameRegex"].as<string>();
  double min_qual = options["min-qual"].as<double>();
  int verbose     = options.count("verbose");

  int nsamp = bcf_hdr_nsamples(header);

  vector<int> grp(nsamp);
  vector<int> grp_size(nsamp,1);
  int ngrp = nsamp;

  string filename = options["groups"].as<string>();
  vector<string> groups;
  vector<string> uniq_grps;
  map<string,int> grp_idx;

  if (filename != "") {
    groups.resize(nsamp,"__undef");

    string next_name;
    string next_grp;
    ifstream in(filename);
    while (in >> next_name >> next_grp) {
      int p = bcf_hdr_id2int(header,BCF_DT_SAMPLE,next_name.c_str());
      if (p >= 0) groups[p] = next_grp;
      else {
        auto pit = find(sample_names.begin(),sample_names.end(),next_name);
        if (pit != sample_names.end()) {
          p = distance(sample_names.begin(),pit);
          groups[p] = next_grp;
        }
      }
    }
    in.close();

    uniq_grps = groups;
    sort(uniq_grps.begin(),uniq_grps.end());
    auto uniq_end = unique(uniq_grps.begin(),uniq_grps.end());
    uniq_end = remove(uniq_grps.begin(),uniq_end,"");
    uniq_grps.resize(distance(uniq_grps.begin(),uniq_end));
    ngrp = uniq_grps.size();

    int gidx = 0;
    for (auto g: uniq_grps) grp_idx[g] = gidx++;

    grp_size.clear();
    grp_size.resize(ngrp,0);

    for (int i = 0; i < nsamp; ++i) {
      grp[i] = grp_idx[groups[i]];
      grp_size[grp[i]]++;
    }
  } else {
    for (int i = 0; i < nsamp; ++i) {
      groups.push_back(sample_names[i]);
      uniq_grps.push_back(groups.back());
      grp_idx[groups.back()] = i;
      grp[i] = i;
      grp_size[i] = 1;
    }
    ngrp = nsamp;
  }

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  if (sr == NULL) ret = bcf_read(bcf1,header,record);
  else ret = bcf_sr_next_line(sr);

  vector<double> pest(ngrp,0.0);
  vector<double> hs(ngrp,0.0);
  vector<int> cov(ngrp,0);
  vector<size_t> cnt(ngrp,0);
  vector<size_t> hs_cnt(ngrp,0);

  while (ret == 0) {
    if (verbose == 1) fprintf(stderr,"%s %d\r",bcf_hdr_id2name(header,record->rid),record->pos);

    int stype = bcf_get_variant_types(record);

    if (verbose > 2) {
      cout << " " << (bcf_is_snp(record) ? '+' : '.') 
           << " " << (record->pos+1) << " " << record->n_allele << " " << stype << endl;
    }

    int ad_ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (ad_ret > 0) {
      if (verbose > 1) {
        cerr << record->pos << " " << ad_ret << " " << record->n_allele << endl;
      }

      for (int i = 0; i < nsamp; ++i) {
        int tot = 0;
        int dmax = 0;

        for (int k = i*(record->n_allele); k < (i+1)*(record->n_allele); ++k) {
          if (ad[k] > 0) {
            tot += ad[k];
            if (dmax < ad[k]) dmax = ad[k];
          }
        }

        if (tot >= min_depth) {
          cnt[grp[i]]++;
          cov[grp[i]] += tot;
        }

        if (dmax < tot && record->qual >= min_qual) {
          double pmax = 1.0*dmax/tot;
          pest[grp[i]] += pmax;
          hs[grp[i]] += pmax*pmax + (1.0-pmax)*(1.0-pmax);
          hs_cnt[grp[i]]++;
        }
      }
    }

    if (sr == NULL) ret = bcf_read(bcf1,header,record);
    else ret = bcf_sr_next_line(sr);
  }
  if (verbose) cerr << "done.     " << endl;

  for (int j = 0; j < ngrp; ++j) {
    cout << uniq_grps[j] << " " << cnt[j] << " " << 1.0*cov[j]/cnt[j] << " " 
         << hs_cnt[j] << " " << pest[j]/hs_cnt[j] << " " << hs[j]/hs_cnt[j] << endl;
  }

  return 0;
}


