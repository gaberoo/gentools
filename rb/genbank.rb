module GenTools
  class GFF
    def initialize(filename = nil)
      @regions = {}
      @alt = {}
      @entries = []
      @flags = {}
      parse filename if filename
    end

    def parse(filename)
      @filename = filename

      @flags = {}
      @entries = []

      pastOrigin = false

      File.open(filename) do |file|
        file.each_line do |line|
          # check for comment line

          if line =~ /^(ORIGIN|##FASTA)/

            pastOrigin = true
            break
          
          elsif m = /^##(.*)$/.match(line)

            parse_flag(m[1])

          elsif not pastOrigin

            arr = line.split /\t/
            @entries << {
              seqname: arr[0],
              source: arr[1],
              feature: arr[2],
              start: arr[3],
              end: arr[4],
              score: arr[5],
              strand: arr[6],
              frame: arr[7],
              attributes: arr[8].split(/;/).map{ |e| e.split '=' }.to_h
            }

          end
        end
      end

      entries.length
    end

    def parse_flag(input)
      flag = input.split
      idx = flag[1].to_s
      if flag[0] == "sequence-region"
        @regions[idx] = { start: flag[1].to_i, end: flag[2].to_i }
      elsif flag[0] == "alt-name"
        @alt[idx] = flag[2]
      else
        (@flags[idx] ||= []) << flag.slice(1,flag.length)
      end
    end

    def contigs
      @regions.keys
    end

    def entries
      @entries
    end

    def genes
      @entries
    end

    def alt(name = nil)
      name ? @alt[name] : @alt
    end

    def find(args = {})
      @entries.select do |entry|
        keep = true
        args.each do |key, value|
          keep = (keep && entry[:attributes][key] == value)
        end
        keep
      end
    end
  end
end
