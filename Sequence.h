#ifndef __SEQUENCE_H__
#define __SEQUENCE_H__

#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
using namespace std;

#include "tetnuc_table.h"

namespace dna_tools {
  uint8_t set_nuc(const char aa);
  uint8_t tet_nuc(const char* dna);

  class Sequence {
    public:
      Sequence(string filename, string name, uint64_t pos, uint64_t len, uint64_t ll, uint64_t ls) 
        : _fn(filename), _in(NULL), _pos(pos), _name(name), 
          _len(len), _linelen(ll), _lineseq(ls)
      {}

      Sequence(const Sequence& s)
        : _fn(s._fn), _in(NULL), _pos(s._pos), _name(s._name), 
          _len(s._len), _linelen(s._linelen), _lineseq(s._lineseq)
      {}

      virtual ~Sequence() { close_file(); }

      inline void open_file() { 
        if (_in == NULL) {
          // cerr << "Opening file for sequence " << _name << "..." << endl;
          _in = fopen(_fn.c_str(),"r");
        }
      }

      inline void close_file() {
        if (_in != NULL) { fclose(_in); _in = NULL; }
      }

      void read_seq();
      void write_seq(FILE* _out) const;
      inline void clear_seq() { _seq.clear(); }

      void pad_seq(size_t new_len);

      string to_str() const;
      inline uint64_t pos() const { return _pos; }
      inline uint64_t length() const { return _len; }

      inline void name(string n) { _name = n; }
      inline string name() const { return _name; }

      inline char operator[](size_t i) const { 
        return (i < _seq.size()) ? _seq[i] : 'n'; 
      }
      inline size_t size() const { return _seq.size(); }

      void base_cnts(size_t* cnts) const;
      void tet_nuc_freq(uint64_t* counts);

      inline size_t count() const {
        size_t cnt = 0;
        for (auto c: _seq) {
          switch (c) {
            case 'A': case 'a':
            case 'G': case 'g':
            case 'T': case 't':
            case 'C': case 'c':
              cnt++;
              break;
            default:
              break;
          }
        }
        return cnt;
      }

    // protected:
      char get(uint64_t pos) const;
      char get_from_seq(uint64_t pos) const;
      char get_from_file(uint64_t pos) const;
      char get_next();

      string _fn;
      FILE* _in;
      uint64_t _pos;

      string _name;  // name of the sequence
      uint64_t _len;      // length of the sequence
      uint64_t _linelen;  // length of each line
      uint64_t _lineseq;  // number of nucleotides per line

      vector<char> _seq;
  };
}

#endif

