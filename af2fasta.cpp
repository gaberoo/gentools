#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
using namespace std;

#include "AlleleFreqs.h"

int main(int argc, char* argv[]) {
  int min_depth = (argc > 1) ? atoi(argv[1]) : 3;

  dna_tools::AlleleFreqs af;
  af.read(cin,1,min_depth);

  af.printConsensus();
  cout << endl;

  /*
  string out(af.consensus());
  int n = out.length();
  int pos = 0;
  do {
    cout << out.substr(pos,60) << endl;
    pos += 60;
  } while (pos < n);
  */

  return 0;
}

