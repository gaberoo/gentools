#include <iostream>
#include <stdexcept>

#include "htslib/hts.h"
#include "htslib/sam.h"

using namespace std;

int main(int argc, char* argv[]) {
  if (argc < 2) return 0;

  string filename = argv[1];

  // open BAM for reading
  htsFile *in = hts_open(filename.c_str(), "r");
  if (in == NULL) {
    cerr << "Unable to open BAM/SAM file." << endl;
    return 0;
  }

  // Load the index
  hts_idx_t *idx = sam_index_load(in, filename.c_str());

  // Get the header
  bam_hdr_t *header = sam_hdr_read(in);
  hts_itr_t *iter = NULL;

  // Initiate the alignment record
  bam1_t *aln = bam_init1();

  while (sam_itr_next(in, iter, aln) >= 0) {
    cout << "Read Chr: " << header->target_name[aln->core.tid];
    cout << "\tPos: " << aln->core.pos;

    string seq, qual;
    uint8_t *quali = bam_get_qual(aln);
    uint8_t *seqi = bam_get_seq(aln);

    for (int i = 0; i < aln->core.l_qseq; i++) {
      seq += seq_nt16_str[bam_seqi(seqi, i)];
      qual += 33 + quali[i];
    }

    cout << "\tSeq: " << seq << "\tQual: " << qual;
    cout << endl;
  }

  hts_itr_destroy(iter);
  hts_idx_destroy(idx);

  bam_destroy1(aln);
  bam_hdr_destroy(header);

  sam_close(in);

  return 0;
}

