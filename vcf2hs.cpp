#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <map>
using namespace std;

#include "hts.h"
#include "AlleleFreqs.h"

#include "cxxopts.hpp"
using namespace cxxopts;

Options options("vcf2hs", "Calculate HS from VCF/BCF files");

string parse_filename(string filename) {
  // parse filename
  stringstream fn(filename);
  string id = "";
  while (getline(fn,id,'/'));
  stringstream name(id);
  vector<string> last;
  while (getline(name,id,'.')) last.push_back(id);
  if (last.size() > 1) last.pop_back();
  return accumulate(last.begin(),last.end(),string(""));
}

int main(int argc, char* argv[]) {
  vector<string> files;
  vector<size_t> depths;
  string region = "";

  options.add_options()
    ("f,filename", "BCF files",    cxxopts::value< vector<string> >(files))
    ("d,depth",    "Depth cutoff", cxxopts::value< vector<size_t> >(depths))
    ("r,region",   "Region",       cxxopts::value<string>(region))
    ("h,help",     "Display help")
  ;

  vector<string> _pos = { "filename" };
  options.parse_positional(_pos);
  options.positional_help("1.bcf [2.bcf 3.bcf ...]");

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  if (depths.size() == 0) {
    cerr << "Please secify at least one depth cutoff." << endl;
    exit(0);
  }

  /**************************************************************************/

  for (auto filename: files) {
    dna_tools::AlleleFreqs af;
    af.id = parse_filename(filename);
    af.read_BCF(filename,region);
    for (auto min_depth: depths) {
      auto HS = af.HS(min_depth);
      cout << af.id << " " << min_depth << " " << HS.first << " " << HS.second << endl;
    }
  }

  return 0;
}
