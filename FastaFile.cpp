#include "FastaFile.h"

/****************************************************************************/

/****************************************************************************/

int dna_tools::FastaFile::read_index(string filename) {
  ifstream index(filename.c_str());
  char name[256];
  uint64_t len = 0;
  uint64_t beg = 0;
  uint64_t seq_len = 0;
  uint64_t line_len = 0;
  string line = "";
  while (getline(index,line)) {
    sscanf(line.c_str(),"%s %lu %lu %lu %lu", name, &len, &beg, &seq_len, &line_len);
    // cerr << name << " " << len << " " << beg << " " << seq_len << " " << line_len << endl;
    _seq.push_back(Sequence(_fn,name,beg,len,line_len,seq_len));
    // _seq.back().open_file();
  }
  index.close();
  return _seq.size();
}

/****************************************************************************/

int dna_tools::FastaFile::build_index() {
  _seq.clear();
  string line;
  int len = 0;
  int line_len = 0;
  int seq_len = 0;
  string name = "";
  
  while (getline(_in,line)) {
    if (line[0] == '>') {
      if (_seq.size() > 0) {
        // set attributes of last sequence
        _seq.back()._len = len;
        _seq.back()._lineseq = line_len;
        _seq.back()._linelen = line_len+1;
      }

      // add new sequence
      line_len = 0;
      seq_len = 0;
      len = 0;
      istringstream iname(line.substr(1));
      iname >> name;
      _seq.push_back(Sequence(_fn,name,_in.tellg(),len,line_len,seq_len));
    } else {
      if (line_len == 0) line_len = line.length();
      len += line.length();
    }
  }

  if (_seq.size() > 0) {
    // set attributes of last sequence
    _seq.back()._len = len;
    _seq.back()._lineseq = line_len;
    _seq.back()._linelen = line_len+1;
  }

  return _seq.size();
}

/****************************************************************************/

char dna_tools::FastaFile::get(int id, int pos) const { 
  return _seq[id].get(pos); 
}

/****************************************************************************/

size_t dna_tools::FastaFile::max_len() const {
  uint64_t l = 0;
  for (size_t i = 0; i < _seq.size(); ++i) {
    if (l < _seq[i]._len) l = _seq[i]._len;
  }
  return l;
}

/****************************************************************************/

string dna_tools::FastaFile::diversity(size_t pos, double* p) const {
  string str = "";
  char c;
  for (size_t i = 0; i < _seq.size(); ++i) {
    c = get(i,pos);
    str += c;
    // cerr << c << endl;
    switch (c) {
      case '-': p[0] += 1.0; break;
      case 'A': case 'a': p[1] += 1.0; break;
      case 'G': case 'g': p[2] += 1.0; break;
      case 'C': case 'c': p[3] += 1.0; break;
      case 'T': case 't': p[4] += 1.0; break;
      default: p[5] += 1.0; break;
    }
  }
  return str;
}

/****************************************************************************/

int dna_tools::FastaFile::count(size_t pos) const {
  int cnt = 0;
  char c;
  for (size_t i = 0; i < _seq.size(); ++i) {
    c = get(i,pos);
    switch (c) {
      case 'A': case 'a':
      case 'G': case 'g':
      case 'C': case 'c':
      case 'T': case 't':
        cnt++;
        break;
      default:
        break;
    }
  }
  return cnt;
}

/****************************************************************************/

void dna_tools::FastaFile::read_seqs() {
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (size_t i = 0; i < _seq.size(); ++i) {
//#ifdef _OPENMP
//#pragma omp critical
//#endif
//    cerr << "Reading sequence " << i << "..." << endl;
    _seq[i].open_file();
    _seq[i].read_seq();
#ifdef _OPENMP
#pragma omp critical
#endif
    _seq[i].close_file();
    // cerr << "done." << endl;
  }
}

/****************************************************************************/

void dna_tools::FastaFile::subseqs(vector<string>& seqs, 
                                   vector<int>& pos,
                                   double min_div) 
{
  pos.clear();
  seqs.clear();
  seqs.resize(_seq.size(),"");
  size_t i;
#pragma omp parallel for
  for (i = 0; i < max_len(); ++i) {
    double p[6];
    memset(p,0,6*sizeof(double));
    string line = diversity(i,p);
    double inf = p[1] + p[2] + p[3] + p[4];
    if (inf > 1.0) {
      double div = plogp(p[1]/inf) + plogp(p[2]/inf) 
                 + plogp(p[3]/inf) + plogp(p[4]/inf);
// #pragma omp critical
      // cerr << i << " " << inf << " " << div << endl;
      if (min_div == 0.0 || div >= min_div) {
        // check if the locus has any diversity
#pragma omp critical
        {
          // add the locus to the consensus
          pos.push_back(i);
        }
      }
    }
  }

  for (auto i: sort_indexes(pos)) {
    for (size_t j = 0; j < seqs.size(); ++j) {
      seqs[j] += get(j,pos[i]);
    }
  }
}

/****************************************************************************/

void dna_tools::FastaFile::minOverlap(vector<string>& seqs, vector<int>& pos, int min) {
{
  pos.clear();
  seqs.clear();
  seqs.resize(_seq.size(),"");
  size_t i;
#pragma omp parallel for
  for (i = 0; i < max_len(); ++i) {
    int cnt = count(i);
    if (cnt >= min) {
#pragma omp critical
        {
          pos.push_back(i);
        }
      }
    }
  }

  for (auto i: sort_indexes(pos)) {
    for (size_t j = 0; j < seqs.size(); ++j) {
      seqs[j] += get(j,pos[i]);
    }
  }
}

/****************************************************************************/

bool is_agct(char a) {
  a = toupper(a);
  if (a == 'G' || a == 'C' || a == 'A' || a == 'T') return true; 
  else return false;
}

/****************************************************************************/

pair<double,uint64_t> 
dna_tools::FastaFile::pairwise_distance(size_t a, size_t b) const
{
  double dist = 0;
  uint64_t total = 0;

  char ca, cb;
  uint64_t i;

  uint64_t la = _seq[a].length();
  uint64_t lb = _seq[b].length();
  uint64_t len = (la > lb) ? lb : la;

  for (i = 0; i < len; ++i) {
    ca = toupper(get(a,i));
    cb = toupper(get(b,i));
    if (is_agct(ca) && is_agct(cb)) {
      if (ca != cb) dist += 1.0;
      total++;
    }
  }

  return make_pair(dist,total);
}

void dna_tools::FastaFile::allDiv(vector<double>& div, vector<int>& cnt) 
{
  div.resize(max_len(),0.0);
  cnt.resize(max_len(),0);
  size_t i;
#pragma omp parallel for
  for (i = 0; i < max_len(); ++i) {
    double p[6];
    memset(p,0,6*sizeof(double));
    string line = diversity(i,p);
    double inf = p[1] + p[2] + p[3] + p[4];
    if (inf > 1.0) {
      div[i] = plogp(p[1]/inf) + plogp(p[2]/inf) 
               + plogp(p[3]/inf) + plogp(p[4]/inf);
    }
    cnt[i] = (int) inf;
  }
}

/****************************************************************************/

void dna_tools::FastaFile::maxFreq(vector<double>& freq, vector<int>& cnt) 
{
  freq.resize(max_len(),0.0);
  cnt.resize(max_len(),0);
  size_t i;
#pragma omp parallel for
  for (i = 0; i < max_len(); ++i) {
    double p[6];
    memset(p,0,6*sizeof(double));
    string line = diversity(i,p);
    double inf = p[1] + p[2] + p[3] + p[4];
    double max = p[1];
    if (p[2] > max) max = p[2];
    if (p[3] > max) max = p[3];
    if (p[4] > max) max = p[4];
    freq[i] = max/inf;
    cnt[i] = (int) inf;
  }
}

/****************************************************************************/

string dna_tools::FastaFile::consensus() const {
  string ret;
  size_t i;

  for (i = 0; i < max_len(); ++i) {
    double p[6];
    memset(p,0,6*sizeof(double));
    string line = diversity(i,p);
    // add the locus to the consensus
    ret += pmax(p);
  }

  return ret;
}

/****************************************************************************/

void dna_tools::FastaFile::baseCounts(vector<basecnt_t>& cnts) const {
  cnts.clear();
  basecnt_t* x;
  for (size_t i = 0; i < size(); ++i) {
    cnts.push_back(basecnt_t());
    x = &cnts.back();
    _seq[i].base_cnts(x->cnts);
  }
}

/****************************************************************************/

char dna_tools::FastaFile::major(int pos) const {
  static const char y[] = { '-', 'A', 'G', 'C', 'T', 'o', 'O', 'x' };
  size_t x[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  for (size_t i = 0; i < size(); ++i) {
    switch (toupper(get(i,pos))) {
      case 'A': x[1]++; break;
      case 'G': x[2]++; break;
      case 'C': x[3]++; break;
      case 'T': x[4]++; break;
      case '-': x[0]++; break;
      default: x[5]++; break;
    }
  }
  // size_t cnt = x[1]+x[2]+x[3]+x[4];
  size_t b1, b2;

  if (x[1] > x[2]) { b1 = 1; }
  else { b1 = 2; }

  if (x[3] > x[4]) { b2 = 3; }
  else { b2 = 4; }

  if (x[b1] > x[b2]) { b2 = b1; }

  return y[b2];
}


