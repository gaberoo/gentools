#include <cstdlib>
#include <cmath>
#include <cstring>
#include <map>
using namespace std;

#include "hts.h"
#include "htslib/sam.h"

#include "cxxopts.hpp"

int main(int argc, char* argv[]) {
  cxxopts::Options options(argv[0], "Filter a BAM file by alignment score");

  int A;
  int B;
  string in_fn;
  string out_fn;
  double divergence;

  options.add_options()
    ("A", "Match score", cxxopts::value<int>(A)->default_value("4"), "INT")
    ("B", "Mismatch score", cxxopts::value<int>(B)->default_value("2"), "INT")
    ("i,input", "Input BAM file", cxxopts::value<string>(in_fn)->default_value("-"), "FILE")
    ("o,output", "Output BAM file", cxxopts::value<string>(out_fn)->default_value("-"), "FILE")
    ("d,divergence", "Allowed divergence", cxxopts::value<double>(divergence)->default_value("0.3"), "FLOAT")
    ("v,verbose", "Verbose")
    ("h,help", "Print help")
  ;

  vector<string> _pos = { "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  int verbose = options.count("verbose");

  // compute divergence cutoff
  double cutoff = ((1.0-divergence)*A - divergence*B);
  if (verbose) cerr << "Divergence cutoff = " << cutoff << endl;

  // open BAM for reading
  if (verbose) cerr << "Opening " << in_fn << "..." << flush;
  htsFile *in = hts_open(in_fn.c_str(), "r");
  if (in == NULL) { cerr << "Unable to open BAM/SAM file." << endl; return 0; }
  if (verbose) cerr << "done." << endl;

  // Get the header
  bam_hdr_t *header = sam_hdr_read(in);

  // Initiate the alignment record
  bam1_t *aln = bam_init1();

  htsFile *out = hts_open(out_fn.c_str(), "w");

  int ret;

  ret = sam_hdr_write(out,header);

  while (sam_read1(in, header, aln) >= 0) {
    string query = bam_get_qname(aln);
    string name = header->target_name[aln->core.tid];

    int AS = bam_aux2i(bam_aux_get(aln,"AS"));
    // int NM = bam_aux2i(bam_aux_get(aln,"NM"));
    int len = aln->core.l_qseq;

    // compute divergence cutoff

    if (1.0*AS/len >= cutoff) {
      ret = sam_write1(out,header,aln);
    }
  }

  bam_destroy1(aln);
  bam_hdr_destroy(header);

  hts_close(in);
  hts_close(out);

  return 0;
}

