#!/usr/bin/env ruby

require 'optparse'
require 'json'

require_relative 'rb/genbank'

args = {
  vcfConsensus: File.dirname(__FILE__) + '/vcfConsensus'
}

OptionParser.new do |opts|
  opts.banner = "Usage: extractGenes.rb [options]"

  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| args[:verbose] = v }
  opts.on("-g", "--genbank [FILE]", "Genbank file" ) { |g| args[:genbank] = g }
  opts.on("-a", "--[no-]alt", "Use alt name" ) { |a| args[:alt] = a }
  opts.on("-d", "--dir [DIR]", "Output directory") { |d| args[:dir]  = d }
  opts.on("-b", "--bcf [FILE]", "BCF file") { |b| args[:bcf] = b }
  opts.on("-C", "--contigs", "Extract full contigs") { |c| args[:contigs] = true }

  opts.on("", "--vcfConsensus [FILE]", "Path to program") { |x| args[:vcfConsensus] = x }

  opts.on("-h", "--help", "Prints this help") do
    puts opts
    exit
  end
end.parse!

if args[:bcf] == "STDIN"
  puts "Reading BCFs files from STDIN" if args[:verbose] 
  args[:bcf] = ARGF.read.split
end

puts "Reading Genbank file '#{args[:genbank]}'..." if args[:verbose] 
gff = GenTools::GFF.new args[:genbank]

#puts "Found #{gff.entries.length} genes."
#puts JSON.pretty_generate gff.entries

if args[:contigs]
  gff.contigs.each_with_index do |contig, i|
    contigName = contig.split(/\|/).first
    output = "#{args[:dir]}/#{contigName}.fasta"
    File.delete(output) if File.exists?(output)

    args[:bcf].each_with_index do |b, j|
      base = File.basename b, ".bcf"
      cmd = "#{args[:vcfConsensus]} -h \"#{base}\" -f #{b} -c \"#{contig}\" -d 3 >> #{output}"
      print "#{i+1}/#{gff.contigs.length} #{j+1}/#{args[:bcf].length} #{contig}\r"
      ret = `#{cmd}`
      #puts cmd
    end
  end
else
  gff.entries.each_with_index do |gene, i|
    reg = args[:alt] ? gff.alt(gene[:seqname]) : gene[:seqname]

    output = "#{args[:dir]}/#{gene[:attributes]["locus_tag"]}.fasta"
    File.delete(output) if File.exists?(output)

    args[:bcf].each_with_index do |b, j|
      base = File.basename b, ".bcf"
      cmd = "#{args[:vcfConsensus]} -h \"#{base}\" -f #{b} -c \"#{reg}\" -b #{gene[:start]} -e #{gene[:end]} -d 3 >> #{output}"
      print "#{i+1}/#{gff.entries.length} #{j+1}/#{args[:bcf].length} #{gene[:attributes]["locus_tag"]}\r"
      ret = `#{cmd}`
      #puts cmd
    end
  end
end
puts "Done.                                    "



