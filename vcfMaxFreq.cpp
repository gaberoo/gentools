#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <fstream>
using namespace std;

#include "hts.h"

int seg_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) if (alleles[i] > 0) cnt++;
  return cnt;
}

int tot_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) cnt += alleles[i];
  return cnt;
}

double arr_max(double* p) {
  double m1 = (p[1] > p[0]) ? p[1] : p[0];
  double m2 = (p[3] > p[2]) ? p[3] : p[2];
  return (m1 > m2) ? m1 : m2;
}

int main(int argc, char* argv[]) {
  // BCF filename
  string bcf_file = argv[1];

  // Chromosome
  string chr = (argc > 2) ? argv[2] : "-";

  // Open files for reading

  // cerr << "Opening files...";
  htsFile *bcf1 = hts_open(bcf_file.c_str(),"r");
  // cerr << "done." << endl;
  if (bcf1 == NULL) { cerr << "Unable to open file " << bcf_file << "." << endl; return 0; }

  bcf_hdr_t *header = bcf_hdr_read(bcf1);
  if (header == NULL) { cerr << "Unable to read header." << endl; return 0; }

  bcf_srs_t *sr = NULL;

  if (chr != "-") {
    cerr << "Selecting chromosome '" << chr << "'..." << flush;
    sr = bcf_sr_init();
    bcf_sr_set_regions(sr,chr.c_str(),0);
    bcf_sr_add_reader(sr,bcf_file.c_str());
    cerr << "done." << endl;
  }

  bcf1_t *record = bcf_init();

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);
  /*
  for (int i = 0; i < nsamp; ++i) {
    cout << i << " " << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << endl;
  }
  */

  int na;
  int ss = 0;
  int tot = 0;

  double p[8];

  vector<int> segSite(nsamp,0);
  vector<int> totSite(nsamp,0);

  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  while (ret == 0) {
    na = record->n_allele;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    cout << bcf_seqname(header,record) << "\t" << record->pos << "\t";
    for (int i = 0; i < nsamp; ++i) {
      ss = seg_sites(na,ad+i*na);
      // tot = tot_sites(na,ad+i*na);
      tot = ptot(ad+i*na,p,record->n_allele,record->d.allele);

      cout << arr_max(p) << "\t";
      // cout << bcf_seqname(header,record) << " " << i << " " << na << " " << tot << " " << ss << " " << endl;
      // cout << bcf_seqname(header,record) << " " << i << " ";
      // cout << p[0] << " " << p[1] << " " << p[2] << " " << p[3] << endl;
    }
    cout << endl;

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  hts_close(bcf1);

  if (sr != NULL) bcf_sr_destroy(sr);

  bcf_hdr_destroy(header);
  bcf_destroy(record); 

  return 0;
}
