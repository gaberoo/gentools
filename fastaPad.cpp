#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0], "Pad multifasta file");
  options.positional_help("fastaFile");

  options.add_options()
    ("f,input",    "Input file", cxxopts::value<string>(), "FILE")
    ("i,index",    "Index file", cxxopts::value<string>()->default_value(""))
    ("v,verbose",  "Verbose")
    ("h,help",     "Print help")
  ;

  vector<string> _pos = { "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    cout << options.help({ "" }) << endl;
    exit(0);
  }

  // -------------------------------------------------------------------------

  string filename = options["input"].as<string>();
  string index = options["index"].as<string>();
  
  dna_tools::FastaFile f(filename,index);
  if (options.count("verbose")) cerr << "Reading sequences...";
  f.read_seqs();
  if (options.count("verbose")) cerr << "done." << endl;

  size_t ml = f.max_len();
  for (size_t i = 0; i < f.size(); ++i) {
    if (options.count("verbose")) cerr << "Writing " << f[i].name() << "..." << endl;
    cout << ">" << f[i].name() << endl;
    for (size_t j = 0; j < ml; ++j) cout << f[i][j];
    cout << endl;
  }

  return 0;
}

