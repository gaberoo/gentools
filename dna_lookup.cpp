#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <sys/stat.h>
#include <unistd.h>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

int main(int argc, char** argv) {
  string filename = argv[1];
  string index = (argc > 2) ? argv[2] : "";

  if (index == "") {
    index = filename + ".fai";
    struct stat buffer;   
    if (stat (index.c_str(), &buffer) != 0) {
      cerr << "Index does not exist." << endl;
      cerr << "  " << index << endl;
      return 0;
    }
  }

  cerr << "Reading fasta file..." << endl;
  dna_tools::FastaFile f(filename,index);
  f.read_seqs();

  int first = 1;
  int block_size = 50;
  size_t j = 0;

  cerr << "Sorting..." << endl;

  size_t ml = f.max_len();
  cout << f.size() << " " << ml << endl;

  return 0;
}

