#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
using namespace std;

#include "hts.h"

int main(int argc, char* argv[]) {
  string file1 = argv[1];

  vector<int> min_depth;
  for (int k = 2; k < argc; ++k) {
    min_depth.push_back(atoi(argv[k]));
  }

  vector<size_t> cnt(min_depth.size(),0);
vector<double> total_FST(min_depth.size(),0.0);
vector<size_t> total_HD(min_depth.size(),0);
vector<size_t> total_seg(min_depth.size(),0);

// Open files for reading

  // cerr << "Opening files...";
  htsFile *bcf1 = hts_open(file1.c_str(),"r");
  // cerr << "done." << endl;
  if (bcf1 == NULL) { cerr << "Unable to open file 1." << endl; return 0; }

  bcf_hdr_t *h1 = bcf_hdr_read(bcf1);
  if (h1 == NULL) { cerr << "Unable to read header 1." << endl; return 0; }

  bcf1_t *rec1 = bcf_init();

  double p1[4];
  double p2[4];

  while (read_next(bcf1,h1,rec1) == 0) {
    int tot1 = info2p(p1,h1,rec1);
    if (tot1 > 0) {
      // cout << rec1->pos << " " << tot1 << endl;
      memset(p2,0,4*sizeof(double));
      const char* ref = rec1->d.allele[0];
      int id = idx(*ref);
      if (id >= 0) p2[id] = 1.0;
      double F = calc_fst(p1,p2);
      unsigned int i1 = max_index(p1);
      unsigned int i2 = max_index(p2);
      int HD = ((i1 & i2) == 0);
      for (size_t k = 0; k < min_depth.size(); ++k) {
        if (tot1 >= min_depth[k]) {
          ++cnt[k];
          total_FST[k] += F;
          total_HD[k] += HD;
          total_seg[k] += (rec1->n_allele>2);
        }
      }
    }
  }

  hts_close(bcf1);

  for (size_t k = 0; k < min_depth.size(); ++k) {
    cout << min_depth[k] << " " 
         << cnt[k] << " " << total_FST[k] << " " 
         << total_FST[k]/cnt[k] << " "
         << total_HD[k] << " " << 1.0*total_HD[k]/cnt[k] << " "
         << total_seg[k] << endl;
  }

  bcf_hdr_destroy(h1);
  bcf_destroy(rec1); 

  return 0;
}
