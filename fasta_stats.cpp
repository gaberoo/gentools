#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <array>
#include <map>
using namespace std;

#include "cxxopts.hpp"

#include "duo.h"
#include "hts.h"
#include "FaIdx.h"
#include "codon.h"

#include "fasta/fasta_stats.h"

gsl_rng* rng;
double unif_rand() { return gsl_rng_uniform(rng); };

double expect = 5.0;
double percent = 80;
double emin = 1;
double fexact_B = 99999;

/****************************************************************************/

int main(int argc, char** argv) {
  string action = "info";
  string filename = "";

  vector<string> marked_seqs;
  vector<string> excluded_seqs;

  cxxopts::Options options("fasta_stats", "Calucluate alignment statistics");

  options.positional_help("[options] <action> <input.fasta[.gz]>");
  options.add_options()
    ("a,action", "Action", cxxopts::value(action))
    ("f,input", "Input file", cxxopts::value(filename), "FILE")
    ("b,beg", "Region start", cxxopts::value<int>()->default_value("0"))
    ("e,end", "Region end",   cxxopts::value<int>()->default_value("0"))
    ("load", "Loading window", cxxopts::value<int>()->default_value("100000"))
    ("subset", "Subset sequences", cxxopts::value<string>()->default_value(""))
    ("s,seq", "Select sequence", cxxopts::value(marked_seqs))
    ("S,exseq", "Exclude sequence", cxxopts::value(excluded_seqs))
    ("format", "Output format", cxxopts::value<string>()->default_value("fasta"))
    ("t,type", "Alphabet type dna|aa [dna]", cxxopts::value<string>()->default_value("dna"))
    ("augment", "Augment alignment")
    ("revcomp", "Reverse complement sequences")
    ("binary", "Sequences are binary (0,1)")
    ("h,help", "Show help")
    ("v,verbose", "Verbose")
    ("window", "Aggregation window", cxxopts::value<int>()->default_value("1000"))
  ;

  options.add_options("raster")
    ("raster-vcf", "Output in VCF format")
    ("raster-all", "Include monomorphic sites")
    ("raster-contig-name", "Contig name", cxxopts::value<string>())
  ;

  options.add_options("rasterPng")
    ("png",        "PNG filename", cxxopts::value<string>()->default_value("raster.png"))
    ("png-width",  "Image width",  cxxopts::value<int>()->default_value("2100"))
    ("png-height", "Image height", cxxopts::value<int>()->default_value("1500"))
  ;

  options.add_options("filter")
    ("filter-non-gap", "Remove sites with gaps or indels")
    ("filter-genome-threshold", "Fraction of genome that must have coverage", cxxopts::value<double>()->default_value("0.0"))
    ("filter-keep-sites", "Keep invariant sites, only filter for coverage")
    ("filter-report-positions", "Only report positions, don't output FASTA")
    ("filter-by-codon", "Ouput all bases of codons (incl. filtered)")
  ;

  options.add_options("uniq")
    ("uniq-by-codon", "Iterate over codons (assumes the correct reading frame)")
    ("uniq-only-variable", "Only print variable sites")
  ;

  options.add_options("dist")
    ("dist-fmt", "Format", cxxopts::value<char>()->default_value("p"))
    ("dist-overlap", "Minimum overlap", cxxopts::value<size_t>()->default_value("1000"))
    ("dist-reps", "Bootstrap replicates", cxxopts::value<size_t>()->default_value("0"))
    ("dist-seed", "RNG seed", cxxopts::value<int>()->default_value("0"))
    ("dist-from", "Only compute distance to this entry", cxxopts::value< vector<string> >())
  ;

  options.add_options("count-snps")
    ("snps-min-freq", "Minimum frequency considered a SNP", cxxopts::value<double>())
    ("snps-min-cnt", "Minimum count considered a SNP", cxxopts::value<int>())
    ("snps-duo", "Output in ZIB format")
  ;

  options.add_options("coverage")
    ("coverage-gapdist", "Output gap histogram")
  ;

  options.add_options("gapFinder")
    ("gap-length", "Acceptable gap length", cxxopts::value<int>()->default_value("0"))
  ;

  options.add_options("informative")
    ("informative-all", "Show whole genome")
  ;

  options.add_options("pattern")
    ("pattern-graph", "Construct pattern correlation graph")
    ("pattern-min-overlap", "Minimum overlap for pattern matching")
  ;

  options.add_options("count-table")
    ("count-table-groups", "Grouping variables", cxxopts::value<string>()->default_value(""))
    ("count-table-all", "Don't skip uniform positions")
  ;

  options.add_options("mask")
    ("mask-ref", "Masking reference", cxxopts::value<string>()->default_value(""))
  ;

  options.add_options("consensus")
    ("consensus-name", "Output name", cxxopts::value<string>()->default_value("Consensus"))
    ("consensus-dist", "Outuput the distance from the consensus instead of the sequence")
    ("consensus-min-freq", "Mininum frequency to call a base", cxxopts::value<double>()->default_value("0.5"))
    ("consensus-output-all", "Output all sequences")
    ("consensus-weights", "Weight sequences when building consensus", cxxopts::value< vector<string> >())
  ;

  vector<string> _pos = { "action", "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  /**************************************************************************/

  FaIdx index(filename);

  if (options.count("subset") || marked_seqs.size() > 0) {
    index.mark_none();

    string subset = options["subset"].as<string>();
    if (subset != "") index.mark_seqs(subset);

    if (marked_seqs.size() > 0) {
      if (options.count("verbose")) {
        cerr << "Marking " << marked_seqs.size() << " additional sequences." << endl;
      }
      for (const auto& seq: marked_seqs) index.mark_seq(seq);
    }
  } else {
    index.mark_all();
  }

  if (excluded_seqs.size() > 0) {
    for (const auto& seq: excluded_seqs) index.mark_seq(seq,false);
  }

  if (action == "info") cout << index.info() << endl;
  else if (action == "view") view(index,options);
  else if (action == "mask") mask(index,options);
  else if (action == "countSnps" || action == "count-snps") count_snps(index,options);
  else if (action == "filter") filter(index,options);
  else if (action == "raster") raster(index,options);
  else if (action == "rasterPng") raster_png(index,options);
  else if (action == "sfs") sfs(index,options);
  else if (action == "informative") informative(index,options);
  else if (action == "coverage") coverage(index,options);
  else if (action == "gapFinder") gap_finder(index,options);
  else if (action == "dist" || action == "pair-dist") dist(index,options);
  else if (action == "patterns_old") aln_pattern(index,options);
  else if (action == "patterns") aln_pattern_full(index,options);
  else if (action == "binSum") binSum(index,options);
  else if (action == "count-table") count_table(index,options);
  else if (action == "composition") composition(index,options);
  else if (action == "consensus") consensus(index,options);
  else if (action == "diversity") diversity(index,options);
  else if (action == "uniq") unique_patterns(index,options);

  index.unload_all();

  return 0;
}


