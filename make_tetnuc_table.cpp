#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <string>
using namespace std;

#include "Sequence.h"

const char nuc[] = "AGCT";

int main() {
  int i1, i2, i3, i4;
  char tet[] = "AAAA";
  char rev[] = "AAAA";
  uint8_t a, b;
  for (i1 = 0; i1 < 4; ++i1) {
    rev[3] = tet[0] = nuc[i1];
    for (i2 = 0; i2 < 4; ++i2) {
      rev[2] = tet[1] = nuc[i2];
      for (i3 = 0; i3 < 4; ++i3) {
        rev[1] = tet[2] = nuc[i3];
        for (i4 = 0; i4 < 4; ++i4) {
          rev[0] = tet[3] = nuc[i4];
          a = dna_tools::tet_nuc(tet);
          b = dna_tools::tet_nuc(rev);
          // cout << (int) a << " " << (int) ((a<b) ? a : b) << endl;
          cout << (int) ((a<b) ? a : b) << ", " << endl;
        }
      }
    }
  }



  return 0;
}
