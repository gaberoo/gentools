#ifndef __PHYLIP_H__
#define __PHYLIP_H__

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
using namespace std;

// ===========================================================================

class Phylip : public vector<char> {
  public:
    Phylip() {}
    Phylip(int n, int len) : vector<char>(n*len), n(n), len(len) {}
    virtual ~Phylip() {}

    int n = 0;
    int len = 0;

    void resize(int n, int len);
    void read(const char* fn);

    int coverage(int locus) const;
    void to_fasta() const;

    vector<string> names;
};

// ===========================================================================

int Phylip::coverage(int locus) const {
  int cov = 0;
  for (int i = locus; i < n*len; i += len) {
    switch ((*this)[i]) {
      case 'a':
      case 'A':
      case 'g':
      case 'G':
      case 't':
      case 'T':
        cov++;
        break;
      default:
        break;
    }
  }
  return cov;
}

void Phylip::resize(int n, int len) { 
  vector<char>::resize(n*len,'-'); 
  this->n = n; 
  this->len = len; 
  names.resize(n,"");
}

void Phylip::read(const char* fn) {
  ifstream in(fn);

  in >> n >> len;
  cerr << "Reading alignment of " << n << " species of length " << len << "." << endl;
  resize(n,len);

  string tmp;
  size_t block = 0;

  for (int i = 0; i < n; ++i) {
    in >> names[i] >> tmp;
    if (block == 0) block = tmp.length();
    else if (block != tmp.length()) {
      cerr << "Block length " << i << " not equal to " << block << "!" << endl;
      return;
    }
    memcpy(data()+i*len,tmp.data(),block*sizeof(char));
  }

  cerr << "Block length = " << block << endl;
  
  int pos = block;
  while (! in.eof()) {
    for (int i = 0; i < n; ++i) {
      in >> tmp;
      memcpy(data()+i*len+pos,tmp.data(),tmp.length()*sizeof(char));
    }
    pos += block;
  }
}

void Phylip::to_fasta() const {
  string out;
  out.resize(len,'-');
  for (int i = 0; i < n; ++i) {
    cout << ">" << names[i] << endl;
    out.assign(data()+i*len,data()+(i+1)*len);
    cout << out << endl;
  }
}

#endif
