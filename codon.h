#ifndef __CODON_H__
#define __CODON_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <ctype.h>

#include "hts_defs.h"
#include "codon_table.h"

uint8_t char2int8(char a);

uint8_t nuc1(uint32_t codon);
uint8_t nuc2(uint32_t codon);
uint8_t nuc3(uint32_t codon);

uint32_t coduo(uint8_t n1, uint8_t n2, uint8_t n3);

uint32_t str2cod(const char str[]);
void cod2str(uint32_t codon, char str[]);

char coduo2aa(uint32_t codon);

#ifdef __cplusplus
}
#endif

#endif // __CODON_H__

