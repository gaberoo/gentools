#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
#include <string>
#include <regex>
#include <map>
using namespace std;

#include "cxxopts.hpp"

#include <gsl/gsl_math.h>
#include <gsl/gsl_histogram.h>

#include "PileUp.h"
#include "HarmonicNumber.h"
#include "duo.h"

cxxopts::Options options("pileupper", "PileUp Parser");

typedef struct { int cnt[4]; } Alleles;

/****************************************************************************/

int main(int argc, char** argv) {
  options.add_options()
    ("a,action",   "Action", cxxopts::value<string>()->default_value("segSites"))
    ("f,filename", "Input file", cxxopts::value<string>()->default_value("-"))
    ("n,names",    "Names file", cxxopts::value<string>()->default_value(""))
    ("regex",      "Regex for sample ID filtering", cxxopts::value<string>())
    ("nameRegex",  "Regex for name parsing", cxxopts::value<string>()->default_value(".*\\/([\\w_\\-]+)\\.bam$"))
    ("c,chr",      "Chromosome", cxxopts::value<string>()->default_value("-"))
    ("d,minDepth", "Minimum depth", cxxopts::value<int>()->default_value("3"))
    ("F,minFreq",  "Minimum frequency", cxxopts::value<double>()->default_value("0.67"))
    ("Q,minQual",  "Minimum quality", cxxopts::value<int>()->default_value("20"))
    ("record",     "Isolate record", cxxopts::value<int>()->default_value("0"))
    ("phred",      "PHRED score", cxxopts::value<int>()->default_value("33"))
    ("v,verbose",  "Verbose")
    ("help",       "Print help")
  ;

  vector<string> _pos = { "action", "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  /**************************************************************************/

  int min_depth = options["minDepth"].as<int>();
  int min_qual = options["minQual"].as<int>();
  string action = options["action"].as<string>();
  string filename = options["filename"].as<string>();
  string names_fn = options["names"].as<string>();
  string chr = options["chr"].as<string>();

  istream* in = NULL;
  if (filename == "-" || filename == "") {
    in = &cin;
  } else {
    in = new ifstream(filename.c_str());
  }

  /**************************************************************************/

  MPileUp entries;

  // set phred score
  entries.phred = options["phred"].as<int>();

  vector<bool> exclude;

  int ngroups = 0;
  // read sample names if they are supplied
  if (names_fn != "") {
    ifstream names_in(names_fn.c_str());
    string line;
    string name = "";
    int group = 1;
    while (getline(names_in,line)) {
      istringstream line_in(line);
      line_in >> name >> group;
      if (group > ngroups) ngroups = group;
      entries.addEntry(name,group);
    }

    /* include/exclude samples ********************************************/

    exclude.resize(entries.size(),false);
    string regex_s = options["regex"].as<string>();
    if (regex_s != "") {
      regex regex_r(regex_s);
      for (size_t i = 0; i < entries.size(); ++i) {
        if (regex_match(entries.names[i],regex_r)) {
          cerr << "Excluding '" << entries.names[i] << "'" << endl;
          exclude[i] = true;
        }
      }
    }
  }

  if (action == "segSites") {

    string line;
    size_t cnt = 0;
    size_t seg = 0;

    PileUpEntry entry;
    entry.phred = options["phred"].as<int>();

    while (getline(*in,line)) {
      entry.parse(line);
      if (entry.depth >= min_depth) {
        entry.unpack();
        ++cnt;
        seg += (entry.num_alleles(min_qual) > 1);
      }
    }

    cout << filename << " " << cnt << " " << seg << " " << 1.0*seg/cnt << endl;

  } else if (action == "sfs") {

    gsl_histogram* hist = gsl_histogram_alloc(10);
    gsl_histogram_set_ranges_uniform(hist,0.1,1.1);

    string line;
    PileUpEntry entry;
    entry.phred = options["phred"].as<int>();

    while (getline(*in,line)) {
      entry.parse(line);
      if (entry.depth >= min_depth) {
        entry.unpack();
        double max_freq = entry.max_freq(min_qual);
        gsl_histogram_increment(hist,max_freq);
      }
    }

    gsl_histogram_fprintf(stdout,hist,"%6.2f","%6.0f");
    gsl_histogram_free(hist);

  } else if (action == "theta") {
    // Estimate effective population size.
    // The fomula is
    //   theta = S / H[i]
    // where S are number of segregating sites, and i
    // are the number of individuals in the population.
    // In our case, the number of individuals is proxied
    // by the (high quality) coverage. However, different
    // sites have different coverage, hence we use an
    // approximation for the effective population size as
    // the mean over all sites.

    HarmonicNumber H; // initiate Harmonic number lookup table

    /************************************************************************/

    vector<int> all_cnt(ngroups+1,0);
    vector<int> all_S0(ngroups+1,0);
    vector<double> all_S(ngroups+1,0.0);

    vector<int> cnt(entries.size(),0);
    vector<int> S0(entries.size(),0);
    vector<double> S(entries.size(),0.0);

    string line;

    if (! getline(*in,line)) {
      cerr << "Empty file." << endl;
      exit(0);
    }

    if (names_fn == "") {
      entries.parse(line); // parse the line and split

      // resize the counter vectors, if necessary
      if (cnt.size() < entries.size()) cnt.resize(entries.size(),0);
      if (S0.size() < entries.size()) S0.resize(entries.size(),0);
      if (S.size() < entries.size()) S.resize(entries.size(),0.0);
    }

    // loop over all the lines in the file
    do {
      entries.parse(line,true,false); // parse the line and split

      // initialize the allele counters across all samples
      vector<Alleles> all_alleles(ngroups+1,{ { 0, 0, 0, 0 } });

      // loop over the samples
      for (size_t i = 0; i < entries.size(); ++i) {
        if (i < exclude.size() && exclude[i]) continue;

        // initialize the allele counter for this sample
        int alleles[] = { 0, 0, 0, 0 };

        int g = entries.groups[i];
        
        entries[i].cnt(alleles,min_qual); // count the alleles;
        add1(all_alleles[0].cnt,alleles); // add allele counts to total;
        add1(all_alleles[g].cnt,alleles); // add allele counts to total;

        int d = sum1(alleles);            // get high qual depth;
        if (d >= min_depth) {             // if passes the depth threshold:
          cnt[i]++;                       // increment the counter;
          int n = seg1(alleles);          // get number of alleles;
          if (n > 1) {                    // if segregating, add to counter
            S[i] += 1.0/H[d-1];
            S0[i]++;
          }
        }
      }

      for (int g = 0; g <= ngroups; ++g) {
        int d0 = sum1(all_alleles[g].cnt);  // get total depth
        if (d0 >= min_depth) {              // if passes the threshold:
          all_cnt[g]++;                     // increment counter
          int n = seg1(all_alleles[g].cnt); // get number of alleles
          if (n > 1) {                      // if segregating, increment the counter
            all_S[g] += 1.0/H[d0-1];
            all_S0[g]++;
          }
        }
      }
    } while (getline(*in,line));

    // set up regex name parsing for file names
    string regex_s = options["nameRegex"].as<string>();
    regex regex_r(regex_s);
    smatch regex_m;

    for (size_t i = 0; i < S.size(); ++i) {
      if (i < exclude.size() && exclude[i]) continue;

      // parse name
      string name = "Seq";
      if (i < entries.names.size()) name = entries.names.at(i);
      if (regex_s != "") {
        if (regex_match(name,regex_m,regex_r)) {
          if (regex_m.size() == 2) name = regex_m[1].str();
        }
      }

      // output values for each sample
      double theta = 0.0;
      double fs = 0.0;
      if (cnt[i] > 0) {
        theta = S[i]/cnt[i];
        fs = 1.0*S0[i]/cnt[i];
      }
      cout << name << " " 
           << theta << " " 
           << fs << " "
           << S[i] << " " 
           << cnt[i] << " "
           << S0[i] << endl;
    }

    // output values for total
    for (int g = 0; g <= ngroups; ++g) {
      cout << "%G" << g << " "
           << ((all_cnt[g]>0) ? all_S[g]/all_cnt[g] : 0.0) << " " 
           << ((all_cnt[g]>0) ? 1.0*all_S0[g]/all_cnt[g] : 0.0) << " "
           << all_S[g] << " " 
           << all_cnt[g] << " "
           << all_S0[g] << endl;
    }

  } else if (action == "af1") {
   
    string line;
    if (! getline(*in,line)) {
      cerr << "Empty file." << endl;
      exit(0);
    }

    vector< map<uint32_t,uint16_t> > cnt(entries.size());
    vector< map<uint32_t,uint16_t> > gcnt(ngroups+1);

    // loop over all the lines in the file
    do {
      entries.parse(line,true,false); // parse the line and split

      vector<Alleles> all_alleles(ngroups+1,{ { 0, 0, 0, 0 } });

      for (size_t i = 0; i < entries.size(); ++i) {
        int alleles[] = { 0, 0, 0, 0 };
        entries[i].cnt(alleles,min_qual);

        int d = sum1(alleles);
        if (d >= min_depth) {
          int m = max1(alleles);
          cnt[i][duo(d,m)]++;
        }

        int g = entries.groups[i];
        add1(all_alleles[0].cnt,alleles);
        add1(all_alleles[g].cnt,alleles);
      }

      for (int g = 0; g <= ngroups; ++g) {
        int d0 = sum1(all_alleles[g].cnt);  // get total depth
        if (d0 >= min_depth) {              // if passes the threshold:
          int m0 = max1(all_alleles[g].cnt);
          gcnt[g][duo(d0,m0)]++;
        }
      }
    } while (getline(*in,line));

    // set up regex name parsing for file names
    string regex_s = options["nameRegex"].as<string>();
    regex regex_r(regex_s);
    smatch regex_m;

    for (size_t i = 0; i < entries.size(); ++i) {
      if (i < exclude.size() && exclude[i]) continue;

      // parse name
      string name = "Seq";
      if (i < entries.names.size()) name = entries.names.at(i);
      if (regex_s != "") {
        if (regex_match(name,regex_m,regex_r)) {
          if (regex_m.size() == 2) name = regex_m[1].str();
        }
      }

      map<uint32_t,uint16_t>::iterator c = cnt[i].begin();
      for (; c != cnt[i].end(); ++c) {
        cout << name << " " << c->first << " " << c->second << endl;
      }
      cout << endl;
    }

    for (int g = 0; g <= ngroups; ++g) {
      map<uint32_t,uint16_t>::iterator c = gcnt[g].begin();
      for (; c != gcnt[g].end(); ++c) {
        cout << "%G" << g << " " << c->first << " " << c->second << endl;
      }
      cout << endl;
    }

  }

  if (in != NULL && in != &cin) delete in;

  return 0;
}


