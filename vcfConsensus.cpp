#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <fstream>
using namespace std;

#include "hts.h"

#include <gsl/gsl_statistics.h>

#include "cxxopts.hpp"

int seg_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) if (alleles[i] > 0) cnt++;
  return cnt;
}

int tot_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) cnt += alleles[i];
  return cnt;
}

double arr_max(double* p) {
  double m1 = (p[1] > p[0]) ? p[1] : p[0];
  double m2 = (p[3] > p[2]) ? p[3] : p[2];
  return (m1 > m2) ? m1 : m2;
}

size_t nchars(const string& seq) {
  size_t cnt = 0;
  string::const_iterator c = seq.begin();
  while (c++ != seq.end()) { 
    switch (*c) {
      case 'a': case 'A':
      case 'c': case 'C':
      case 'g': case 'G':
      case 't': case 'T':
        ++cnt;
        break;
      default:
        break;
    }
  }
  return cnt;
}

cxxopts::Options options("vcfConsensus", "Get consensus for a specific region");

int main(int argc, char* argv[]) {
  string bcf_file = "";
  string chr = "-";
  int min_depth = 3;
  string prefix = "S";
  int chrBegin = 0;
  int chrEnd = 0;

  options.add_options()
    ("f,filename", "Input file", cxxopts::value<string>(bcf_file))
    ("c,chr",      "Chromosome", cxxopts::value<string>(chr))
    ("d,minDepth", "Minimum depth", cxxopts::value<int>(min_depth))
    ("b,begin",    "Chr begin", cxxopts::value<int>(chrBegin))
    ("e,end",      "Chr end", cxxopts::value<int>(chrEnd))
    ("h,header",   "Header prefix", cxxopts::value<string>(prefix))
    ("v,verbose",  "Verbose")
    ("help",       "Print help")
  ;

  vector<string> _pos = { "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  if (chr != "-" && (chrBegin > 0 || chrEnd > 0)) {
    ostringstream ochr;
    ochr << chr << ":" << chrBegin;
    if (chrEnd > chrBegin) ochr << "-" << chrEnd;
    chr = ochr.str();
  }

  // Open files for reading

  if (options.count("verbose")) cerr << "Opening file: " << bcf_file << "..." << flush;
  htsFile *bcf1 = hts_open(bcf_file.c_str(),"r");
  if (options.count("verbose")) cerr << "done." << endl;

  if (bcf1 == NULL) { cerr << "Unable to open file " << bcf_file << "." << endl; return 0; }

  bcf_hdr_t *header = bcf_hdr_read(bcf1);
  if (header == NULL) { cerr << "Unable to read header." << endl; return 0; }

  bcf_srs_t *sr = NULL;

  if (chr != "-") {
    if (options.count("verbose")) cerr << "Selecting chromosome '" << chr << "'..." << flush;
    sr = bcf_sr_init();
    bcf_sr_set_regions(sr,chr.c_str(),0);
    bcf_sr_add_reader(sr,bcf_file.c_str());
    if (options.count("verbose")) cerr << "done." << endl;
  }

  bcf1_t *record = bcf_init();

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);

  int na;
  int tot = 0;

  double p[8];

  if (sr == NULL) ret = read_next(bcf1,header,record);
  else ret = read_next_idx(sr,header,&record);

  if (ret != 0) exit(0);

  int pos = (chrBegin > 0) ? chrBegin-1 : 0;
  int idx = -1;

  vector<string> seqs(nsamp);

  if (options.count("verbose") > 1) {
    cerr << "Starting at position " << pos << endl;
    cerr << "  " << record->pos << endl;
    cerr << "  " << chrBegin << endl;
  }

  int32_t curSeq = record->rid;

  while (ret == 0) {
    if (record->rid != curSeq) {
      for (int i = 0; i < nsamp; ++i) {
        cout << ">" << prefix;
        if (nsamp > 1) cout << i;
        cout << " " << bcf_hdr_id2name(header,curSeq) << " " << seqs[i].length();
        cout << " " << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << endl;
        cout << seqs[i] << endl;

        curSeq = record->rid;
        pos = 0;
        seqs[i].clear();
      }

    }

    na = record->n_allele;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    while (pos++ < record->pos) {
      for (int i = 0; i < nsamp; ++i) seqs[i] += 'n';
    }

    for (int i = 0; i < nsamp; ++i) {
      tot = ptot(ad+i*na,p,record->n_allele,record->d.allele);
      if (tot >= min_depth && arr_max(p) > 0.66) {
        idx = max_index(p);
        if (idx == HTS_A) seqs[i] += 'A';
        else if (idx == HTS_G) seqs[i] += 'G';
        else if (idx == HTS_C) seqs[i] += 'C';
        else if (idx == HTS_T) seqs[i] += 'T';
        else seqs[i] += 'N';
      } else seqs[i] += 'n';
    }

    if (sr == NULL) ret = read_next(bcf1,header,record);
    else ret = read_next_idx(sr,header,&record);
  }

  for (int i = 0; i < nsamp; ++i) {
    if (seqs[i].length() > 0) {
      cout << ">" << prefix;
      if (nsamp > 1) cout << i;
      cout << " " << bcf_hdr_id2name(header,curSeq) << " " << seqs[i].length();
      cout << " " << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << endl;
      cout << seqs[i] << endl;
    }
  }

  hts_close(bcf1);

  // if (sr != NULL) bcf_sr_destroy(sr);

  bcf_hdr_destroy(header);
  bcf_destroy(record); 

  return 0;
}
