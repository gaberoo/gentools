#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <map>
#include <vector>
using namespace std;

#include <gsl/gsl_rng.h>

#include "htslib/sam.h"

typedef pair<int,string> Genus;

class Table {
  public:
    Table(size_t n = 0) : table(n,make_pair(0.0,0)) {}
    virtual ~Table() {}

    inline void resize(size_t n) { table.resize(n,make_pair(0.0,0)); }

    inline double operator[](size_t i) { return table[i].first; }
    inline void inc(size_t i, double val) { 
      table[i].first += val; 
      table[i].second++;
    }

    void print(ostream& out = cout, int ignore = 0);
    void read_genus_map(string filename);
    void read_name_map(string filename);
    void read_lengths(string filename);

    void randomize(gsl_rng* rng);

    string genus(string name);

    vector< pair<double,size_t> > table;
    map<string,Genus> gmap;
    map<string,size_t> lengths;
    map<string,int> plasmid;
    vector<string> names;
    map<string,int> taxmap;

    size_t all = 0;
    size_t mapped = 0;
    size_t passed = 0;
    bam_hdr_t* header = NULL;
};


