#include <iostream>
#include <string>
#include <vector>
using namespace std;

int is_subset(const string& a, const string& b) {
  size_t n = a.size();
  if (b.size() < n) n = b.size();

  int ndiff = 0;

  for (size_t i = 0; i < n; ++i) {
    if (b[i] != a[i] && a[i] == '1') ndiff++;
  }

  return ndiff;
}

int main() {
  vector<string> patterns;
  vector<int> nsnps;
  vector<int> cnt;

  string p;

  do {
    cin >> p;

    if (! cin.eof()) {
      patterns.push_back(p);
      nsnps.push_back(0);
      cnt.push_back(0);

      size_t i = patterns.size()-1;
      cin >> nsnps[i] >> cnt[i];

      cout << i << " ";
      // cout << patterns[i] << " "
      cout << cnt[i] << " " << nsnps[i] << " ";

      if (i > 0) {
        size_t j = i-1;
        for (; j >= 0; --j) {
          if (! is_subset(patterns[j],patterns[i])) {
            cout << j << endl;
            break;
          }
        }
        if (j < 0) cout << "-" << endl;
      } else {
        cout << "-" << endl;
      }
    }
  } while (! cin.eof());

  return 0;
}
