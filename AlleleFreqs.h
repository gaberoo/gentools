#ifndef __ALLELEFREQS_H__
#define __ALLELEFREQS_H__

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <map>
using namespace std;

#include "hts.h"

namespace dna_tools {
  inline double plogp(double x) { return (x==0.0) ? 0.0 : -x*log(x); }

  class AF {
    public:
      AF() {}
      AF(const AF& af) 
        : cnt(af.cnt), pA(af.pA), pG(af.pG), pT(af.pT), pC(af.pC)
      {}
      virtual ~AF() {}

      inline double maxFreq() const {
        double m1 = (pG > pA) ? pG : pA;
        double m2 = (pC > pT) ? pC : pT;
        return (m1 > m2) ? m1 : m2;
      }

      inline int whichMax() const {
        double m1 = 0.0, m2 = 0.0;
        int i1 = 0, i2 = 0;
        if (pG > pA) { m1 = pG; i1 = 2; } else { m1 = pA; i1 = 1; }
        if (pC > pT) { m2 = pC; i2 = 4; } else { m2 = pT; i2 = 3; }
        return (m1 > m2) ? i1 : i2;
      }

      inline double entropy() const {
        return -plogp(pA) - plogp(pG) - plogp(pT) - plogp(pC);
      }

      inline string str() const {
        ostringstream out;
        out << cnt << " (" << pA << "," << pG << "," << pT << "," << pC << ") "
            << whichMax();
        return out.str();
      }

      inline void normalize() {
        double sum = pA+pG+pT+pC;
        pA /= sum;
        pG /= sum;
        pT /= sum;
        pC /= sum;
      }

      inline char callBase() const {
        switch (whichMax()) {
          case 1: return 'A'; break;
          case 2: return 'G'; break;
          case 3: return 'T'; break;
          case 4: return 'C'; break;
          default: return 'N'; break;
        }
      }

      size_t cnt = 0;
      double pA = 0.0;
      double pG = 0.0;
      double pT = 0.0;
      double pC = 0.0;
  };

  class AlleleFreqs {
    public:
      AlleleFreqs() {}

      void read(istream& in, int skip = 0, size_t min_depth = 0);
      void read_BCF(string filename, string region = "");
      size_t FST(const AlleleFreqs& that, 
                 vector<double>& fst, 
                 vector<double>& s2,
                 int& hd, size_t min_depth = 3) const;
      pair<double,size_t> HS(size_t min_depth = 3) const;

      string consensus() const;
      void printConsensus(size_t w = 60) const;

      string id = "";
      map<size_t,AF> freqs;
  };
}

#endif
