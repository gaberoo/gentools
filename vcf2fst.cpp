#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
using namespace std;

#include "hts.h"
#include "AlleleFreqs.h"

#include "cxxopts.hpp"
using namespace cxxopts;

Options options("vcf2fst", "Calculate FST from VCF/BCF files");

string parse_filename(string filename) {
  // parse filename
  stringstream fn(filename);
  string id = "";
  while (getline(fn,id,'/'));
  stringstream name(id);
  vector<string> last;
  while (getline(name,id,'.')) last.push_back(id);
  if (last.size() > 1) last.pop_back();
  return accumulate(last.begin(),last.end(),string(""));
}

int main(int argc, char* argv[]) {
  vector<string> files;
  vector<size_t> depths;
  string region = "";

  options.add_options()
    ("f,filename", "BCF files",    cxxopts::value< vector<string> >(files))
    ("d,depth",    "Depth cutoff", cxxopts::value< vector<size_t> >(depths))
    ("r,region",   "Region",       cxxopts::value<string>(region))
    ("h,help",     "Display help")
  ;

  vector<string> _pos = { "filename" };
  options.parse_positional(_pos);
  options.positional_help("1.bam 2.bam [3.bam] ...");

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    cout << options.help(show_help) << endl;
    exit(0);
  }

  if (depths.size() == 0) {
    cerr << "Please secify at least one depth cutoff." << endl;
    exit(0);
  }

  /**************************************************************************/

  vector<dna_tools::AlleleFreqs> A;
  string filename;

  for (auto filename: files) {
    A.push_back(dna_tools::AlleleFreqs());
    dna_tools::AlleleFreqs& af = A.back();
    cerr << "Reading " << filename << "..." << endl;
    af.id = parse_filename(filename);
    af.read_BCF(filename,region);
  }

  vector< pair<size_t,size_t> > ids;
  for (size_t i = 1; i < A.size(); ++i) {
    for (size_t j = 0; j < i; ++j) {
      ids.push_back(make_pair(i,j));
    }
  }

  for (auto min_depth: depths) {
    size_t k;
#pragma omp parallel for shared(A)
    for (k = 0; k < ids.size(); ++k) {
      size_t i = ids[k].first;
      size_t j = ids[k].second;
      int hd;
      vector<double> fst;
      vector<double> x;
      size_t cnt = A[i].FST(A[j],fst,x,hd,min_depth);
      double fst_tot = accumulate(fst.begin(),fst.end(),0.0);
      double x_tot = accumulate(x.begin(),x.end(),0.0);
#pragma omp critical
      cout << A[i].id << " " << A[j].id << " " << min_depth << " "
           << cnt << " " << fst_tot << " " << fst_tot/cnt << " " 
           << hd << " " << 1.0*hd/cnt << " " << x_tot/cnt << endl;
    }
  }

  return 0;
}
