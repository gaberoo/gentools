#!/usr/bin/env ruby

require 'json'
require 'optparse'

options = {
  samtools: "samtools"
}

OptionParser.new do |opts|
  opts.banner = "Usage: fasta_concat.rb [options]"
  opts.on("-S", "--samtools [PATH]", "Path to samtools") { |s| options[:samtools] = s }
  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| options[:verbose] = v }
  opts.on("-k", "--keys [FILE]", "Specify keys") { |k| options[:keys] = k }
  opts.on("-f", "--force", "Force index generation") { |f| options[:force] = f }
end.parse!

indices = {}
lengths = {}
keys = []

if options[:keys]
  STDERR.puts options[:keys]
  File.open options[:keys] do |file|
    keys = file.read.split(/[\n\r]+/)
  end
end
#files = ARGV.map{ |x| x.split }.flatten

input = ARGV
input += $stdin.read.split(/\s+/) unless STDIN.tty?

files = input.map do |file|
  if File.exists?(file) and File.size(file) > 0
    file
  else
    nil
  end
end.compact

# read indices
files.each do |file|
  idx_name = "#{file}.fai"
  if options[:force] or not File.exists?(idx_name)
    STDERR.puts "Generating index..." if options[:verbose]
    `#{options[:samtools]} faidx "#{file}"` 
  end
  indices[file] = {}
  File.open idx_name do |index|
    index.each_line do |line|
      row = line.split
      indices[file][row[0]] = {
        length: row[1].to_i,
        start: row[2].to_i,
        row_len: row[3].to_i,
        row_size: row[4].to_i
      }
    end
  end
  lengths[file] = indices[file].collect{ |k,v| v[:length] }.max
  keys |= indices[file].keys unless options[:keys]
end

keys.each do |key|
  info = files.map{ |file| "#{File.basename file}:#{lengths[file]}" }.join(";")

  puts ">#{key} #{info}"

  files.each do |file|
    STDERR.puts "#{key} #{file}" if options[:verbose]
    File.open file do |fasta|
      idx = indices[file][key]
      if idx
        read_nrow = idx[:length] / idx[:row_len]
        read_len = read_nrow * idx[:row_size]
        read_len += idx[:length] - read_nrow * idx[:row_len]
        fasta.seek idx[:start]
        seq = fasta.read(read_len).gsub(/\s+/,"")
        seq = seq.ljust(lengths[file], 'n')
        print seq
      else
        print "".ljust(lengths[file], 'n')
      end
    end
  end
  print "\n"
end

