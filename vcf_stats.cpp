#include "vcf_stats.h"

/****************************************************************************/

size_t nchars(const string& seq) {
  size_t cnt = 0;
  string::const_iterator c = seq.begin();
  while (c++ != seq.end()) { 
    switch (*c) {
      case 'a': case 'A':
      case 'c': case 'C':
      case 'g': case 'G':
      case 't': case 'T':
        ++cnt;
        break;
      default:
        break;
    }
  }
  return cnt;
}

/****************************************************************************/

int delta_pl(int n, int* pl) {
  if (n > 2) {
    vector<size_t> pos(n);
    gsl_sort_int_index(pos.data(),pl,1,n);
    return pl[pos[1]]-pl[pos[0]];
  } else if (n == 2) {
    return abs(pl[0]-pl[1]);
  } else {
    return 0;
  }
}

/****************************************************************************/

int main(int argc, char* argv[]) {
  vector<bool> exclude;
  vector<string> sample_names;

  int nsamp = 0;
  int verbose = 0;

  double cutoff = 0.67;

  cxxopts::Options options("vcf_stats", "VCF statistics");
  options.positional_help("action input_file");

  vector<string> filt_names;

  options.add_options()
    ("a,action",   "Action", cxxopts::value<string>()->default_value("consensus"))
    ("f,filename", "Input file", cxxopts::value<string>())
    ("o,output",   "Output file", cxxopts::value<string>())
    ("c,chr",      "Chromosome", cxxopts::value<string>()->default_value("-"))
    ("b,begin",    "Chr begin", cxxopts::value<int>()->default_value("-1"))
    ("e,end",      "Chr end", cxxopts::value<int>()->default_value("-1"))
    ("h,header",   "Header prefix", cxxopts::value<string>()->default_value("S"))
    ("name",       "Only operate on this sample (filtered name)", cxxopts::value< vector<string> >(filt_names))
    ("exclude",    "Exclude samples", cxxopts::value<string>()->default_value(""))
    ("regex",      "Regex for sample ID filtering", cxxopts::value<string>())
    ("groups",     "Grouping for samples", cxxopts::value<string>())
    ("group-id",   "Grouping column", cxxopts::value<int>()->default_value("1"))
    ("nameRegex",  "Regex for name parsing", cxxopts::value<string>()->default_value(".*\\/([\\w_\\-]+)\\.bam$"))
    ("d,min-depth", "Minimum depth", cxxopts::value<int>()->default_value("1"))
    ("D,min-ref-depth", "Minimum depth for reference allele", cxxopts::value<int>()->default_value("1"))
    ("q,min-qual", "Minimum quality", cxxopts::value<double>()->default_value("0.0"))
    ("Q,min-sample-qual", "Minimum per sample quality", cxxopts::value<double>()->default_value("0.0"))
    ("threshold",  "Cutoff threshold", cxxopts::value<double>()->default_value("0.1"))
    ("gff",        "GFF file", cxxopts::value<string>())
    ("v,verbose",  "Verbose")
    ("help",       "Print help")
    ("C,cutoff", "Cutoff to consider a sites segregating", cxxopts::value<double>(cutoff))
    ("blockSize",  "Block size to analyze", cxxopts::value<int>())
    ("window",     "Window size", cxxopts::value<int>())
    ("use-gt",     "Use genotype calls")
    ("use-ref",    "Use reference for missing genotypes")
    ("bed",        "Only consider regions in BED file", cxxopts::value<string>())
    ("ref",        "FASTA reference file", cxxopts::value<string>())
    ("rescale-cov", "Rescale the coverage estimate", cxxopts::value<double>()->default_value("1.0"))
  ;

  options.add_options("segSites")
    ("m,merge", "Merge all samples")
    ("P,pl",    "Use PL values")
    ("probs",   "Use probabilities to merge")
    ("minCov",  "Minimum coverage across samples", cxxopts::value<double>()->default_value("0.0"))
  ;

  options.add_options("Histogram options")
    ("bins", "Number of histogram bins", cxxopts::value<int>()->default_value("20"))
  ;

  options.add_options("consensus")
    ("only-variable", "Only output variable sites")
    ("consensus-all", "Include empty entries")
    ("consensus-noalign", "Don't align variants and keep insertions")
  ;

  options.add_options("count-table")
    ("count-table-groups", "Sample-to-group mapping", cxxopts::value<string>()->default_value(""))
    ("count-table-all", "Count all sites")
  ;

  options.add_options("zib")
    ("zib-long", "SNPs long format")
  ;

  options.add_options("coverage")
    ("coverage-outliers", "Only show outlier regions")
    ("coverage-min-freq", "Minimum coverage", cxxopts::value<double>()->default_value("0.0"))
  ;

  options.add_options("diversity")
    ("div-cutoff", "Threshold probability", cxxopts::value<double>()->default_value("1e-2"))
    ("div-alpha",  "Beta-Binomial alpha",   cxxopts::value<double>()->default_value("1.0"))
    ("div-beta",   "Beta-Binomial beta",    cxxopts::value<double>()->default_value("1.0"))
  ;

  options.add_options("fst12")
    ("fst-error", "Error probability", cxxopts::value<double>()->default_value("0.0"))
  ;

  vector<string> _pos = { "action", "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "", "Histogram options", "segSites" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  string bcf_file  = options["filename"].as<string>();
  string chr       = options["chr"].as<string>();

  verbose = options.count("verbose");

  /* open files for reading *************************************************/

  if (verbose) cerr << "Opening file: " << bcf_file << "..." << flush;
  htsFile* bcf1 = hts_open(bcf_file.c_str(),"r");
  if (verbose) cerr << "done." << endl;

  if (bcf1 == NULL) {
    cerr << "Unable to open file " << bcf_file << "." << endl;
    return 0;
  }

  if (verbose) cerr << "Opening header..." << flush;
  bcf_hdr_t* header = bcf_hdr_read(bcf1);

  if (header == NULL) {
    cerr << "Unable to read header." << endl;
    return 0;
  }
  if (verbose) cerr << "done." << endl;

  bcf_srs_t* sr = NULL;
  if (chr != "-") {
    if (verbose) cerr << "Selecting chromosome '" << chr << "'..." << flush;
    sr = bcf_sr_init();
    bcf_sr_set_regions(sr,chr.c_str(),0);
    bcf_sr_add_reader(sr,bcf_file.c_str());
    if (verbose) cerr << "done." << endl;
  }

  nsamp = bcf_hdr_nsamples(header);
  if (verbose) cerr << "# samples = " << nsamp << endl;

  exclude.resize(nsamp,false);

  /* translate names ********************************************************/

  string regex_nm_s = options["nameRegex"].as<string>();
  sample_names.resize(nsamp,"");

  if (regex_nm_s != "") {
    if (verbose) cerr << "Translating names: " << regex_nm_s << endl;

    regex regex_nm_r(regex_nm_s);
    smatch regex_nm_m;

    for (int i = 0; i < nsamp; ++i)  {
      string name_s = bcf_hdr_int2id(header,BCF_DT_SAMPLE,i);
      sample_names[i] = name_s;

      if (regex_match(name_s,regex_nm_m,regex_nm_r)) {
        if (regex_nm_m.size() == 2) {
          sample_names[i] = regex_nm_m[1].str();
        }
      }
    }
  } else {
    for (int i = 0; i < nsamp; ++i)  {
      sample_names[i] = bcf_hdr_int2id(header,BCF_DT_SAMPLE,i);
    }
  }

  /* include/exclude samples ************************************************/

  string exclude_str = options["exclude"].as<string>();
  if (verbose) cerr << "Checking for exclusions: " << exclude_str << endl;

  if (exclude_str != "") {
    istringstream exclude_s(exclude_str);
    string token;
    while (getline(exclude_s, token, ',')) {
      int exclude_i = atoi(token.c_str());
      if (exclude_i < nsamp && exclude_i >= 0) {
        if (verbose) cerr << "Excluding '" << exclude_i << "'" << endl;
        exclude[exclude_i] = false;
      }
    }
  }

  string regex_s = options["regex"].as<string>();
  if (verbose) cerr << "Checking for regex exclusions: " << regex_s << endl;

  if (regex_s != "") {
    regex regex_r(regex_s);
    for (int i = 0; i < nsamp; ++i) {
      string name = bcf_hdr_int2id(header,BCF_DT_SAMPLE,i);
      if (regex_match(name,regex_r)) {
        if (verbose) cerr << "Excluding '" << name << "'" << endl;
        exclude[i] = true;
      }
    }
  }

  if (filt_names.size() > 0) {
    ostringstream hdr_names;
    for (size_t i = 0; i < filt_names.size(); ++i) {
      auto it = find(sample_names.begin(),sample_names.end(),filt_names[i]);
      if (it != sample_names.end()) {
        int i = distance(sample_names.begin(),it);
        if (i > 0) hdr_names << ",";
        hdr_names << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i);
      }
    }

    int ret = bcf_hdr_set_samples(header,hdr_names.str().c_str(),0);

    if (ret < 0) {
      cerr << "Error setting samples." << endl;
      abort();
    } else if (ret > 0) {
      cerr << "Sample " << ret << " not found." << endl;
      abort();
    }
  }

  /* perform action *********************************************************/

  if (verbose) cerr << "Initiating record..." << endl;
  bcf1_t* record = bcf_init();

  string action = options["action"].as<string>();
  if (verbose) cerr << "Calling action: " << action << endl;

  /*
  if (action == "segSites") {
    if (options.count("merge")) seg_sites_multi();
    else if (options.count("pl")) seg_sites_pl();
    else seg_sites();
  }
  else if (action == "segHist") seg_hist();
  else if (action == "segList") seg_list();
  else if (action == "siteHist") site_hist();
  else*/
  if (action == "consensus")
    consensus(bcf1,header,sr,record,sample_names,options);
  else if (action == "coverage") {
    if (options.count("rescale-cov")) {
      coverage_scaled(bcf1,header,sr,record,sample_names,options);
    } else {
      coverage(bcf1,header,sr,record,sample_names,options);
    }
  }
  else if (action == "sliding-coverage")
    sliding_coverage(bcf1,header,sr,record,sample_names,options);
  else if (action == "clonal") 
    mean_hs(bcf1,header,sr,record,sample_names,options);
  else if (action == "variable-all") 
    variable_sites_all(bcf1,header,sr,record,sample_names,options);
  else if (action == "zib") 
    zib(bcf1,header,sr,record,sample_names,options);
  else if (action == "zib4") 
    zib_dp4(bcf1,header,sr,record,sample_names,options);
  else if (action == "true-snps") 
    true_snps(bcf1,header,sr,record,sample_names,options);
  else if (action == "stats") {
    if (options.count("gff"))
      gff_stats(bcf1,header,sr,record,sample_names,options);
    else
      stats(bcf1,header,sr,record,sample_names,options);
  }
  else if (action == "pairs") 
    check_pairs(bcf1,header,sr,record,sample_names,options);
  else if (action == "pair-dist")
    pair_dist(bcf1,header,sr,record,sample_names,options);
  else if (action == "count-table")
    count_table(bcf1,header,sr,record,sample_names,options);
  else if (action == "fst")
    fst(bcf1,header,sr,record,sample_names,options);
  else if (action == "diversity")
    diversity(bcf1,header,sr,record,sample_names,options);

  hts_close(bcf1);

  // if (sr != NULL) bcf_sr_destroy(sr);
  bcf_hdr_destroy(header);
  bcf_destroy(record); 
  
  return 0;
}

/****************************************************************************/

