#include <string>
#include <sstream>
#include <iomanip>

inline std::string pretty_bytes(size_t bytes) {
  const char* suffixes[7] = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
  size_t s = 0;
  double count = bytes;
  std::ostringstream out;
  while (count >= 1024 && s < 7) {
    s++;
    count /= 1024;
  }
  out << std::setprecision(2) << count << " " << suffixes[s];
  return out.str();
}
