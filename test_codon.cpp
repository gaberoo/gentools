#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

#include "codon.h"

int main() {
  /*
  char str[] = "CAG";
  uint32_t codon = str2cod(str);
  cout << str << endl;
  cout << codon << endl;
  printf("%u | %u | %u\n",nuc1(codon),nuc2(codon),nuc3(codon));
  printf("%c %c %c\n",int2char(nuc1(codon)),int2char(nuc2(codon)),int2char(nuc3(codon)));
  printf("%c\n",coduo2aa(codon));
  */

  /*
  uint32_t c1 = str2cod("nnn");
  uint8_t n1 = nuc1(c1);
  uint8_t n2 = nuc2(c1);
  uint8_t n3 = nuc3(c1);
  cout << "nnn = " << c1 << endl;
  printf("nnn(1) = %u | %c\n",n1,int2char(n1));
  printf("nnn(2) = %u | %c\n",n2,int2char(n2));
  printf("nnn(3) = %u | %c\n",n3,int2char(n3));
  return 0;
  */

  
  char str[] = "AGTCRYKMSWBDHVN";
  char aa_str[] = "---";

  printf("typedef enum codon_table {\n");
  for (size_t i = 0; i < strlen(str); ++i) {
    for (size_t j = 0; j < strlen(str); ++j) {
      for (size_t k = 0; k < strlen(str); ++k) {
        uint32_t codon = coduo(char2int8(str[i]),char2int8(str[j]),char2int8(str[k]));
        char aa = coduo2aa(codon);
        cod2str(codon,aa_str);
        // printf("%c%c%c = %u > %c (%s)\n",str[i],str[j],str[k],codon,aa,aa_str);
        if ((i+j+k) > 0) printf(",\n");
        printf("  HTS_%c%c%c = %u",str[i],str[j],str[k],codon);
      }
    }
  }
  printf("\n} codon_t;\n");

  return 0;
}
