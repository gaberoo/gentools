#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <array>
#include <map>
#include <regex>
using namespace std;

#include "cxxopts.hpp"

#include "duo.h"
#include "hts.h"
#include "FaIdx.h"

void filter(FaIdx& index, const cxxopts::Options& options);
void dropMissing(FaIdx& index, const cxxopts::Options& options);
void rename(FaIdx& index, const cxxopts::Options& options);

/****************************************************************************/

int main(int argc, char** argv) {
  string action = "info";
  string filename = "";

  cxxopts::Options options("fasta_filter", "Filter FASTA files");

  options.positional_help("[options] <action> <input.fasta[.gz]>");
  options.add_options()
    ("a,action", "Action", cxxopts::value<string>(action))
    ("f,input", "Input file", cxxopts::value<string>(filename), "FILE")
    ("b,beg", "Region start", cxxopts::value<int>()->default_value("0"))
    ("e,end", "Region end",   cxxopts::value<int>()->default_value("0"))
    ("load", "Loading window", cxxopts::value<int>()->default_value("100000"))
    ("subset", "Subset sequences", cxxopts::value<string>()->default_value(""))
    ("h,help", "Show help")
    ("v,verbose", "Verbose")
  ;

  options.add_options("filter")
    ("filter-uninf", "Filter uninformative")
  ;

  options.add_options("drop")
    ("drop-min", "Minimum bases", cxxopts::value<double>()->default_value("-1.0"))
  ;

  options.add_options("rename")
    ("rename-search", "Search regex", cxxopts::value<string>()->default_value(""))
    ("rename-replace", "Replace regex", cxxopts::value<string>()->default_value(""))
  ;

  vector<string> _pos = { "action", "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  /**************************************************************************/

  FaIdx index(filename);

  string subset = options["subset"].as<string>();
  if (subset != "") index.mark_seqs(subset);

  if (action == "info") cout << index.info() << endl;
  else if (action == "view") {
    index.load_all(rbeg,rend);
    for (size_t i = 0; i < index.size(); ++i) {
      printf(">%s\n%s\n",index[i].c_str(),index(i));
    }
  }
  else if (action == "filter") filter(index,options);
  else if (action == "drop") dropMissing(index,options);
  else if (action == "rename") rename(index,options);

  return 0;
}

/****************************************************************************/

void filter(FaIdx& index, const cxxopts::Options& options) {
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");
  vector<size_t> positions;

  int b = options["beg"].as<int>();
  int e = options["end"].as<int>();

  int beg = (b > 0) ? b : 1;

  int end = index.max_full_len();
  if (e > beg && e < end) end = e;

  if (end-beg+1 < load_window) load_window = end-beg+1;

  for (; beg < end; beg += load_window) {
    if (verbose) {
      cerr << "Loading sequences " 
           << beg << "-" << beg+load_window-1 << "..." << flush;
    }
    index.load_all(beg,beg+load_window-1);
    if (verbose)  cerr << "done." << endl;

    int k, j;
    for (k = beg, j = 0; k < beg+load_window; ++k, ++j) {

      auto cnts = index.counts(k);

      int zeros = 0;
      for (auto c: cnts) zeros += (c == 0);
      if (zeros < 3) positions.push_back(k);
    }
  }

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;

    index.load(i);

    cout << ">" << index[i] << endl;
    for (auto k: positions) cout << index(i,k);
    cout << endl;

    index.unload(i);
  }
}

/****************************************************************************/

void dropMissing(FaIdx& index, const cxxopts::Options& options) {
  double min_bases = options["drop-min"].as<double>();
  int verbose = options.count("verbose");

  int maxLen = index.max_full_len();
  if (verbose) cerr << "Max length = " << maxLen << endl;

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;
    index.load(i);
    int n = index.nucleotides(i);
    if (verbose) cerr << i << ") " << n << " bases: " << flush;
    if (1.0*n/index.length(i) > min_bases) {
      if (verbose) cerr << "keep" << endl;
      cout << ">" << index[i] << endl;
      cout << index(i);
      for (int k = index.length(i); k < maxLen; ++k) cout << 'n';
      cout << endl;
    } else {
      if (verbose) cerr << "drop" << endl;
    }
  }
}

/****************************************************************************/

void rename(FaIdx& index, const cxxopts::Options& options) {
  regex re(options["rename-search"].as<string>());
  string replace = options["rename-replace"].as<string>();
  int maxLen = index.max_full_len();

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;
    cout << ">" << regex_replace(index[i],re,replace) << endl;
    cout << index(i);
    for (int k = index.length(i); k < maxLen; ++k) cout << 'n';
    cout << endl;
  }
}


