#!/usr/bin/env ruby

require 'json'
require 'optparse'

options = {
  samtools: "samtools"
}

OptionParser.new do |opts|
  opts.banner = "Usage: fasta_concat.rb [options]"
  opts.on("-S", "--samtools [PATH]", "Path to samtools") { |s| options[:samtools] = s }
  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| options[:verbose] = v }
end.parse!

files = ARGV
indices = {}
lengths = {}
keys = []

keep = STDIN.read.split
print keep if options[:verbose]

# read indices
files.each do |file|
  idx_name ="#{file}.fai"
  `#{options[:samtools]} faidx #{file}` unless File.exists? idx_name
  indices[file] = {}
  File.open idx_name do |index|
    index.each_line do |line|
      row = line.split
      indices[file][row[0]] = {
        length: row[1].to_i,
        start: row[2].to_i,
        row_len: row[3].to_i,
        row_size: row[4].to_i
      }
    end
  end
  lengths[file] = indices[file].collect{ |k,v| v[:length] }.max
  keys |= indices[file].keys
end

keys.each do |key|
  if keep.include? key
    puts ">#{key}"
    files.each do |file|
      File.open file do |fasta|
        idx = indices[file][key]
        if idx
          read_nrow = idx[:length] / idx[:row_len]
          read_len = read_nrow * idx[:row_size]
          read_len += idx[:length] - read_nrow * idx[:row_len]
          fasta.seek idx[:start]
          seq = fasta.read(read_len).gsub(/\s+/,"")
          seq = seq.ljust(lengths[file], 'n')
          print seq
        else
          print "".ljust(lengths[file], 'n')
        end
      end
    end
    print "\n"
  end
end

