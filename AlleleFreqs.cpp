#include "AlleleFreqs.h"

void dna_tools::AlleleFreqs::read(istream& in, int skip, size_t min_depth)
{
  string line;
  size_t ival = 0, cnt = 0;
  double val = 0.0;
  map<size_t,AF>::iterator ret;
  while (skip-- > 0) getline(in,line);
  while (getline(in,line)) {
    istringstream stream(line);
    stream >> ival >> cnt;
    if (cnt >= min_depth) {
      ret = freqs.insert(freqs.end(),make_pair(ival,AF()));
      AF& pt = ret->second;
      pt.cnt = cnt;
      stream >> val >> val >> pt.pA >> pt.pG >> pt.pT >> pt.pC;
      // cout << line << endl;
      // cout << pt.str() << endl;
      pt.normalize();
    }
  }
}

/****************************************************************************/

void dna_tools::AlleleFreqs::read_BCF(string filename, string region) {
  htsFile *bcf1 = hts_open(filename.c_str(),"r");
  if (bcf1 == NULL) { cerr << "Unable to open file 1." << endl; return; }

  bcf_hdr_t *h1 = bcf_hdr_read(bcf1);
  if (h1 == NULL) { cerr << "Unable to read header 1." << endl; return; }

  bcf_srs_t* sr = NULL;
  if (region != "") {
    sr = bcf_sr_init();
    bcf_sr_set_regions(sr,region.c_str(),0);
    bcf_sr_add_reader(sr,filename.c_str());
  }

  bcf1_t *rec1 = bcf_init();
  double p1[4];
  map<size_t,AF>::iterator ret;

  int bcf_ret = -1;
  if (sr == NULL) bcf_ret = read_next(bcf1,h1,rec1);
  else bcf_ret = read_next_idx(sr,h1,&rec1);

  while (bcf_ret == 0) {
    ret = freqs.insert(freqs.end(), make_pair(rec1->pos,AF()));
    AF& pt = ret->second;
    pt.cnt = info2p(p1,h1,rec1);
    pt.pA = p1[0];
    pt.pG = p1[1];
    pt.pT = p1[2];
    pt.pC = p1[3];

    if (sr == NULL) bcf_ret = read_next(bcf1,h1,rec1);
    else bcf_ret = read_next_idx(sr,h1,&rec1);
  }

  hts_close(bcf1);
  bcf_hdr_destroy(h1);
  bcf_destroy(rec1); 
}

/****************************************************************************/

string dna_tools::AlleleFreqs::consensus() const {
  size_t n = freqs.rend()->first;
  string out(n,'n');
  map<size_t,AF>::const_iterator it = freqs.begin();
  for (; it != freqs.end(); ++it) {
    out[it->first] = (it->second).callBase();
  }
  return out;
}

/****************************************************************************/

void dna_tools::AlleleFreqs::printConsensus(size_t w) const {
  size_t n = freqs.rend()->first;
  map<size_t,AF>::const_iterator it = freqs.begin();
  for (size_t i = 1; i <= n; ++i) {
    if (it->first == i) cout << (it++->second).callBase();
    else cout << 'n';
    if (w > 0 && i % w == 0) cout << endl;
  }
}

/****************************************************************************/
/* Calculation of FST using the method from Boca & Rosenberg                *
 * See 10.1534/genetics.112.144758                                          */

size_t dna_tools::AlleleFreqs::FST(const AlleleFreqs& that, 
    vector<double>& fst, vector<double>& sigma2,
    int& hd, size_t min_depth) const 
{
  size_t cnt = 0;

  map<size_t,AF>::const_iterator r1 = freqs.begin();
  map<size_t,AF>::const_iterator r2 = that.freqs.begin();

  double sigma[4];
  double delta[4];

  fst.clear();
  sigma2.clear();
  hd = 0;

  while (r1 != freqs.end() && r2 != that.freqs.end()) {
    if (r1->first == r2->first &&
        (r1->second).cnt >= min_depth &&
        (r2->second).cnt >= min_depth)
    {
      ++cnt;

      sigma[0] = (r1->second).pA + (r2->second).pA;
      sigma[1] = (r1->second).pG + (r2->second).pG;
      sigma[2] = (r1->second).pT + (r2->second).pT;
      sigma[3] = (r1->second).pC + (r2->second).pC;

      delta[0] = fabs((r1->second).pA - (r2->second).pA);
      delta[1] = fabs((r1->second).pG - (r2->second).pG);
      delta[2] = fabs((r1->second).pT - (r2->second).pT);
      delta[3] = fabs((r1->second).pC - (r2->second).pC);

      double d2 = delta[0]*delta[0] + delta[1]*delta[1]
                  + delta[2]*delta[2] + delta[3]*delta[3];
      double s2 = sigma[0]*sigma[0] + sigma[1]*sigma[1]
                  + sigma[2]*sigma[2] + sigma[3]*sigma[3];

      double F = (s2<4.0) ? d2/(4.0-s2) : 0.0;
      fst.push_back(F);

      double x2 = 0.0;
      x2 += (r1->second).pA*(r2->second).pA;
      x2 += (r1->second).pG*(r2->second).pG;
      x2 += (r1->second).pT*(r2->second).pT;
      x2 += (r1->second).pC*(r2->second).pC;
      sigma2.push_back(x2);

      if ((r1->second).whichMax() != (r2->second).whichMax()) ++hd;

      ++r1;
      ++r2;
    } else if (r1->first < r2->first) {
      ++r1;
    } else {
      ++r2;
    }
  }

  return cnt;
}

pair<double,size_t> 
dna_tools::AlleleFreqs::HS(size_t min_depth) const 
{
  size_t cnt = 0;
  double H = 0.0;
  for (const auto& r: freqs) {
    if (r.second.cnt >= min_depth) {
      ++cnt;
      H += r.second.pA*r.second.pA;
      H += r.second.pG*r.second.pG;
      H += r.second.pT*r.second.pT;
      H += r.second.pC*r.second.pC;
    }
  }
  return make_pair(H/cnt,cnt);
}

