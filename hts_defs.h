#ifndef __HTS_DEFS_H__
#define __HTS_DEFS_H__

#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif

#define HTS_GAP  0b00000000
#define HTS_NONE 0b00000000

#define HTS_A    0b00000001
#define HTS_G    0b00000010
#define HTS_T    0b00000100
#define HTS_C    0b00001000

#define HTS_R    0b00000011
#define HTS_Y    0b00001100
#define HTS_K    0b00000110
#define HTS_M    0b00001001
#define HTS_S    0b00001010
#define HTS_W    0b00000101

#define HTS_B    0b00001110
#define HTS_D    0b00000111
#define HTS_H    0b00001101
#define HTS_V    0b00001011

#define HTS_ANY  0b00001111

#define HTS_ZERO 0b00010000
#define HTS_ONE  0b00100000

const static int hts_cols[5][3] = { 
  { 0xe4, 0x1a, 0x1c },
  { 0x37, 0x7e, 0xb8 },
  { 0x4d, 0xaf, 0x4a },
  { 0x98, 0x4e, 0xa3 },
  { 0xff, 0x7f, 0x00 }
};

/****************************************************************************/

int idx(char a);
int char2int(char a);
char int2char(int a);
char compChar(char a);

#ifdef __cplusplus
}
#endif

#endif
