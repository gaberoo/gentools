include Make.inc

CPPFLAGS = $(CFLAGS) -std=c++17 

.PHONY: clean htslib

htslib: $(HTSLIB)/libhts.a

$(HTSLIB)/libhts.a:
	cd $(HTSLIB)
	./configure
	make

OBJ = Sequence.o FastaFile.o Table.o

FASTA_SRC = $(wildcard fasta/*.cpp)
FASTA_OBJ = $(patsubst fasta/%.cpp, fasta/%.o, $(FASTA_SRC))

VCF_SRC = $(wildcard vcf/*.cpp)
VCF_OBJ = $(patsubst vcf/%.cpp, vcf/%.o, $(VCF_SRC))

GFF_SRC = $(wildcard gff/*.c)
GFF_OBJ = $(patsubst gff/%.c, gff/%.o, $(GFF_SRC))

FEXACT_OBJ = fexact.o prterr.o rcont.o

SEQAN_INC = -Iseqan3 -DSEQAN3_HAS_ZLIB=1 -DSEQAN3_HAS_BZIP2=1
SEQAN_LD = -lz -lbz2 -lstdc++fs -pthreads

# Main programs ##############################################################

fasta_stats: fasta_stats.o hts_defs.o codon.o FaIdx.o pngwriter.o $(FASTA_OBJ) $(FEXACT_OBJ) \
	htslib
	$(CXX) $(CPPFLAGS) $(FT2_CFLAGS) -o $@ $(filter-out htslib,$^) \
		$(HTSLIB_LIB) $(GSL) $(FT2_LIBS) $(PNG_LIBS) -lgfortran

vcf_stats: vcf_stats.o hts_defs.o codon.o FaIdx.o hts.h $(VCF_OBJ) $(GFF_OBJ) \
	edlib/edlib.o $(FEXACT_OBJ) htslib
	$(CXX) $(CPPFLAGS) $(FT2_CFLAGS) -o $@ $(filter-out htslib,$^) \
		$(HTSLIB_LIB) $(GSL) $(FT2_LIBS) $(PNG_LIBS) -lgfortran

# Old leftovers ##############################################################

fasta_filter: fasta_filter.o FaIdx.o
	$(CXX) $(CPPFLAGS) -o $@ $^ $(HTSLIB_LIB)

fasta_cor: fasta_cor.o FaIdx.o
	$(CXX) $(CPPFLAGS) -o $@ $^ $(HTSLIB_LIB) $(GSL)
		
dna_dist: dna_dist.o
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $^

dna_stats: dna_stats.o $(OBJ)
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $^

fa2phy: fa2phy.o $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $^

dropSites: dropSites.o $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $^ $(GSL)

phylip_to_fasta: phylip_to_fasta.cpp Phylip.h
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $<

tetnuc: tetnuc.o $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $< $(OBJ)

make_tetnuc_table: make_tetnuc_table.o $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $< $(OBJ)

dna_lookup: dna_lookup.o $(OBJ)
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $^

run_dmix: run_dmix.cpp dmix.cpp
	$(CXX) $(CPPFLAGS) -g -o $@ $^ $(GSL)

clean_fasta: clean_fasta.cpp $(OBJ)
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $^

dna_dist_mem: dna_dist_mem.o $(OBJ)
	$(CXX) $(CPPFLAGS) -fopenmp -o $@ $^

af2fst: af2fst.cpp
	$(CXX) -Wall -O3 -m64 -std=c++11 -fopenmp -o $@ $^

af2fasta: af2fasta.cpp
	$(CXX) -Wall -O3 -m64 -std=c++11 -o $@ $^

fastaFrag: fastaFrag.cpp $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $^ $(GSL)

fastaPad: fastaPad.cpp $(OBJ)
	$(CXX) $(CPPFLAGS) -o $@ $^

pileupper: pileupper.cpp PileUp.o
	$(CXX) $(CPPFLAGS) -o $@ $^ $(GSL)

sfs: sfs.cpp
	$(CXX) $(CPPFLAGS) -o $@ $^ $(GSL)

# HTSlib

# BAM

parse_bam: parse_bam.cpp
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(HTSLIB_LIB)

bam_filter: bam_filter.cpp
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(HTSLIB_LIB)

ref_reads: ref_reads.cpp
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(HTSLIB_LIB)

bamcnt: bamcnt.cpp Table.o
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(GSL) $(HTSLIB_LIB)

# VCF

vcf_stats-debug: vcf_stats.cpp hts.h
	$(CXX) $(CPPFLAGS) -DDEBUG=2 -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2 -L/usr/local/lib $(GSL)

parse_vcf: parse_vcf.cpp
	$(CXX) -Wall -O3 -m64 -std=c++14 -o $@ $^ $(HTSLIB_LIB)

vcf2fst: vcf2fst.o AlleleFreqs.o
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(HTSLIB_LIB) $(GSL)

vcf2hs: vcf2hs.o AlleleFreqs.o
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ $(HTSLIB_LIB) $(GSL)

id_len: id_len.cpp hts.h
	$(CXX) -fpic -O2 -Wall -m64 -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2

splitPileUp: splitPileUp.cpp hts.h
	$(CXX) -fpic -O2 -Wall -m64 -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2 $(GSL)

vcfRefDist: vcfRefDist.cpp hts.h
	$(CXX) -fpic -O2 -Wall -m64 -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2

vcfMulti: vcfMulti.cpp hts.h
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2 -L/usr/local/lib $(GSL)

vcfMaxFreq: vcfMaxFreq.cpp hts.h
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2 -L/usr/local/lib $(GSL)

vcfConsensus: vcfConsensus.cpp hts.h
	$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $< $(HTSLIB_LIB)

test_max_index: test_max_index.cpp
	$(CXX) -fpic -O2 -Wall -m64 -I$(HTSLIB) -o $@ $< $(HTSLIB)/libhts.a -llzma -lcurl -lz -lbz2

tree_likelihood: tree_likelihood.c
	clang -Wall -I/usr/local/include/libpll -o $@ $< -L/usr/local/lib -lpll

#$(CXX) $(CPPFLAGS) -I$(HTSLIB) -o $@ $^ -L$(HTSLIB) -lhtsa -llzma -lcurl -lz -lbz2

pattern_graph: pattern_graph.cpp
	$(CXX) $(CPPFLAGS) -o $@ $^

contig_cor: contig_cor.cpp
	$(CXX) $(CPPFLAGS) -o $@ $^ $(GSL)

# test codon #################################################################

test_codon: test_codon.o hts_defs.o codon.o
	$(CXX) $(CPPFLAGS) -o $@ $^ 

make_codon_table_h: make_codon_table_h.o hts_defs.o codon.o
	$(CXX) $(CPPFLAGS) -o $@ $^ 

# requires FT2 ###############################################################

fasta_stats.o: fasta_stats.cpp
	$(CXX) $(CPPFLAGS) $(FT2_CFLAGS) -c -o $@ $^

pngwriter.o: pngwriter.cc
	$(CXX) $(CPPFLAGS) $(FT2_CFLAGS) -c -o $@ $^

fasta/raster_png.o: fasta/raster_png.cpp
	$(CXX) $(CPPFLAGS) $(FT2_CFLAGS) -c -o $@ $^

##############################################################################

libpll:
	cd libpll-2-0.3.2 && ./autogen.sh && ./configure CC="$(CC)" CXX="$(CXX)" --prefix=$(shell pwd)/libpll && make install

##############################################################################

.f.o:
	$(FC) -c -o $@ $^

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $^

.cpp.o:
	$(CXX) $(CPPFLAGS) -c -o $@ $^

clean:
	$(RM) *.o edlib/edlib.o $(FASTA_OBJ) $(VCF_OBJ) $(GFF_OBJ)


