#include <string>

int extract_length(const string& line) {
  string token = "";
  int inside = 0;

  for (size_t i = 0; i < line.length() && inside < 3; ++i) {
    if (inside == 0) {
      // find first separator
      if (line[i] == ' ' || line[i] == '\t') inside = 1;
    } else if (inside == 1) {
      // find start of token
      if (line[i] != ' ' && line[i] != '\t') {
        inside = 2;
        token += line[i];
      }
    } else {
      // add until end of token
      if (line[i] != ' ' && line[i] != '\t') {
        token += line[i];
      } else {
        inside = 3;
      }
    }
  }

  return stoi(token);
}


