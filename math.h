#include "math.h"

inline double logit(double x) { return log(x) - log1p(x); }

inline double unlogit(double y) { double ey = exp(y); return ey/(ey+1.0); }

inline double log_sum_exp(double u, double v) {
  double max_uv = (u > v) ? u : v;
  return max_uv + log(exp(u-max_uv)) + log(exp(v-max_uv));
}

