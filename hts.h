#ifndef __HTS_H__
#define __HTS_H__

#include <iostream>
#include <cmath>
#include <array>
using namespace std;

#include "htslib/sam.h"
#include "htslib/vcf.h"
#include "htslib/vcfutils.h"
#include "htslib/synced_bcf_reader.h"

#include <gsl/gsl_math.h>

#include "hts_defs.h"

inline string compStr(const char* str, size_t n) {
  string out = "";
  for (size_t k = 0; k < n; ++k) {
    switch (toupper(str[k])) {
      case 'A': out += 'T'; break;
      case 'a': out += 't'; break;
      case 'T': out += 'A'; break;
      case 't': out += 'a'; break;
      case 'G': out += 'C'; break;
      case 'g': out += 'c'; break;
      case 'C': out += 'G'; break;
      case 'c': out += 'g'; break;
      case '0': out += '1'; break;
      case '1': out += '0'; break;
      default: out += str[k]; break;
    }
  }
  return out;
}

inline int pureInt(int a) {
  switch (a) {
    case HTS_A:
    case HTS_C:
    case HTS_G:
    case HTS_T:
      return 1;

    case HTS_ANY:
      return -1;
      
    default:
      return 0;
  }
}

/****************************************************************************/

template<typename T, size_t len>
inline int get_major(const array<T,len>& cnts) {
  int i1 = 0, i2 = 0;
  T cnt1 = 0, cnt2 = 0;

  if (cnts[0] > cnts[1])      { i1 = HTS_A; cnt1 = cnts[0]; }
  else if (cnts[1] > cnts[0]) { i1 = HTS_G; cnt1 = cnts[1]; }
  else                        { i1 = HTS_A | HTS_G; cnt1 = cnts[0]; }

  if (cnts[2] > cnts[3])      { i2 = HTS_T; cnt2 = cnts[2]; }
  else if (cnts[3] > cnts[2]) { i2 = HTS_C; cnt2 = cnts[3]; }
  else                        { i2 = HTS_T | HTS_C; cnt2 = cnts[2]; }

  if (cnt2 > cnt1)            { i1 = i2; cnt1 = cnt2; }
  else if (cnt2 == cnt1)      { i1 = i1 | i2; }
  
  return (cnt1 > 0) ? i1 : HTS_ANY;
}

/****************************************************************************/

template<typename T>
inline int array_max(const T* arr, int n)
{
  T m = arr[0];
  int k = 0;
  for (int i = 1; i < n; ++i) {
    if (m < arr[i]) { m = arr[i]; k = i; }
  }
  return k;
}

/****************************************************************************/

inline string bit2str(bam1_t* aln) {
  uint8_t* bits = bam_get_seq(aln);
  int32_t len = aln->core.l_qseq;
  string seq;
  for (int i = 0; i < len; ++i) {
    switch (bam_seqi(bits,i)) {
      case 0b0001: seq += 'A'; break;
      case 0b0010: seq += 'C'; break;
      case 0b0100: seq += 'G'; break;
      case 0b1000: seq += 'T'; break;
      default: seq += 'n'; break;
    }
  }
  return seq;
}

/****************************************************************************/

inline void calc_sigma(const double* p1, const double* p2, double* sigma) {
  sigma[0] = p1[0] + p2[0];
  sigma[1] = p1[1] + p2[1];
  sigma[2] = p1[2] + p2[2];
  sigma[3] = p1[3] + p2[3];
}

/****************************************************************************/

inline void calc_delta(const double* p1, const double* p2, double* delta) {
  delta[0] = fabs(p1[0] - p2[0]);
  delta[1] = fabs(p1[1] - p2[1]);
  delta[2] = fabs(p1[2] - p2[2]);
  delta[3] = fabs(p1[3] - p2[3]);
}

/****************************************************************************/

inline double pow_sum(const double *p, size_t n, int pow) {
  double x = 0.0;
  for (size_t i = 0; i < n; ++i) {
    x += gsl_pow_int(p[i],pow);
  }
  return x;
}

/****************************************************************************/

inline double calc_fst(const double* p1, const double* p2) {
  double s[4];
  double d[4];

  calc_sigma(p1,p2,s);
  calc_delta(p1,p2,d);

  double ss = s[0]*s[0] + s[1]*s[1] + s[2]*s[2] + s[3]*s[3];
  double sd = d[0]*d[0] + d[1]*d[1] + d[2]*d[2] + d[3]*d[3];

  double F = (ss < 4.0) ? sd/(4.0-ss) : 0.0;

  return F;
}

/****************************************************************************/

inline unsigned int max_index(const double* p) {
  double m1, m2;
  unsigned int i1, i2;
  if (p[0] > p[1]) { 
    m1 = p[0]; 
    i1 = HTS_A; 
  } else if (p[0] == p[1]) {
    m1 = p[0];
    i1 = HTS_A | HTS_G; 
  } else { 
    m1 = p[1]; 
    i1 = HTS_G; 
  }
  if (p[2] > p[3]) { 
    m2 = p[2]; 
    i2 = HTS_T; 
  } else if (p[2] == p[3]) {
    m2 = p[2];
    i2 = HTS_T | HTS_C;
  } else { 
    m2 = p[3]; 
    i2 = HTS_C; 
  }
  if (m1 > m2) {
    return i1;
  } else if (m1 == m2) {
    return i1 | i2;
  } else {
    return i2;
  }
}

/****************************************************************************/

inline int max_allele_len(int n_allele, char** allele) {
  int len = 0;
  int l2 = 0;
  for (int i = 0; i < n_allele; ++i) {
    l2 = strlen(allele[i]);
    if (l2 == 3 && allele[i][0] == '<' && allele[i][1] == '*' && allele[i][2] == '>') l2 = 0;
    if (len < l2) len = l2;
  }
  return len;
}
  
/****************************************************************************/

inline int ctot(int* ad, int* c, int n_allele, char** allele) {
  int ct = 0;

  for (int i = 0; i < n_allele; ++i) {
    if (strlen(allele[i]) != 1) continue;
    const char *al = allele[i];
    int id = idx(*al);
    if (id >= 0) c[id] += ad[i];
    ct += ad[i];
  }

  return ct;
}

/****************************************************************************/

inline int ptot(int* ad, double* p, int n_allele, char** allele) {
  int ptot = 0;
  memset(p,0,4*sizeof(double));

  for (int i = 0; i < n_allele; ++i) {
    if (strlen(allele[i]) != 1) continue;
    const char *al = allele[i];
    int id = idx(*al);
    if (id >= 0) p[id] += ad[i];
    ptot += ad[i];
  }

  if (ptot > 0.0) {
    p[0] /= ptot;
    p[1] /= ptot;
    p[2] /= ptot;
    p[3] /= ptot;
  }

  return ptot;
}

/****************************************************************************/

inline int info2p(double* p, bcf_hdr_t* header, bcf1_t* record) {
  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  ret = bcf_get_info_int32(header,record,"AD",&ad,&ndst);
  if (ret < 0) {
    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
  }

  if (ret > 0) {
    return ptot(ad,p,record->n_allele,record->d.allele);
  } else {
    return -1;
  }
}

/****************************************************************************/

inline int read_next(htsFile* f, bcf_hdr_t* h, bcf1_t* r) {
  int ret = -1;
  int type = 0;
  do {
    ret = bcf_read(f,h,r);
    if (ret == 0) type = bcf_get_variant_type(r,1);
  } while (ret == 0 && type > 2);
  return ret;
}

/****************************************************************************/

inline int read_next_idx(bcf_srs_t *sr, bcf_hdr_t* h, bcf1_t** r) {
  int ret = -1;
  int type = 0;
  do {
    ret = bcf_sr_next_line(sr);
    if (ret > 0) {
      *r = bcf_sr_get_line(sr, 0);
      type = bcf_get_variant_type(*r,1);
    }
  } while (ret > 0 && type > 2);
  return (ret <= 0) ? -1 : 0;
}

/****************************************************************************/

inline size_t mat2lin(size_t i, size_t j, size_t n) {
  if (i > j) { size_t tmp = i; i = j; j = tmp; }
  return (n*(n-1)/2) - (n-i)*((n-i)-1)/2 + j - i-1;
}

inline array<size_t,2> lin2mat(size_t k, size_t n) {
  array<size_t,2> ij;
  ij[0] = n-2 - floor(sqrt(-8*k + 4*n*(n-1)-7)/2.0 - 0.5);
  ij[1] = k+ij[0]+1 - n*(n-1)/2 + (n-ij[0])*((n-ij[0])-1)/2;
  return ij;
}

/****************************************************************************/

#endif
