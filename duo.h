#ifndef __duo_h__
#define __duo_h__

/*
 * These helper functions efficiently store integer pairs, but using the first
 * 32 bits of an 64 bit integer for the first value and the second 32 bits for
 * the second value
 */

inline uint64_t duo(uint32_t a, uint32_t b) { 
  uint64_t _a = a;
  uint64_t _b = b;
  return (_b << 32) | _a; 
}

inline uint32_t duo1(uint64_t p) { return p & 0xFFFFFFFF; }
inline uint32_t duo2(uint64_t p) { return p >> 32; }

#endif
