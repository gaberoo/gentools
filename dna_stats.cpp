#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"
#include "bitmap_image.hpp"

double plogp(double x) { return (x==0.0) ? 0.0 : -x*log(x); }

void sfs(dna_tools::FastaFile& f, int bins);
void freq_summary(dna_tools::FastaFile& f);
void freq_all(dna_tools::FastaFile& f);
void consensus(dna_tools::FastaFile& f, string name = "");
void count(dna_tools::FastaFile& f);
void binary(dna_tools::FastaFile& f);

cxxopts::Options options("dna_stats", "Calucluate alignment statistics");
size_t interval = 1;
string count_type = "";
int reg_beg = 0;
int reg_end = 0;

int main(int argc, char** argv) {
  options.positional_help("<type> filename");

  options.add_options()
    ("f,input",    "Input file", cxxopts::value<string>(), "FILE")
    ("i,index",    "Index file", cxxopts::value<string>()->default_value(""))
    ("t,type",     "Stat type",  cxxopts::value<string>()->default_value(""))
    ("beg",        "Start pos",  cxxopts::value<int>(reg_beg))
    ("end",        "End pos",    cxxopts::value<int>(reg_end))
    ("v,verbose",  "Verbose")
    ("h,help",     "Print help")
  ;

  options.add_options("cons")
    ("cons-name",  "Consensus name", cxxopts::value<string>()->default_value("Consensus"))
  ;

  options.add_options("sfs")
    ("sfs-bins",  "Histogram bins", cxxopts::value<int>(), "N")
  ;

  options.add_options("summary")
    ("cutoff",  "Cutoff for segregating site", cxxopts::value<double>()->default_value("0.6666667"))
  ;

  options.add_options("count")
    ("interval", "Interval", cxxopts::value<size_t>(interval))
    ("countType", "Count type", cxxopts::value<string>(count_type))
  ;

  vector<string> _pos = { "type", "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  // -------------------------------------------------------------------------

  string filename = options["input"].as<string>();
  string index = options["index"].as<string>();
  string type = options["type"].as<string>();

  string prefix = "";
  if (options.count("cons-name")) prefix = options["cons-name"].as<string>();

  int sfs_bins = 10;
  if (options.count("sfs-bins")) sfs_bins = options["sfs-bins"].as<int>();

  if (options.count("verbose") > 1) cerr << "Reading index...";
  dna_tools::FastaFile f(filename,index);
  if (options.count("verbose") > 1) cerr << "done." << endl;

  if (options.count("verbose")) cerr << "Reading sequences...";
  f.read_seqs();
  if (options.count("verbose")) cerr << "done." << endl;

  vector<double> maxFreq;
  vector<double> allDiv;
  vector<int> cnt;

  if      (type == "freqs")     { freq_all(f); }
  else if (type == "summary")   { freq_summary(f); }
  else if (type == "cons")      { consensus(f, prefix); }
  else if (type == "sfs")       { sfs(f, sfs_bins); }
  else if (type == "diversity") { f.allDiv(allDiv, cnt); }
  else if (type == "count")     { count(f); }
  else if (type == "binary")    { binary(f); }

  return 0;
}

// ===========================================================================

void freq_summary(dna_tools::FastaFile& f) {
  vector<double> maxFreq;
  vector<int> cnt;

  double cutoff = options["cutoff"].as<double>();

  f.maxFreq(maxFreq,cnt);

  size_t nTot = maxFreq.size();
  size_t nSegSites = 0;

  vector<double>::const_iterator cx = maxFreq.begin();
  while (cx++ != maxFreq.end()) nSegSites += (*cx < cutoff);
  cout << nTot << " " << nSegSites << " " << 1.*nSegSites/nTot << endl;
}

// ===========================================================================

void sfs(dna_tools::FastaFile& f, int bins) {
  vector<double> maxFreq;
  vector<int> cnt;
  f.maxFreq(maxFreq, cnt);

  vector<int> bin_cnt(bins+1,0);

  int all_cnt = 0;
  int id;

  for (size_t i = 0; i < maxFreq.size(); ++i) {
    if (cnt[i] > 1) {
      id = maxFreq[i]*bins;
      bin_cnt[id]++;
      all_cnt++;
    }
  }

  for (size_t i = 0; i < bin_cnt.size(); ++i) {
    cout << i << " " << bin_cnt[i] << " " << 1.0*bin_cnt[i]/all_cnt << endl;
  }
}

// ===========================================================================

void freq_all(dna_tools::FastaFile& f) {
  vector<double> maxFreq;
  vector<int> cnt;

  f.maxFreq(maxFreq,cnt);

  for (size_t i = 0; i < maxFreq.size(); ++i) {
    cout << i << " " << cnt[i] << " " << maxFreq[i] << endl;
  }
}

// ===========================================================================

void consensus(dna_tools::FastaFile& f, string name) {
  if (name == "") name = "Consensus";
  cout << ">" << name << endl;
  cout << f.consensus() << endl;
}

// ===========================================================================

void binary(dna_tools::FastaFile& f) {
  if (reg_end > reg_beg) {
    for (int k = reg_beg; k <= reg_end; ++k) {
      char m = f.major(k);
      cout << k << " ";
      for (size_t i = 0; i < f.size(); ++i) {
        char c = toupper(f.get(i,k));
        switch (c) {
          case 'A': case 'G':
          case 'T': case 'C':
            if (c == m) cout << 1 << " ";
            else cout << 0 << " ";
            break;
          default:
            cout << "- ";
            break;
        }
      }
      cout << endl;
    }
  } else {
    for (size_t k = 0; k < f.max_len(); ++k) {
      char m = toupper(f.major(k));
      for (size_t i = 0; i < f.size(); ++i) {
        char c = toupper(f.get(i,k));
        switch (c) {
          case 'A': case 'G':
          case 'T': case 'C':
            if (c != m) printf("%lu\t%lu\t%c\t%c\t%s\n",k,i,c,m,f[i].name().c_str());
            break;
          default:
            break;
        }
      }
    }
  }
}

// ===========================================================================

void count(dna_tools::FastaFile& f) {
  if (count_type == "individual") {

    for (size_t i = 0; i < f.size(); ++i) {
      cout << i << " " << f[i].count() << endl;
    }

  } else if (count_type == "matrix") {

    for (size_t i = 0; i < f.max_len(); i += interval) {
      for (size_t j = 0; j < f.size(); ++j) {
        size_t cnt = 0;
        for (size_t k = i; k < i+interval; ++k) {
          switch (f[j][k]) {
            case 'A': case 'a': case 'G': case 'g':
            case 'T': case 't': case 'C': case 'c':
              cnt++;
              break;
            default:
              break;
          }
        }
        cout << 1.0*cnt/interval << " ";
      }
      cout << endl;
    }

  } else if (count_type == "bitmap") {

    size_t bsize = 10;
    size_t n = f.max_len();
    size_t m = f.size();
    size_t nbin = n/bsize;
    if (n % bsize) nbin++;

    size_t width = bsize*nbin;
    size_t height = bsize*m;

    bitmap_image image(width,height);

    for (size_t i = 0; i < f.max_len(); i += interval) {
      size_t ival = i/interval;
      for (size_t j = 0; j < f.size(); ++j) {
        size_t cnt = 0;
        for (size_t k = i; k < i+interval; ++k) {
          switch (f[j][k]) {
            case 'A': case 'a': case 'G': case 'g':
            case 'T': case 't': case 'C': case 'c':
              cnt++;
              break;
            default:
              break;
          }
        }
        uint8_t val = 1.0*cnt/interval/m*255;
        for (size_t x = ival*bsize; x < (ival+1)*bsize; ++x) {
          for (size_t y = j*bsize; y < (j+1)*bsize; ++y) {
            image.set_pixel(x,y,val,val,val);
          }
        }
      }
    }

    image.save_image("output.bmp");


  } else {

    size_t cnt = 0;
    for (size_t i = 0; i < f.max_len(); ++i) {
      size_t c = f.count(i);
      cnt += c;
      if ((i+1) % interval == 0) {
        cout << i << " " << 1.0*cnt/interval << endl;
        cnt = 0;
      }
    }

  }
}

