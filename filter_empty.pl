#!/usr/bin/env perl

use warnings;
use strict;

local $/ = '>';

while (<>) {
  chomp $_;
  my @row = split /[\n\r]+/, $_;
  if (@row) {
    my $id = shift @row;
    my $seq = join('',@row);
    if ($seq =~ /[agct]/i) {
      print ">$id\n$seq\n";
    }
  }
}

