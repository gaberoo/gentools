#include "gff.h"

int gff1_free(gff1_t* gff) { 
  if (gff->core.seqname != NULL) free(gff->core.seqname);
  if (gff->core.source != NULL) free(gff->core.source);
  if (gff->core.feature != NULL) free(gff->core.feature);
  if (gff->core.score != NULL) free(gff->core.score);
  if (gff->core.attribute_s != NULL) free(gff->core.attribute_s);
  free(gff); 
  return 0;
}

