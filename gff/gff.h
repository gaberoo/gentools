#ifdef __cplusplus
extern "C" {
#endif

#include "stdlib.h"

typedef struct gff_core {
  char* seqname;
  char* source;
  char* feature;
  int32_t start;
  int32_t end;
  char* score;
  char strand;
  char frame;
  char* attribute_s;
} gff_core_t;

typedef struct {
  gff_core_t core;
} gff1_t;

int gff1_free(gff1_t* gff);

gff1_t* parse_gff1(const char raw[]);

#ifdef __cplusplus
}
#endif
