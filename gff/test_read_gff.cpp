#include <iostream>
#include <fstream>
#include <string>
#include <regex>
using namespace std;

#include "GFFFile.h"

int main(int argc, char** argv) {
  GFFFile gff(argv[1]);

  gff.read_gff();

  cout << gff[0]->core.attribute_s << endl;

  return 0;
}
