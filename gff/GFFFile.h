#ifndef __GFFFILE_H__
#define __GFFFILE_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <regex>
#include <vector>
using namespace std;

#include "gff.h"

class GFFFile : public vector<gff1_t*> {
public:
  GFFFile() {}
  GFFFile(string fn) : filename(fn) {}
  GFFFile(const char fn[]) : filename(fn) {}
  virtual ~GFFFile() { clear(); }

  inline void clear() {
    while (size() > 0) {
      gff1_free(back());
      pop_back();
    }
  }

  inline void read_gff() {
    ifstream in(filename);
    string line;

    // get first line
    getline(in,line);
    regex regex_hdr("^##gff-version\\s+([\\d.]+)");
    smatch regex_hdr_m;
    if (regex_match(line,regex_hdr_m,regex_hdr)) {
      if (regex_hdr_m.size() == 2) gff_version = regex_hdr_m[1].str();
    }

    if (gff_version == "") cerr << "No GFF version specified." << endl;

    string token;
    size_t pos;
    bool in_seq_reg = false;

    while (getline(in,line)) {
      if (line[0] == '#') {
        pos = line.find_first_of(" \t\n");
        if (pos != string::npos) {
          token = line.substr(0,pos);
          if (token == "##sequence-region") {
            in_seq_reg = true;
          } else {
            in_seq_reg = false;
          }
        } else {
          in_seq_reg = false;
        }
      } else if (in_seq_reg) {
        push_back(parse_gff1(line.c_str()));
      }
    }
  }
  
  string filename = "";
  string gff_version = "";
};

#endif
