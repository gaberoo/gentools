#include "gff.h"
#include "string.h"

gff1_t* parse_gff1(const char raw[]) {
  char* line = strdup(raw);

  gff1_t* gff = (gff1_t*) malloc(sizeof(gff1_t));

  char* token;
  char delim[] = " \t\n";

  char* attr;
  char* key, val;

  token = strtok(line, delim);
  if (token != NULL) {
    gff->core.seqname = strdup(token);
  }

  if ((token = strtok(NULL, delim))) { gff->core.source = strdup(token); }
  if ((token = strtok(NULL, delim))) { gff->core.feature = strdup(token); }
  if ((token = strtok(NULL, delim))) { gff->core.start = atoi(token); }
  if ((token = strtok(NULL, delim))) { gff->core.end = atoi(token); }
  if ((token = strtok(NULL, delim))) { gff->core.score = strdup(token); }
  if ((token = strtok(NULL, delim))) { gff->core.strand = token[0]; }
  if ((token = strtok(NULL, delim))) { gff->core.frame = token[0]; }

  if ((token = strtok(NULL, delim))) { 
    gff->core.attribute_s = strdup(token); 
    // while ((attr = strtok(token, ';'))) sscanf(attr, "%[^=]=%s", key, val);
  }

  free(line);

  return gff;
}


