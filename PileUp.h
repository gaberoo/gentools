#ifndef __PILEUP_H__
#define __PILEUP_H__

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
using namespace std;

#define PU_ALLELE 1
#define PU_INDEL  2 

/****************************************************************************/

int sum1(int* alleles);
int max1(int* alleles);
int cnt1(int* alleles);
int seg1(int* alleles);
void add1(int* a1, const int* a2);
void norm1(double* p, const int* a);

/****************************************************************************/

class Variant {
  public:
    Variant(int type) : type(type) {}
    Variant(char b) : type(PU_ALLELE), base(toupper(b)) {}
    Variant(int type, string indel) : type(type), indel(indel) {}

    size_t parse_indel(size_t pos, string match);

    char type = 0;
    char base = 'n';
    string indel = "";
    int qual = 0;
};

/****************************************************************************/

class PileUpEntry {
  public:
    void parse(string line, bool mapQual = true);
    istream& parse(istream& in, bool mapQual = true);

    void unpack();

    friend ostream& operator<<(ostream& out, const PileUpEntry& entry);
    friend istream& operator>>(istream& in, PileUpEntry& entry);

    void cnt(int* alleles, int min_qual = 20);

    int num_alleles(int min_qual = 20);
    double max_freq(int min_qual = 20);
    int max_cnt(int min_qual = 20);
    int is_segregating(int min_qual = 20);

    string name = "";
    size_t pos = 0;
    char ref = 'n';
    int depth = 0;
    string match = "";
    string baseQual = "";
    string mapQual = "";

    int phred = 33;

    vector<Variant> variants;

    int packed = -1;
};

/****************************************************************************/

class MPileUp : public vector<PileUpEntry> {
  public:
    void addEntry(string name = "", int group = 0);
    void parse(string line, bool mapQual = true, bool grow = true);
    int max_cnt(int min_qual);
    int is_segregating(int min_qual);
    int depth() const;

    vector<string> names;
    vector<int> groups;
    int phred = 33;
};

#endif
