#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
using namespace std;

#include "Phylip.h"
#include "FastaFile.h"

int main(int argc, char** argv) {
  if (argc < 2) return EXIT_FAILURE;

  Phylip phy;
  phy.read(argv[1]);

  for (int i = 0; i < phy.len; ++i) {
    int cov = phy.coverage(i);
    cout << i << " " << cov << " " << endl;
  }

  return 0;
}

