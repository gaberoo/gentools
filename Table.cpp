#include "Table.h"

string Table::genus(string name) {
  std::map< string, Genus >::const_iterator it;
  it = gmap.find(name);
  if (it != gmap.end()) return it->second.second;
  else return "";
}

void Table::print(ostream& out, int ignore) {
  map< string, Genus >::const_iterator it;
  map< string, map<int,double> > grouped;
  map< string, map<int,double> >::iterator group;

  for (size_t i = 0; i < table.size(); ++i) {
    string name = header->target_name[i];

    // lookup name if requested
    if (names.size() > 0) name = names[atoi(name.c_str())];

    it = gmap.find(name);
    if (it != gmap.end()) {
      const string& gname = it->second.second;
      group = grouped.find(gname);
      if (group == grouped.end()) {
        auto _new = grouped.insert(make_pair(gname,map<int,double>()));
        group = _new.first;
      }
      (group->second)[taxmap[name]] += table[i].first;
    } else if (ignore == 0) {
      group = grouped.find(name);
      if (group == grouped.end()) {
        auto _new = grouped.insert(make_pair(name,map<int,double>()));
        group = _new.first;
      }
      (group->second)[taxmap[name]] += table[i].first;
    }
  }

  for (const auto& g: grouped) {
    double total = 0.0;
    double H = 0.0;
    size_t S = 0;

    for (const auto& s: g.second) {
      total += s.second;
      if (s.second > 0.0) ++S;
    }

    for (const auto& s: g.second) {
      if (s.second > 0.0) {
        H -= s.second/total * log(s.second/total);
      }
    }

    out << g.first << "\t" 
        << total << "\t"
        << 1.0*total/all << "\t"
        << 1.0*total/mapped << "\t"
        << H << "\t" 
        << g.second.size() << "\t"
        << S << "\t"
        << ((S>1) ? H/log(S) : 1.0)
        << endl;
  }
}

void Table::read_genus_map(string filename) {
  string line;
  ifstream in(filename.c_str());
  int id, tax_id;
  string acc, name;
  while (getline(in,line)) {
    istringstream tokens(line);
    tokens >> id >> acc >> tax_id >> name;
    gmap[acc] = make_pair(id, name);
    taxmap[acc] = tax_id;
  }
}

void Table::read_name_map(string filename) {
  string line;
  ifstream in(filename.c_str());
  int id;
  string name;
  while (getline(in,line)) {
    istringstream tokens(line);
    tokens >> id >> name;
    names.push_back(name);
  }
}

void Table::read_lengths(string filename) {
  string line;
  ifstream in(filename.c_str());
  string accession;
  string genus;
  size_t length;
  int is_plasmid;
  while (getline(in,line)) {
    istringstream tokens(line);
    tokens >> accession >> genus >> length >> is_plasmid;
    lengths[accession] = length;
    plasmid[accession] = is_plasmid;
  }
}

void Table::randomize(gsl_rng* rng) {
  size_t n = table.size();
  vector<double> weights(n+1,0.0);
  size_t cnt = 0;
  for (size_t i = 0; i < n; ++i) {
    weights[i] = table[i].second;
    cnt += table[i].second;
  }
  weights[n] = all - cnt;
}

