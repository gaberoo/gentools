#include <cstdlib>
#include <cmath>
#include <cstring>

#include <iostream>
#include <stdexcept>
#include <vector>
#include <numeric>
#include <map>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

#include "hts.h"
#include "htslib/sam.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "cxxopts.hpp"

#include "Table.h"

typedef enum _action_t { 
  DEFAULT, 
  TABLE, 
  HIST, 
  STATS, 
  FASTQ,
  MAP,
} action_t;

Table table;

void make_map(const map<string,string>& query_map, string dbFile);

// options are global so they can be accessed by any of the subroutines
cxxopts::Options options("bamcnt", "Summarize BAM files");

int main(int argc, char* argv[]) {
  options.positional_help("list|hist|table filename");

  options.add_options()
    ("a,action", "Action to perform", cxxopts::value<string>(), "[hist,table]")
    ("f,filename", "Input BAM file", cxxopts::value<string>(), "FILE")
    ("m,mean", "Calculate mean across all matches")
    ("b,bonus", "Match bonus", cxxopts::value<float>()->default_value("1"), "FLOAT")
    ("v,verbose", "Verbose")
    ("h,help", "Print help")
  ;

  options.add_options("hist")
    ("H,histBins", "Number of histogram bins", cxxopts::value<int>()->default_value("10"), "N")
    ("histLo", "Histogram lower bound", cxxopts::value<float>()->default_value("0.0"), "FLOAT")
    ("histHi", "Histogram upper bound", cxxopts::value<float>()->default_value("1.0"), "FLOAT")
  ;

  options.add_options("table")
    ("c,cutoff", "Score cutoff", cxxopts::value<float>(), "FLOAT")
    ("G,genera", "Genus Map File", cxxopts::value<string>(), "FILE")
    ("N,names", "Name Map File", cxxopts::value<string>(), "FILE")
    ("L,lengths", "Length File", cxxopts::value<string>(), "FILE")
    ("plasmids", "Include plasmids")
    ("ignore", "Ignore entries without genus")
  ;

  options.add_options("fastq");

  options.add_options("map")
    ("db", "BAM file to compare against", cxxopts::value<string>(), "FILE")
  ;

  vector<string> _pos = { "action", "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  int verbose = options.count("verbose");

  vector<size_t> index;
  vector<string> name;

  string action_s = options["action"].as<string>();
  string filename = options["filename"].as<string>();
  double bonus = options["bonus"].as<float>();
  // int calc_mean = options.count("mean");
  
  double histLo = options["histLo"].as<float>();
  double histHi = options["histHi"].as<float>();
  double cutoff = options.count("cutoff") ? options["cutoff"].as<float>() : -1.0/0.0;
  int plasmids = options.count("plasmids");

  action_t action = DEFAULT;
  if (action_s == "table") action = TABLE;
  else if (action_s == "hist") action = HIST;
  else if (action_s == "stats") action = STATS;
  else if (action_s == "fastq") action = FASTQ;
  else if (action_s == "map") action = MAP;

  // open BAM for reading
  htsFile *in = hts_open(filename.c_str(), "r");
  if (in == NULL) { cerr << "Unable to open BAM/SAM file." << endl; return 0; }

  // Get the header
  bam_hdr_t *header = sam_hdr_read(in);

  table.header = header;

  if (action == TABLE) {
    // cerr << "Total reference sequences = " << header->n_targets << endl;
    table.resize(header->n_targets);
  }

  if (options.count("genera")) {
    table.read_genus_map(options["genera"].as<string>());
  }

  if (options.count("names")) {
    table.read_name_map(options["names"].as<string>());
  }

  if (options.count("lengths")) {
    if (verbose) cerr << "Reading lengths..." << flush;
    table.read_lengths(options["lengths"].as<string>());
    if (verbose) cerr << table.lengths.size() << endl;
  }
  
  /**************************************************************************/

  gsl_histogram* h = NULL;
  if (action == HIST) {
    h = gsl_histogram_alloc(options["histBins"].as<int>());
    gsl_histogram_set_ranges_uniform(h,histLo,histHi);
  }

  std::map<string,size_t>::const_iterator l;
  std::map<string,int>::const_iterator p;
  std::map<string,string> query_map;

  /**************************************************************************/

  bam1_t *aln = bam_init1();  // Initiate the alignment record
  string last_query = "";
  string query;
  double AS = 0;

  while (sam_read1(in, header, aln) >= 0) {
    query = bam_get_qname(aln);

    if (query != last_query) {
      last_query = query;
      table.all++;

      // check if the read is mapped
      if (aln->core.n_cigar > 0) {
        string name = header->target_name[aln->core.tid];
        if (table.names.size() > 0) name = table.names[atoi(name.c_str())];

        p = table.plasmid.find(name);
        if (!plasmids && table.plasmid.size() > 0 && p != table.plasmid.end()) {
          if (p->second) continue;
        }

        table.mapped++;

        AS = 1.0*bam_aux2i(bam_aux_get(aln,"AS"))/aln->core.l_qseq;
        if (bonus < 0.0) AS = (AS - bonus)/fabs(bonus);
        else AS /= bonus;

        l = table.lengths.find(name);
        double len = (l != table.lengths.end()) ? l->second : 1.0;

        if (len <= 0.0) cerr << "Genome length < 0" << endl;

        // The read-pair matches, then only count the read 1/2
        // ! In the current code, this does not produce the correct results,
        // ! as only the forward reads are counted.
        // ! double score = (aln->core.mtid < 0) ? 1.0 : 0.5;
        double score = 1.0;

        switch (action) {
          case TABLE:
            if (AS >= cutoff) table.inc(aln->core.tid,score/len);

          case STATS:
            if (AS >= cutoff) table.passed++;
            break;

          case HIST:
            if (AS > histHi || AS < histLo) {
              cerr << "Score out of range [" << histLo << "," << histHi << "] ! " << AS << endl;
            }
            gsl_histogram_increment(h,AS);
            break;

          case FASTQ:
            if (AS >= cutoff) {
              cout << "@" << query << " AS=" << AS << endl;
              cout << bit2str(aln) << endl;
              cout << "+" << endl;
              printf("%.*s\n", aln->core.l_qseq, bam_get_qual(aln));
            }
            break;

          case MAP:
            if (AS >= cutoff) {
              query_map[query] = name;
            }
            break;

          case DEFAULT:
          default:
            cout << query << " " << aln->core.tid << " " 
                 << AS << " " << aln->core.l_qseq << " " << name;
            cout << " " << table.genus(name) << " " << len;
            cout << endl;
            break;
        }
      }
    }
  }

  switch (action) {
    case TABLE:
      table.print(cout, options.count("ignore"));
      break;

    case HIST:
      gsl_histogram_fprintf(stdout,h,"%g","%g");
      gsl_histogram_free(h);
      break;

    case STATS:
      cout << table.all << " " 
           << table.mapped << " " 
           << table.passed << endl;
      break;

    default:
      break;
  }

  bam_destroy1(aln);
  bam_hdr_destroy(header);

  sam_close(in);

  if (action == MAP) {
    make_map(query_map, options["db"].as<string>());
  }

  return 0;
}

/****************************************************************************/

template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});
  return idx;
  // for (auto i: sort_indexes(table)) {
}

/****************************************************************************/

void make_map(const map<string,string>& query_map, string dbFile) {
  htsFile *in = hts_open(dbFile.c_str(), "r");
  if (in == NULL) { 
    cerr << "Unable to open BAM/SAM file: " << dbFile << endl;
    return; 
  }

  bam_hdr_t *header = sam_hdr_read(in);
  map<string,string>::const_iterator it;

  string query;
  string name;
  bam1_t *aln = bam_init1();

  while (sam_read1(in, header, aln) >= 0) {
    query = bam_get_qname(aln);
    if (aln->core.tid >= 0) {
      name = header->target_name[aln->core.tid];
      it = query_map.find(query);
      if (it != query_map.end()) {
        cout << query << "\t" 
             << it->second << "\t"
             << table.genus(name) << "\t"
             << name << endl;
      }
    }
  }
}

