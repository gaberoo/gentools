#include <iostream>
#include <stdexcept>
using namespace std;

#include "htslib/vcf.h"
#include "htslib/vcfutils.h"

int idx(char a) {
  switch (a) {
    case 'A':
    case 'a':
      return 0;
    case 'g':
    case 'G':
      return 1;
    case 't':
    case 'T':
      return 2;
    case 'c':
    case 'C':
      return 3;
    default:
      return -1;
  }
}

int main(int argc, char* argv[]) {
  htsFile *bcf = hts_open(argv[1], "r");
  if (bcf == NULL) {
    throw runtime_error("Unable to open file.");
  }

  bcf_hdr_t *header = bcf_hdr_read(bcf);
  if (header == NULL) {
    throw runtime_error("Unable to read header.");
  }

  bcf1_t *record = bcf_init();

  while (bcf_read(bcf, header, record) == 0) {
    // cout << bcf_hdr_id2name(header, record->rid) << " ";

    // check if there was an allele found, i.e. an alignment
    if (record->n_allele > 1) {
      // check for INDEL and skip
      if (bcf_get_variant_type(record,1) == VCF_INDEL) continue;

      bcf_unpack(record,BCF_UN_STR);

      cout << record->pos+1 << " " << record->n_allele << " ";
      cout << record->d.allele[0] << "|";
      for (int i = 1; i < record->n_allele; ++i) {
        cout << record->d.allele[i] << ",";
      }
      cout << " ";

      int *ad;
      int ndst = 0;
      int ret = bcf_get_info_int32(header,record,"AD",&ad,&ndst);
      if (ret < 0) {
        ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
      }

      if (ret > 0) {
        double* p = (double*) calloc(4,sizeof(double));
        double ptot = 0.0;

        for (int i = 0; i < record->n_allele-1; ++i) {
          char *al = record->d.allele[i];
          int id = idx(*al);
          if (id >= 0) p[id] += ad[i];
          ptot += ad[i];
        }

        cout << "(";
        cout << p[0]/ptot << ",";
        cout << p[1]/ptot << ",";
        cout << p[2]/ptot << ",";
        cout << p[3]/ptot;
        cout << ") ";

        free(p);
      }

    }

    cout << endl;
  }

  bcf_hdr_destroy(header);
  bcf_destroy(record); 
  bcf_close(bcf);

  return 0;
}
