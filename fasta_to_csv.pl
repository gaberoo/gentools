#!/usr/bin/env perl

use warnings;
use strict;

local $/ = ">";

while (<>) {
  #my @row = split /[\s;]/, $_;
  my @row = split /\n/, $_;
  #print "$row[0],$row[3]\n";
  if ($#row > 1) {
    for (my $i = 1; $i < $#row; $i++) {
      print $row[$i];
    }
    print "\n";
  }
}

