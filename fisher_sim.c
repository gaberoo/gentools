/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 2001-2016  The R Core Team.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  https://www.R-project.org/Licenses/
 */

#include <math.h>

void rcont2(int *nrow, int *ncol, int *nrowt, int *ncolt, int *ntotal,
            double *fact, int *jwork, int *matrix, double (*unif_rand)());

void fisher_sim(int *nrow, int *ncol, int *nrowt, int *ncolt, int *n,
                int B, int *observed, double *fact,
                int *jwork, double *results,
                double (*unif_rand)())
{
  int i, j, ii, iter;
  double ans;

  /* Calculate log-factorials.  fact[i] = lgamma(i+1) */
  fact[0] = fact[1] = 0.;
  for (i = 2; i <= *n; i++) fact[i] = fact[i - 1] + log(i);

  for (iter = 0; iter < B; ++iter) {
    rcont2(nrow, ncol, nrowt, ncolt, n, fact, jwork, observed, unif_rand);
    /* Calculate log-prob value from the random table. */
    ans = 0.;
    for (j = 0; j < *ncol; ++j) {
      for (i = 0, ii = j * *nrow; i < *nrow;  i++, ii++) ans -= fact[observed[ii]];
    }
    results[iter] = ans;
  }

  return;
}
