#include "codon.h"

uint8_t nuc1(uint32_t codon) { return codon & 0xFFFF; }
uint8_t nuc2(uint32_t codon) { return codon >> 8; }
uint8_t nuc3(uint32_t codon) { return codon >> 16; }

uint32_t coduo(uint8_t n1, uint8_t n2, uint8_t n3) { return (n3 << 16) | (n2 << 8) | n1; }

uint32_t str2cod(const char str[]) {
  uint8_t n1 = char2int8(str[0]);
  uint8_t n2 = char2int8(str[1]);
  uint8_t n3 = char2int8(str[2]);
  return coduo(n1,n2,n3);
}

uint8_t char2int8(char a) {
  switch (a) {
    case 'a': case 'A': return HTS_A;
    case 'g': case 'G': return HTS_G;
    case 't': case 'T': return HTS_T;
    case 'c': case 'C': return HTS_C;
    case 'r': case 'R': return HTS_R;
    case 'y': case 'Y': return HTS_Y;
    case 'k': case 'K': return HTS_K;
    case 'm': case 'M': return HTS_M;
    case 's': case 'S': return HTS_S;
    case 'w': case 'W': return HTS_W;
    case 'b': case 'B': return HTS_B;
    case 'd': case 'D': return HTS_D;
    case 'h': case 'H': return HTS_H;
    case 'v': case 'V': return HTS_V;
    case 'n': case 'N': return HTS_ANY;
    default: return HTS_NONE;
  }
}

void cod2str(uint32_t codon, char str[]) {
  str[0] = int2char(nuc1(codon));
  str[1] = int2char(nuc2(codon));
  str[2] = int2char(nuc3(codon));
}

char coduo2aa(uint32_t codon) {
  switch (codon) {
    case HTS_CODON_TTT:
    case HTS_CODON_TTC:
      return 'F';

    case HTS_CODON_TTA:
    case HTS_CODON_TTG:

    case HTS_CODON_CTT:
    case HTS_CODON_CTC:
    case HTS_CODON_CTA:
    case HTS_CODON_CTG:
    case HTS_CODON_CTN:
      return 'L';

    case HTS_CODON_ATT:
    case HTS_CODON_ATC:
    case HTS_CODON_ATA:
      return 'I';

    case HTS_CODON_ATG:
      return 'M';

    case HTS_CODON_GTT:
    case HTS_CODON_GTC:
    case HTS_CODON_GTA:
    case HTS_CODON_GTG:
    case HTS_CODON_GTN:
      return 'V';

    case HTS_CODON_TCT:
    case HTS_CODON_TCC:
    case HTS_CODON_TCA:
    case HTS_CODON_TCG:
    case HTS_CODON_TCN:
      return 'S';

    case HTS_CODON_CCT:
    case HTS_CODON_CCC:
    case HTS_CODON_CCA:
    case HTS_CODON_CCG:
    case HTS_CODON_CCN:
      return 'P';

    case HTS_CODON_ACT:
    case HTS_CODON_ACC:
    case HTS_CODON_ACA:
    case HTS_CODON_ACG:
    case HTS_CODON_ACN:
      return 'T';

    case HTS_CODON_GCT:
    case HTS_CODON_GCC:
    case HTS_CODON_GCA:
    case HTS_CODON_GCG:
    case HTS_CODON_GCN:
      return 'A';

    case HTS_CODON_TAT:
    case HTS_CODON_TAC:
      return 'Y';

    case HTS_CODON_CAT:
    case HTS_CODON_CAC:
      return 'H';

    case HTS_CODON_CAA:
    case HTS_CODON_CAG:
      return 'Q';

    case HTS_CODON_AAT:
    case HTS_CODON_AAC:
      return 'N';

    case HTS_CODON_AAA:
    case HTS_CODON_AAG:
      return 'K';

    case HTS_CODON_GAT:
    case HTS_CODON_GAC:
      return 'D';

    case HTS_CODON_GAA:
    case HTS_CODON_GAG:
      return 'E';

    case HTS_CODON_TGT:
    case HTS_CODON_TGC:
      return 'C';

    case HTS_CODON_TGG:
      return 'W';

    case HTS_CODON_CGT:
    case HTS_CODON_CGC:
    case HTS_CODON_CGA:
    case HTS_CODON_CGG:
    case HTS_CODON_CGN:
      return 'R';

    case HTS_CODON_AGT:
    case HTS_CODON_AGC:
      return 'S';

    case HTS_CODON_AGA:
    case HTS_CODON_AGG:
      return 'R';

    case HTS_CODON_GGT:
    case HTS_CODON_GGC:
    case HTS_CODON_GGA:
    case HTS_CODON_GGG:
    case HTS_CODON_GGN:
      return 'G';

    case HTS_CODON_TAA:
    case HTS_CODON_TAG:
    case HTS_CODON_TGA:
      // stop codon
      return '*';

    default:
      return 'X';
  }
}

/*
char coduo2aa(uint32_t codon) {
  switch (codon) {
    case 263172: // TTT
    case 525316: // TTC
      return 'F';

    case 66564: // TTA
    case 132100: // TTG

    case 263176: // CTT
    case 525320: // CTC
    case 66568: // CTA
    case 132104: // CTG
    case 984072: // CTN
      return 'L';

    case 263169: // ATT
    case 525313: // ATC
    case 66561: // ATA
      return 'I';

    case 132097: // ATG
      return 'M';

    case 263170: // GTT
    case 525314: // GTC
    case 66562: // GTA
    case 132098: // GTG
    case GTN:
    // case 984066: // GTN
      return 'V';

    case 264196: // TCT
    case 526340: // TCC
    case 67588: // TCA
    case 133124: // TCG
      return 'S';

    case 264200: // CCT
    case 526344: // CCC
    case 67592: // CCA
    case 133128: // CCG
      return 'P';

    case 264193: // ACT
    case 526337: // ACC
    case 67585: // ACA
    case 133121: // ACG
      return 'T';

    case 264194: // GCT
    case 526338: // GCC
    case 67586: // GCA
    case 133122: // GCG
      return 'A';

    case 262404: // TAT
    case 524548: // TAC
      return 'Y';

    case 262408: // CAT
    case 524552: // CAC
      return 'H';

    case 65800: // CAA
    case 131336: // CAG
      return 'Q';

    case 262401: // AAT
    case 524545: // AAC
      return 'N';

    case 65793: // AAA
    case 131329: // AAG
      return 'K';

    case 262402: // GAT
    case 524546: // GAC
      return 'D';

    case 65794: // GAA
    case 131330: // GAG
      return 'E';

    case 262660: // TGT
    case 524804: // TGC
      return 'C';

    case 131588: // TGG
      return 'W';

    case 262664: // CGT
    case 524808: // CGC
    case 66056: // CGA
    case 131592: // CGG
      return 'R';

    case 262657: // AGT
    case 524801: // AGC
      return 'S';

    case 66049: // AGA
    case 131585: // AGG
      return 'R';

    case 262658: // GGT
    case 524802: // GGC
    case 66050: // GGA
    case 131586: // GGG
      return 'G';

    case 65796: // TAA
    case 131332: // TAG
    case 66052: // TGA
      // stop codon
      return '*';

    default:
      return 'X';
  }
}
*/


