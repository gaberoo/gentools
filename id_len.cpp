#include <iostream>
#include <stdexcept>

#include "htslib/hts.h"
#include "htslib/sam.h"

using namespace std;

int main(int argc, char* argv[]) {
  if (argc < 2) {
    cerr << "Provide filename." << endl;
    return 0;
  }

  string filename = argv[1];

  char type = (argc > 2) ? argv[2][0] : 's';

  // open BAM for reading
  htsFile *in = hts_open(filename.c_str(), "r");
  if (in == NULL) {
    cerr << "Unable to open BAM/SAM file." << endl;
    return 0;
  }

  bam_hdr_t *header = sam_hdr_read(in);
  hts_itr_t *iter = NULL;
  bam1_t *aln = bam_init1();

  uint64_t totalReadLength = 0;
  uint32_t totalReads = 0;

  while (sam_read1(in,header,aln) >= 0) {
    if (type == 's') {
      ++totalReads;
      totalReadLength += aln->core.l_qseq;
    } else {
      cout << bam_get_qname(aln) << " " << aln->core.l_qseq << endl;
    }
  }

  if (type == 's') {
    cout << totalReads << " " << totalReadLength << endl;
  }

  bam_destroy1(aln);
  hts_itr_destroy(iter);
  bam_hdr_destroy(header);
  sam_close(in);

  return 0;
}

