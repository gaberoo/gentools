#include "FaIdx.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

const char* aa_missing = "Nn.- ";

/****************************************************************************/

FaIdx& FaIdx::open(string filename) {
  if (n > 0) clear();

  this->filename = filename;
  string fai_fn = filename + ".fai";

  struct stat result;

  time_t fa_mod_time;
  time_t fai_mod_time;

  int ret = stat(filename.c_str(),&result);
  if (ret == 0) {
    fa_mod_time = result.st_mtime;
  } else {
    cerr << "File " << filename << " doesn't appear to exist." << endl;
    return *this;
  }

  ret = stat(fai_fn.c_str(),&result);
  if (ret == 0) {
    // index file exists !
    fai_mod_time = result.st_mtime;
    if (fai_mod_time < fa_mod_time) {
      cerr << "FAI index was created before the last modification time of the FASTA file." << endl;
      cerr << fai_mod_time << " < " << fa_mod_time << endl;

      cerr << "Removing FAI index." << endl;
      if (unlink(fai_fn.c_str()) != 0) { cerr << "Error removing old FAI file." << endl; return *this; }

      string gzi_fn = filename + ".gzi";
      if (stat(gzi_fn.c_str(),&result) == 0) {
        cerr << "Removing GZI index." << endl;
        if (unlink(gzi_fn.c_str()) != 0) { cerr << "Error removing GZI file." << endl; return *this; }
      }
    }
  }

  index = fai_load(filename.c_str());

  if (index != NULL) {
    n = faidx_nseq(index);
    name.resize(n);
    for (size_t i = 0; i < n; ++i) name[i] = faidx_iseq(index,i);
    seq.resize(n,NULL);
    beg.resize(n,0);
    len.resize(n,0);
    mark.resize(n,true);
  } else {
    cerr << "Error loading " << filename << endl;
  }

  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::clear() {
  unload_all();
  name.clear();
  seq.clear();
  beg.clear();
  len.clear();
  mark.clear();
  n = 0;
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::unload(size_t i) {
  if (i < seq.size() && seq[i] != NULL) {
    free(seq[i]);
    seq[i] = NULL;
    len[i] = 0;
    beg[i] = 0;
  }
  return *this;
}

FaIdx& FaIdx::unload_all() {
  for (size_t i = 0; i < seq.size(); ++i) unload(i);
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::load(size_t i, int beg, int end) {
  if (i >= n) return *this;

  unload(i);

  ostringstream reg;
  reg << name[i];
  if (beg > 0 && end > 0 && end > beg) {
    reg << ":" << beg << "-" << end;
  }
  seq[i] = fai_fetch(index,reg.str().c_str(),&len[i]);
  (this->beg)[i] = (beg > 0) ? beg : 1;
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::load_all(int beg, int end) {
  for (size_t i = 0; i < n; ++i) load(i,beg,end);
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::load_marked(int beg, int end) {
  for (size_t i = 0; i < n; ++i) {
    if (mark[i]) load(i,beg,end);
  }
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::load_all(regex pattern, int beg, int end) {
  for (size_t i = 0; i < n; ++i) {
    if (regex_match(name[i],pattern)) {
      load(i,beg,end);
    } else {
      cerr << "Skipping " << name[i] << endl;
    }
  }
  return *this;
}

/****************************************************************************/

FaIdx& FaIdx::mark_seqs(string file) {
  ifstream in(file.c_str());
  string nm;

  mark.assign(name.size(),false);
  int cnt = 0;

  while (in >> nm) {
    auto it = std::find(name.begin(),name.end(),nm);
    if (it != name.end()) {
      int i = it-name.begin();
      mark[i] = true;
      cnt++;
    }
  }

  cerr << "Marked " << cnt << " sequences." << endl;

  return *this;
}

/****************************************************************************/

string FaIdx::info() const {
  ostringstream info;
  info << filename << endl
       << n << " sequences:" << endl;
  for (size_t i = 0; i < n; ++i) {
    info << setw(5) << i << ": " << name[i] << endl;
  }
  return info.str();
}

/****************************************************************************/

char FaIdx::rget(size_t i, int rpos) const {
  if (rpos >= 0 && rpos < len[i]) {
    return seq[i][rpos];
  } else {
    return 'n';
  }
}

/****************************************************************************/

void FaIdx::rset(size_t i, int rpos, char a) {
  if (rpos >= 0 && rpos < len[i]) {
    seq[i][rpos] = a;
  }
}

/****************************************************************************/

char FaIdx::get1(size_t i, size_t pos) const {
  int l = 0;
  ostringstream reg;
  reg << name[i] << ":" << pos << "-" << pos;
  char* c = fai_fetch(index,reg.str().c_str(),&l);
  char o = *c;
  free(c);
  return o;
}

/****************************************************************************/

array<int,4> FaIdx::counts(size_t pos) const {
  array<int,4> cnts({ 0, 0, 0, 0 });
  for (size_t i = 0; i < size(); ++i) {
    if (! mark[i]) continue;
    int z = idx(get(i,pos));
    if (z >= 0 && z < 4) cnts[z]++;
  }
  return cnts;
}

/****************************************************************************/

array<int,5> FaIdx::counts_n(size_t pos) const {
  array<int,5> cnts({ 0, 0, 0, 0, 0 });
  for (size_t i = 0; i < size(); ++i) {
    if (! mark[i]) continue;
    int z = idx(get(i,pos));
    if (z >= 0 && z < 5) cnts[z]++;
  }
  return cnts;
}

/****************************************************************************/

array<double,4> FaIdx::counts2(size_t pos, const double* weights) const {
  array<double,4> cnts({ 0.0, 0.0, 0.0, 0.0 });

  for (size_t i = 0; i < size(); ++i) {
    if (! mark[i]) continue;

    char c = get(i,pos);
    int ci = char2int(c);

    int nbits = 0;
    if (ci & HTS_A) nbits++;
    if (ci & HTS_G) nbits++;
    if (ci & HTS_T) nbits++;
    if (ci & HTS_C) nbits++;

    double f = 1.0/nbits;
    if (weights != NULL) f *= weights[i];

    if (ci & HTS_A) cnts[0] += f;
    if (ci & HTS_G) cnts[1] += f;
    if (ci & HTS_T) cnts[2] += f;
    if (ci & HTS_C) cnts[3] += f;
  }

  return cnts;
}

/****************************************************************************/

map<char,size_t> FaIdx::count_aa(size_t pos) const {
  map<char,size_t> cnts;
  for (size_t i = 0; i < size(); ++i) {
    char c = toupper(get(i,pos));
    switch (c) {
      case '-':
      case '.':
      case 'X':
        continue;

      default:
        cnts[c]++;
        break;
    }
  }
  return cnts;
}

/****************************************************************************/

int FaIdx::cmp(size_t i, size_t a, size_t b) const {
  int ca = char2int(get(a,i));
  int cb = char2int(get(b,i));

  if ((ca == HTS_ANY && cb == HTS_ANY) || (ca == HTS_GAP && cb == HTS_GAP)) {
    return 0;
  } else if (ca > 0 && cb > 0) {
    if ((ca & cb) == 0) return 2; // mismatch
    else return 1;                // match
  }

  return 0;                  // missing information
}

/****************************************************************************/

double FaIdx::cmp2(size_t i, size_t a, size_t b) const {
  int ca = char2int(get(a,i));
  int cb = char2int(get(b,i));
  if (ca > 0 && cb > 0) {
    if (ca == cb) return 0.0;
    else if (ca & cb) return 0.5;
    else return 1.0;
  } else {
    return -1.0;
  }
}

/****************************************************************************/

int cmpStr(size_t i, const char* strA, const char* strB) {
  int ca = char2int(strA[i]);
  int cb = char2int(strB[i]);
  if (ca && cb) {
    if ((ca & cb) == 0) return 2;
    else return 1;
  } else {
    return 0;
  }
}

/****************************************************************************/

pair<size_t,size_t> 
FaIdx::pairwise_distance_binary(size_t a, size_t b) const 
{
  size_t dist = 0.0;
  size_t dist2 = 0.0;
  size_t total = 0;

  int la = len[a];
  if (len[b] < la) la = len[b];

  if (beg[a] != beg[b]) return make_pair(-1,-1);

  string cstr = compStr(seq[b],len[b]);

  for (int i = 0; i < la; ++i) {
    int ret  = cmpStr(i,seq[a],seq[b]);
    int ret2 = cmpStr(i,seq[a],cstr.c_str());
    if (ret || ret2) total++;
    if (ret > 1) dist++;
    if (ret2 > 1) dist2++;
  }

  if (dist2 < dist) dist = dist2;

  return make_pair(dist,total);
}

/****************************************************************************/

pair<size_t,size_t> 
FaIdx::pairwise_distance_char(size_t a, size_t b) const
{
  size_t dist = 0.0;
  size_t total = 0;

  int la = len[a];
  if (len[b] < la) la = len[b];

  if (beg[a] != beg[b]) return make_pair(-1,-1);

  for (int i = 0; i < la; ++i) {
    char ca = get(a,beg[a]+i);
    char cb = get(b,beg[a]+i);

    if (strchr(aa_missing,ca) == NULL && strchr(aa_missing,cb) == NULL) {
      if (ca != cb) dist++;
      total++;
    }
  }

  return make_pair(dist,total);
}

/****************************************************************************/

pair<double,size_t> 
FaIdx::pairwise_distance(size_t a, size_t b) const 
{
  double dist = 0.0;
  size_t total = 0;

  int la = len[a];
  if (len[b] < la) la = len[b];
  if (beg[a] != beg[b]) return make_pair(-1,-1);

  for (int i = 0; i < la; ++i) {
    double ret = cmp2(beg[a]+i,a,b);

    if (ret >= 0.0) {
      total++;
      dist += ret;
    }
  }

  return make_pair(dist,total);
}

/****************************************************************************/

pair<size_t,size_t> 
FaIdx::pairwise_distance_pos(string nmA, string nmB, const vector<size_t>& pos) const
{
  auto a = std::find(name.begin(),name.end(),nmA);
  auto b = std::find(name.begin(),name.end(),nmB);

  if (a != name.end() && b != name.end()) {
    return pairwise_distance_pos(*a,*b,pos);
  } else {
    return make_pair(0,0);
  }
}

/****************************************************************************/

pair<size_t,size_t> 
FaIdx::pairwise_distance_pos(size_t a, size_t b, const vector<size_t>& pos) const
{
  size_t total = 0;
  size_t diff = 0;

  size_t la = length(a);
  size_t lb = length(b);
  size_t len = (la > lb) ? lb : la;

  for (size_t i = 0; i < pos.size(); ++i) {
    if (pos[i] >= len) break;

    switch (cmp(pos[i],a,b)) {
      case 2:
        diff++;
      case 1:
        total++;
      default:
        break;
    }
  }
  return make_pair(diff,total);
}

/****************************************************************************/

vector<size_t>
FaIdx::pairwise_distance_rng(size_t a, size_t b, size_t seed, size_t reps) const 
{
  size_t la = length(a);
  size_t lb = length(b);
  size_t len = (la > lb) ? lb : la;

  mt19937 rng(seed);

  vector<size_t> pos;
  for (size_t j = 0; j < len; ++j) {
    if (cmp(j,a,b)) pos.push_back(j);
  }

  vector<size_t> dist(reps+1,0);
  dist[0] = pos.size();

  uniform_int_distribution<size_t> range(0,pos.size()-1);

  for (size_t k = 1; k <= reps; ++k) {
    for (size_t j = 0; j < pos.size(); ++j) {
      size_t i = pos[range(rng)];
      if (cmp(i,a,b) > 1) dist[k]++;
    }
  }

  return dist;
}

/****************************************************************************/

int FaIdx::nucleotides(size_t i) const {
  int n = 0;
  for (int pos = 0; pos < length(i); ++pos) {
    if (char2int(get(i,pos)) & HTS_ANY) n++;
  }
  return n;
}

/****************************************************************************/

array<int,4> FaIdx::composition(size_t pos) const {
  array<int,4> comp{ 0, 0, 0, 0 };
  for (size_t i = 0; i < size(); ++i) {
    int c = idx(get(i,pos));
    if (c >= 0) comp[c]++;
  }
  return comp;
}

/****************************************************************************/

map<int,int> FaIdx::gaps(size_t i) const {
  map<int,int> gaps;
  int len = 0;
  for (int pos = 0; pos < length(i); ++pos) {
    int c = char2int(get(i,pos));
    if (c == HTS_ANY) {
      len++;
    } else if (len > 0) {
      gaps[len]++;
      len = 0;
    }
  }
  return gaps;
}

/****************************************************************************/

int FaIdx::informative(size_t pos) const {
  int n = 0;
  for (size_t i = 0; i < size(); ++i) {
    if (char2int(get(i,pos)) & HTS_ANY) n++;
  }
  return n;
}

/****************************************************************************/

string FaIdx::pattern_full(size_t pos) const {
  array<int,4> cmap { 0, 0, 0, 0 };
  array<int,4> cnt  { 0, 0, 0, 0 };
  string out = "";

  int k = 0;

  for (size_t i = 0; i < size(); ++i) {
    if (! mark[i]) continue;
    
    int z = idx(get(i,pos));
    if (z >= 0) {
      if (cmap[z] == 0) cmap[z] = (++k);
      cnt[z]++;
      out += (cmap[z]+'0');
    } else {
      out += 'n';
    }
  }

  return out;
}

/****************************************************************************/

string FaIdx::pattern(size_t pos, int rule) const {
  array<int,4> cnts(counts(pos));

  int imax = array_max(cnts.data(),4);

  string out = "";

  for (size_t i = 0; i < size(); ++i) {
    if (! mark[i]) continue;
    
    int z = idx(get(i,pos));
    if (z < 0) {
      if (rule == 1) out += "0";
      else out += "n";
    }
    else if (z == imax) out += "0";
    else out += "1";
  }

  return out;
}

/****************************************************************************/

void FaIdx::augment_nn(size_t min_overlap) {
  size_t n = size();
  size_t N = n*(n-1)/2;

  size_t L = max_full_len();
  if (L < min_overlap) min_overlap = L;

  vector<double> dmat(n*n,2.0);
  vector<size_t> idx(n*n,n);

#pragma omp parallel for shared(dmat)
  for (size_t k = 0; k < N; ++k) {
    array<size_t,2> ij(lin2mat(k,n));

    size_t i = ij[0];
    size_t j = ij[1];

    if (! mark[i] || ! mark[j]) continue;

    pair<double,uint64_t> cnt = pairwise_distance(i,j);

    if (cnt.second >= min_overlap) {
      dmat[i*n+j] = 1.0*cnt.first/cnt.second;
      dmat[j*n+i] = dmat[i*n+j];
    }
  }

  for (size_t i = 0; i < n; ++i) {
    if (! mark[i]) continue;

    size_t offset = i*n;

    iota(idx.data()+offset,idx.data()+offset+n,0);

    // sort indexes based on comparing values in v
    sort(
      idx.data()+offset,
      idx.data()+offset+n,
      [&dmat,offset](size_t i1, size_t i2) { 
        return dmat[offset+i1] < dmat[offset+i2]; 
      }
    );

    for (size_t j = beg[i]; j < beg[i]+len[i]; ++j) {
      if (get(i,j) == 'n') {
        for (size_t k = 0; k < n; ++k) {
          char alt = get(idx[offset+k],j);
          if (alt != 'n' && alt != 'N') {
            set(i,j,alt);
            break;
          }
        }
      }

    }
  }
}

/****************************************************************************/

void FaIdx::reverse_complement() {
  for (size_t i = 0; i < size(); ++i) {
    reverse_complement(i);
  }
}

/****************************************************************************/

void FaIdx::reverse(size_t i) { std::reverse(seq[i],seq[i]+len[i]); }

/****************************************************************************/

void FaIdx::complement(size_t i) {
  string out = compStr(seq[i],len[i]);
  strcpy(seq[i],out.c_str());
}


