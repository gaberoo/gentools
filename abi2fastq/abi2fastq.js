const fs = require('fs');
const Abi = require('./abi.js');

var filename = process.argv[2];
console.log(filename);

console.log("Reading file...");
var buf = fs.readFileSync(filename);

console.log("Reading ABI format...");
Abi.read_abif(data).then( info => {
  console.log(info);
  // console.log("done.");
}).catch( err => {
  console.log("Error reading ABI file: " + err);
});

/*
    Abi.extract_seqs(a).then( a => {
      $("#abi-" + a.name).append($('<span/>', {
        class: "abi-info seq-len-raw",
        html: 'raw length = ' + a.raw.seq.length
      }));
      return abi_get_db(a);
    }).then( a => {
      abi_add_glyph(a);
      return abi_trim(a);
    }).then( a => {
      if (a.trimmed) {
        // get mean quality
        var mq = array_mean(a.trimmed.num_qual);
        // add information to list
        var li = $('#abi-' + a.name).append($('<span/>', {
          class: "abi-info seq-len-trimmed",
          html: 'trimmed length = ' + a.trimmed.seq.length
        })).append($('<span/>', {
          class: "abi-info seq-qual-trimmed",
          html: 'mean trimmed qual = ' + Math.round(100*mq)/100
        }));
        // check for quality
        if (mq < 28 || a.trimmed.seq.length < 100) li.addClass("low-qual");
        // return next step
        // return abi_sintax(a);
      } else {
        console.log("No trimmed sequence.");
        $('#abi-' + a.name).addClass("low-qual");
        // reject("Trimming.");
      }
      fulfill(a);
    }).catch( err => {
      $('#abi-' + a.name).addClass("low-qual");
      // $('#abi-' + a.name + " .seq-sintax").html("No lineage data.");
      reject(err);
    });
*/

