// Required packages:
//   1. binary-parser
//   2. promise
//
var fs = require('fs');
var Parser = require('binary-parser').Parser;
var Promise = require('promise');

// Parsers

var abi_directory = new Parser()
  .string('name', { length: 4 })
  .int32be('tagnumber')
  .int16be('elementtype')
  .int16be('elementsize')
  .int32be('numelements')
  .int32be('datasize')
  .int32be('dataoffset')
  .int32be('datahandle');

var abi_header = new Parser()
  .string('header', { length: 4 })
  .int16be('version');

// Functions

function make_parser(name, dir) {
  var blen = dir.numelements*dir.elementsize;
  switch (dir.elementtype) {
    case 1: 
      return (new Parser()).array(name, { type: 'uint8', length: dir.numelements });
      break;
    case 2:
      return (new Parser()).string(name, { length: blen });
      break;
    case 3:
      return (new Parser()).array(name, { type: 'uint16', length: dir.numelements });
      break;
    case 4:
      return (new Parser()).array(name, { type: 'int16be', length: dir.numelements });
      break;
    case 5:
      return (new Parser()).array(name, { type: 'int32be', length: dir.numelements });
      break;
    case 7:
      return (new Parser()).array(name, { type: 'floatbe', length: dir.numelements });
      break;
    case 8:
      return (new Parser()).array(name, { type: 'doublebe', length: dir.numelements });
      break;
    case 10:
      return (new Parser()).int16be('year').uint8('month').uint8('year');
      break;
    case 11:
      return (new Parser()).uint8('hour').uint8('minute').uint8('second').uint8('hsecond');
      break;
    case 18:
      return (new Parser()).uint8('n').string(name, { length: blen-1 });
      break;
    case 19:
      return (new Parser()).string(name, { length: blen-1 });
      break;
    default:
      return (new Parser());
      break;
  }
}

module.exports = {
  read_abif: function(data) {
    return new Promise( (fulfill, reject) => {
      var header = abi_header.parse(data);
      var headDir = abi_directory.parse(data.slice(6));
      var buflen = headDir.numelements*headDir.elementsize;

      var dir = [];
      var offset = headDir.dataoffset;
      for (var i = 0; i < headDir.numelements; ++i) {
        dir.push(abi_directory.parse(data.slice(offset)));
        offset += headDir.elementsize;
      }

      // read the data
      for (var k = 0; k < dir.length; ++k) {
        if (dir[k].name == "PCON") dir[k].elementtype = 1;

        var pos = 0;
        if (dir[k].datasize > 4) {
          pos = dir[k].dataoffset;
        } else { 
          pos = k*headDir.elementsize + headDir.dataoffset + 20;
        }

        dir[k].data = make_parser('_out',dir[k]).parse(data.slice(pos))._out;
      }

      fulfill(dir);
    });
  },
  extract_seqs: function(out) {
    var abi = out.abi;
    out.raw = {};
    return new Promise( (fulfill, reject) => {
      for (var i = 0; i < abi.length; ++i) {
        if (abi[i].name == "PBAS" && abi[i].tagnumber == 1) {
          out.raw.seq = abi[i].data;
        }
        if (abi[i].name == "PCON" && abi[i].tagnumber == 1) {
          out.raw.num_qual = abi[i].data;
        }
      }
      if (out.raw.num_qual) {
        out.raw.qual = out.raw.num_qual.map( d => String.fromCharCode(d+33) ).join('');
      }
      if (out.raw.seq || out.raw.qual) fulfill(out);
      else reject("No match.");
    });
  }
};

