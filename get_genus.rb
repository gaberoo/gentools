#!/usr/bin/env ruby

require 'uri'
require 'curb'
require 'nokogiri'

require 'optparse'

db = "nuccore"
verbose = false
query = ARGV[0] # "NZ_LN890518.1"
tax_id = nil
parse_xml = true

base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils"

OptionParser.new do |opts|
  opts.banner = "Usage: get_genus.rb [options]"
  opts.on("-d", "--database [STRING]", "Database") { |d| db = d }
  opts.on("-q", "--query [STRING]", "Query") { |q| query = q }
  opts.on("-t", "--taxid [INT]", "Tax ID") { |t| tax_id = t }
  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| verbose = v }
  opts.on("-x", "--[no-]parse", "Parse XML response") { |x| parse_xml = x }
end.parse!

unless tax_id
  url = "#{base}/esearch.fcgi?db=#{db}&term=#{query}&usehistory=y"

  puts url if verbose

  q = Curl::Easy.new url
  q.follow_location = true
  q.enable_cookies = true
  q.perform

  puts q.body_str if verbose

  web = $1 if q.body_str.match(/<WebEnv>(\S+)<\/WebEnv>/)
  key = $1 if q.body_str.match(/<QueryKey>(\d+)<\/QueryKey>/)

  ### include this code for ESearch-ESummary
  #assemble the esummary URL
  url = "#{base}/esummary.fcgi?db=#{db}&query_key=#{key}&WebEnv=#{web}"
  puts url if verbose

  doc = Curl::Easy.new url
  doc.follow_location = true
  doc.enable_cookies = true
  doc.perform

  nok = Nokogiri::XML(doc.body_str)

  puts doc.body_str if verbose

  res = nok.xpath("//Item[@Name='TaxId']")
  tax_id = res ? res.text.to_i : nil

  puts tax_id if verbose

  unless tax_id
    id = nok.xpath("//Id").text

    url = "#{base}/elink.fcgi?dbfrom=#{db}&db=taxonomy&id=#{id}"
    puts url if verbose

    q = Curl::Easy.new url
    q.follow_location = true
    q.enable_cookies = true
    q.perform

    nok = Nokogiri::XML(q.body_str)
    tax_id = nok.xpath("//LinkSetDb//Id").text
  end
end

def nok_find(nok, str, options = {})
  res = nok.xpath("//Item[@Name='#{str}']")
  out = res ? (res.first ? res.first.text : nil) : nil
  out = out.downcase if out and options[:lower]
  out
end

unless tax_id == ""
  url = "#{base}/esummary.fcgi?db=taxonomy&id=#{tax_id}"
  puts url if verbose

  q = Curl::Easy.new url
  q.follow_location = true
  q.enable_cookies = true
  q.perform

  if parse_xml
    nok = Nokogiri::XML(q.body_str)

    rank = nok_find nok, "Rank", lower: true
    name = nok_find nok, "ScientificName"
    phylum = nok_find nok, "Phylum"
    genus = (rank == 'genus') ? name : nok_find(nok, "Genus")
    species = nok_find nok, "Species"
    subsp = nok_find nok, "Subsp"

    puts "#{query};#{tax_id};#{genus};#{species};#{subsp};#{name};#{rank}"
  else
    puts q.body_str
  end
end

