#ifndef __FASTA_STATS_H__
#define __FASTA_STATS_H__

#include "../FaIdx.h"
#include "../cxxopts.hpp"
#include "../duo.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_statistics_int.h>
#include <gsl/gsl_rng.h>

extern "C" {
  void fexact_(int* nrow, int* ncol, double* table, int* ldtab, 
               double* expect, double* percnt, double* emin, 
               double* prt, double* pre);

  void rcont2(int *nrow, int *ncol, int *nrowt, int *ncolt, int *ntotal,
              double *fact, int *jwork, int *matrix, double (*unif_rand)());

  extern double expect;
  extern double percent;
  extern double emin;
  extern double fexact_B;

  extern gsl_rng* rng;

  double unif_rand();
}

void view(FaIdx& index, const cxxopts::Options& options);
void test(FaIdx& index, const cxxopts::Options& options);
void dist(FaIdx& index, const cxxopts::Options& options);
void count_snps(FaIdx& index, const cxxopts::Options& options);
void filter(FaIdx& index, const cxxopts::Options& options);
void aln_pattern(FaIdx& index, const cxxopts::Options& options);
void aln_pattern_full(FaIdx& index, const cxxopts::Options& options);
void raster(FaIdx& index, const cxxopts::Options& options);
void raster_png(FaIdx& index, const cxxopts::Options& options);
void sfs(FaIdx& index, const cxxopts::Options& options);
void binary();
void pairwise_distances(FaIdx& index, const cxxopts::Options& options);
void informative(FaIdx& index, const cxxopts::Options& options);
void coverage(FaIdx& index, const cxxopts::Options& options);
void gap_finder(FaIdx& index, const cxxopts::Options& options);
void binSum(FaIdx& index, const cxxopts::Options& options);
void count_table(FaIdx& index, const cxxopts::Options& options);
void composition(FaIdx& index, const cxxopts::Options& options);
void mask(FaIdx& index, const cxxopts::Options& options);
void consensus(FaIdx& index, const cxxopts::Options& options);
void diversity(FaIdx& index, const cxxopts::Options& options);
void unique_patterns(FaIdx& index, const cxxopts::Options& options);

#endif
