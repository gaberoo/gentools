#include "fasta_stats.h"

void sfs(FaIdx& index, const cxxopts::Options& options) {
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");

  int len = index.max_full_len();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  map<uint32_t,uint16_t> sfs;

  for (int k = rbeg; k <= rend; ++k) {
    if ((k-1) % load_window == 0) {
      if (verbose) cerr << "Loading sequences " << k << "-" << k+load_window-1 << "..." << flush;
      index.load_all(k,k+load_window-1);
      if (verbose) cerr << "done." << endl;
    }

    array<int,4> cnts(index.counts(k));

    uint16_t imax = array_max(cnts.data(),4);
    uint16_t sum = cnts[0]+cnts[1]+cnts[2]+cnts[3];

    uint32_t idx = duo(sum,cnts[imax]);

    sfs[idx]++;
  }

  for (const auto& x: sfs) {
    cout << x.first << "\t"
         << duo1(x.first) << "\t"
         << duo2(x.first) << "\t"
         << x.second << endl;
  }
}


