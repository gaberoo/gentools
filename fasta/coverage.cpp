#include "fasta_stats.h"

void coverage(FaIdx& index, const cxxopts::Options& options) {
  for (size_t i = 0; i < index.size(); ++i) {
    if (options.count("verbose")) cerr << i << "/" << index.size() << "       \r";
    if (! index.mark[i]) continue;

    index.load(i);

    int k = index.nucleotides(i);
    map<int,int> g = index.gaps(i);

    int g_total = 0;
    double g_mean = 0.0;
    double g_var = 0.0;
    int g_lo = -1;
    int g_l = -1;
    int g_h = -1;
    int g_hi = -1;

    for (auto k = g.begin(); k != g.end(); ++k) {
      if (options.count("coverage-gapdist")) {
        cout << index[i] << " " << k->first << " " << k->second << endl;
      } else {
        g_total += k->second;
        g_mean += k->first*k->second;
      }
    }
    g_mean /= g_total;

    if (! options.count("coverage-gapdist")) {
      int cnt = 0;
      for (auto k = g.begin(); k != g.end(); ++k) {
        g_var += k->second*gsl_pow_2(k->first-g_mean);
        cnt += k->second;
        if (g_lo < 0 && cnt >= 0.025*g_total) g_lo = k->first;
        if (g_l < 0 && cnt >= 0.25*g_total) g_l = k->first;
        if (g_h < 0 && cnt >= 0.75*g_total) g_h = k->first;
        if (cnt <= 0.975*g_total) g_hi = k->first;
      }
      g_var /= g_total;

      cout << index[i] << "\t" 
           << k << "\t" 
           << 1.0*k/index.max_full_len() << "\t"
           << g.size() << "\t" 
           << g_mean << "\t" 
           << sqrt(g_var) << "\t"
           << g_lo << "\t" 
           << g_hi << endl;
    }

    index.unload(i);
  }
  if (options.count("verbose")) cerr << "Done.            " << endl;
}


