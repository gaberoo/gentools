#include "fasta_stats.h"

void aln_pattern_full(FaIdx& index, const cxxopts::Options& options) {
  if (options.count("verbose")) cerr << "Getting patterns..." << endl;

  map<string,uint32_t> patterns;

  int len = index.max_full_len();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  index.load_all(rbeg,rend);

  for (int k = rbeg; k <= rend; ++k) {
    string pattern = index.pattern_full(k);
    patterns[pattern]++;
  }

  if (options.count("pattern-graph")) {
    int i = 0;
    for (auto p1 = patterns.begin(); p1 != patterns.end(); ++p1, ++i) {
      const string& s1 = p1->first;
      auto p2 = p1; 
      p2++;
      int j = i+1;
      for (; p2 != patterns.end(); ++p2, ++j) {
        const string& s2 = p2->first;
        int match = 0;
        int overlap = 0;
        for (size_t k = 0; k < s1.length(); ++k) {
          if (s1[k] != 'n' && s2[k] != 'n') {
            overlap++;
            if (s1[k] == s2[k]) match++;
          }
        }
        cout << i << " " << j << " " << 1.0*(overlap-match)/overlap << endl;
      }
    }
  } else {
    for (auto it = patterns.begin(); it != patterns.end(); ++it) {
      array<int,5> cnt{ 0, 0, 0, 0, 0 };

      for (const char& s: it->first) {
        switch (s) {
          case 'n': cnt[0]++; break;
          case '1': cnt[1]++; break;
          case '2': cnt[2]++; break;
          case '3': cnt[3]++; break;
          case '4': cnt[4]++; break;
        }
      }

      cout << it->first << " " << it->second << " "
             << cnt[1] << " " << cnt[2] << " "
             << cnt[3] << " " << cnt[4] << " "
             << cnt[0] << endl;
    }
  }
}


