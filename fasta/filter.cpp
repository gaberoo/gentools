#include "fasta_stats.h"

void filter(FaIdx& index, const cxxopts::Options& options) {
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");
  int non_gap = options.count("filter-non-gap");
  double sites_cutoff = options["filter-genome-threshold"].as<double>();
  int keep_sites = options.count("filter-keep-sites");
  int report_pos = options.count("filter-report-positions");
  int by_codon = options.count("filter-by-codon");
  int type = (options["type"].as<string>() == "aa") ? 2 : 1;

  int len = index.max_full_len();
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  if (rbeg < 1) rbeg = 1;
  if (rend < 1 || rend > len) rend = len;

  vector<size_t> positions;

  if (options.count("revcomp")) index.reverse_complement();

  if (verbose) {
    cerr << "Loading sequences " << rbeg << "-" << rend << "..." << flush;
  }
  index.load_all(rbeg,rend);
  if (verbose)  cerr << "done." << endl;

  if (keep_sites) {
    if (verbose) cerr << "[filter] keeping all sites (" << index.size() << ")" << endl;
    for (size_t i = 0; i < index.size(); ++i) {
      if (! index.mark[i]) {
        if (verbose) cerr << "[filter] skipping " << index[i] << endl;
        continue;
      }
      size_t cnt = index.nucleotides(i);
      if (verbose) {
        cerr << index[i] << " " << cnt << "/" << len << endl;
      }
      if (1.0*cnt/len >= sites_cutoff) {
        cout << ">" << index[i] << "\n" << index(i) << endl;
      }
    }
  } else {
    int k, j;
    for (k = rbeg, j = 0; k < rend; ++k, ++j) {
      if (type == 2) {
        auto cnts = index.count_aa(k);
        if (report_pos && cnts.size() > 1) {
          cout << k;
          for (auto x: cnts) {
            cout << "\t" << x.first << "(" << x.second << ")";
          }
          cout << endl;
        } else {
          if (cnts.size() > 1) positions.push_back(k);
        }
      } else {
        auto cnts = index.counts2(k);
        size_t zeros = 0;
        double total = 0.0;
        for (auto c: cnts) {
          zeros += (c == 0.0);
          total += c;
        }
        if (report_pos && zeros < 3) {
          cout << k;
          for (auto x: cnts) cout << "\t" << x;
          cout << endl;
        } else {
          if ((non_gap && total >= index.size()) || (!non_gap && zeros < 3)) {
            positions.push_back(k);
          }
        }
      }
    }

    if (! report_pos) {
      for (size_t i = 0; i < index.size(); ++i) {
        if (! index.mark[i]) continue;
        int nsites = 0;

        for (auto k: positions) {
          if (char2int(index(i,k)) & HTS_ANY) nsites++;
        }

        if (sites_cutoff <= 0.0 || 1.0*nsites/positions.size() > sites_cutoff) {
          cout << ">" << index[i] << endl;

          if (by_codon) {
            int last_codon = -1;
            for (auto k: positions) {
              // get codon
              int cpos = k / 3;
              if (cpos > last_codon) {
                cout << index(i,cpos*3) << index(i,cpos*3+1) << index(i,cpos*3+2);
                last_codon = cpos;
              }
            }
          } else {
            for (auto k: positions) cout << index(i,k);
          }

          cout << endl;
        }
      }
    }
  }

  index.unload_all();
}


