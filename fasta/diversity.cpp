#include "fasta_stats.h"

void diversity(FaIdx& index, const cxxopts::Options& options) {
  int len = index.max_full_len();
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  int verbose = options.count("verbose");

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  index.load_all(rbeg,rend);

  double Stot = 0.0; // total sum of entropy
  int Scnt = 0;      // total counts
  int Spos = 0;      // total pos values
  for (int k = rbeg; k <= rend; ++k) {
    auto cnts = index.counts2(k);
    double total = accumulate(cnts.begin(),cnts.end(),0.0);
    if (total > 0.0) {
      double S = 0.0;
      for (double cnt: cnts) {
        if (cnt > 0.0) S += cnt/total * log(cnt/total);
      }
      if (verbose) {
        cerr << k;
        for (double cnt: cnts) cerr << " " << cnt/total;
        cerr << endl;
      }
      Stot += -S/4.0;
      Scnt++;
      if (S < 0.0) Spos++;
    }
  }

  cout << Stot << "\t" << Scnt << "\t" << Stot/Scnt << "\t" << Spos << endl;

  index.unload_all();
}


