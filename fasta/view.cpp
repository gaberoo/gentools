#include "fasta_stats.h"

void view(FaIdx& index, const cxxopts::Options& options) {
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  string format = options["format"].as<string>();
  if (format.length() == 0) format = "f";

  index.load_all(rbeg,rend);

  if (options.count("augment")) {
    size_t min_overlap = options["dist-overlap"].as<size_t>();
    index.augment_nn(min_overlap);
  }

  if (options.count("revcomp")) {
    index.reverse_complement();
  }

  if (format[0] == 'F' || format[0] == 'f') {
    for (size_t i = 0; i < index.size(); ++i) {
      if (! index.mark[i]) continue;
      printf(">%s\n%s\n",index[i].c_str(),index(i));
    }
  } else if (format[0] == 'P' || format[0] == 'p') {
    int first = 1;
    int block_size = 70;

    size_t j = 0;
    size_t ml = index.max_len();

    cout << index.size() << " " << ml << endl;

    while (j < ml) {
      for (size_t i = 0; i < index.size(); ++i) {
        // if (first) printf("%-20s ",index[i].c_str());
        if (first) cout << index[i] << " ";
        for (size_t k = j; k < ml && k < j+block_size; ++k) {
          cout << index(i,k);
        }
        cout << endl;
      }
      if (first) first = 0;
      cout << endl;
      j += block_size;
    }
  }
}


