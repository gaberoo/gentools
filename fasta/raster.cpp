#include "fasta_stats.h"

void raster(FaIdx& index, const cxxopts::Options& options) {
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");
  int ovcf = options.count("raster-vcf");
  int monomorphic = options.count("raster-all");

  string contig_name = "1";
  if (options.count("raster-contig-name")) contig_name = options["raster-contig-name"].as<string>();

  int len = index.max_full_len();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  if (ovcf) {
    char odate[80];
    time_t rawtime = time(NULL);
    strftime(odate,80,"%Y%m%d",localtime(&rawtime));
    cout << "##fileformat=VCFv4.2" << endl
         << "##fileDate=" << odate << endl
         << "##source=fasta_stats-raster" << endl
         << "##reference=" << options["input"].as<string>() << endl;
    cout << "##contig=<ID=" << contig_name << ",length=" << len << ">" << endl;
    cout << "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">" << endl;
    cout << "##FORMAT=<ID=GT,Number=1,Type=Integer,Description=\"Genotype\">" << endl;
    cout << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
    for (size_t i = 0; i < index.size(); ++i) {
      cout << "\t" << index[i];
    }
    cout << endl;
  }

  for (int k = rbeg; k <= rend; ++k) {
    if ((k-1) % load_window == 0) {
      if (verbose) {
        cerr << "Loading sequences " 
             << k << "-" << k+load_window-1 << "..." << flush;
      }
      index.load_all(k,k+load_window-1);
      if (verbose) cerr << "done." << endl;
    }

    auto cnts = index.counts_n(k);
    auto cnts2 = index.counts2(k);
    int major = get_major(cnts2);
    char cmajor = int2char(major);

    map<int,int> genotypes;
    map<int,int> gt_idx;
    for (size_t i = 0; i < index.size(); ++i) genotypes[char2int(index.get(i,k))]++;
    gt_idx[major] = 0;
    size_t idx = 1;
    for (const auto& gt: genotypes) {
      if (gt.first != major) gt_idx[gt.first] = idx++;
    }

    int ns = cnts[0] + cnts[1] + cnts[2] + cnts[3];

    if (ovcf) {
      if (ns > 0 && (monomorphic || gt_idx.size() > 1)) {
        cout << contig_name << "\t" << k << "\t" << "." << "\t" << int2char(major) << "\t";

        // MAJOR + ALT
        if (gt_idx.size() > 1) {
          int first = 0;
          for (const auto& gt: genotypes) {
            if (gt.first != major) {
              if (first) { cout << ","; } else { first = 1; }
              cout << int2char(gt.first);
            }
          }
        } else {
          cout << "*";
        }

        // QUAL + FILTER
        cout << "\t*\t.\t";

        // INFO
        cout << "NS=" << ns << "\t";

        // FORMAT
        cout << "GT";

        // SAMPLES
        for (size_t i = 0; i < index.size(); ++i) {
          cout << "\t" << gt_idx[char2int(index.get(i,k))];
        }
        cout << endl;
      }
    } else {
      if (major) {
        for (size_t i = 0; i < index.size(); ++i) {
          if (index.get(i,k) != cmajor) {
            cout << k << "\t" << i << "\t" << index.get(i,k) << "\t" 
                 << cmajor << "\t" << index[i] << endl;
          }
        }
      }
    }
  }
}


