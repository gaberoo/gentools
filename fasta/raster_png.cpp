#include "fasta_stats.h"
#include "../pngwriter.h"

void raster_png(FaIdx& index, const cxxopts::Options& options) {
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");
  string filename = options["png"].as<string>();
  int w = options["png-width"].as<int>();
  int h = options["png-height"].as<int>();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  int len = index.max_full_len();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  int bases = rend-rbeg+1;
  int base_w = GSL_MAX(w/bases,2);
  int base_h = GSL_MAX(h/index.size(),2);

  if (load_window < bases) load_window = bases;

  w = bases*base_w;
  h = index.size()*base_h;

  cerr << "Output file: " << filename << endl;
  cerr << "  width  = " << w << endl;
  cerr << "  height = " << h << endl;

  pngwriter png(w,h,1.0,filename.c_str());

  for (int k = rbeg; k <= rend; ++k) {
    if ((k-rbeg) % load_window == 0) {
      if (verbose) {
        cerr << "Loading sequences " 
             << k << "-" << k+load_window-1 << "..." << flush;
      }
      index.load_all(k,k+load_window-1);
      if (verbose)  cerr << "done." << endl;
    }

    array<int,4> cnts(index.counts(k));
    int major = get_major(cnts);

    for (size_t i = 0; i < index.size(); ++i) {
      if (! index.mark[i]) continue;

      char c = index.get(i,k);
      int id = idx(c);
      if (id >= 0) {
        if ((char2int(c) & major) == 0) {
          png.filledsquare((k-rbeg)*base_w,i*base_h,
                           (k-rbeg+1)*base_w,(i+1)*base_h,
                           hts_cols[id][0]*255,
                           hts_cols[id][1]*255,
                           hts_cols[id][2]*255);
          png.square((k-rbeg)*base_w,i*base_h,
                     (k-rbeg+1)*base_w,(i+1)*base_h,
                     0.4,0.4,0.4);
        } else {
          png.filledsquare_blend((k-rbeg)*base_w,i*base_h,
                           (k-rbeg+1)*base_w,(i+1)*base_h,
                           0.2,
                           hts_cols[id][0]*255,
                           hts_cols[id][1]*255,
                           hts_cols[id][2]*255);
        }
      }
    }
  }

  /*
  for (size_t i = 0; i < index.size(); ++i) {
    png.square(0,i*base_h,w,(i+1)*base_h,0.4,0.4,0.4);
  }
  */

  png.close();
}


