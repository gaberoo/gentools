#include "fasta_stats.h"

void aln_pattern(FaIdx& index, const cxxopts::Options& options) {
  cerr << "Getting patterns..." << endl;

  map<string,uint32_t> patterns;

  int len = index.max_full_len();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  int rule = options["pattern-rule"].as<int>();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  index.load_all(rbeg,rend);

  if (options.count("augment")) {
    size_t min_overlap = options["dist-overlap"].as<size_t>();
    index.augment_nn(min_overlap);
  }

  map<uint32_t,uint16_t> sfs;

  for (int k = rbeg; k <= rend; ++k) {
    string pattern = index.pattern(k,rule);
    patterns[pattern]++;
  }

  if (! options.count("pattern-consensus")) {
    for (const auto& pat: patterns) {
      int cnt = 0;
      for (const auto& c: pat.first) if (c == '1') ++cnt;
      if (cnt > 0 || options.count("pattern-zero")) {
        cout << pat.first << " " << pat.second << " " << cnt << endl;
      }
    }
  } else {
    string cons = "";
    size_t n = patterns.begin()->first.size();

    int zero = 0;
    int one = 0;
    int none = 0;

    for (size_t i = 0; i < n; ++i) {
      zero = 0;
      one = 0;
      none = 0;

      for (const auto& p: patterns) {
        int cnt = 0;
        for (char c: p.first) if (c == '1') ++cnt;

        if (cnt > 0 || options.count("pattern-zero")) {
          switch (p.first[i]) {
            case '0': zero += p.second; break;
            case '1': one += p.second; break;
            default: none += p.second; break;
          }
        }
      }

      if (options.count("verbose")) {
        cerr << i << " " << zero << " " << one << " " << none << endl;
      }

      if (zero+one == 0) {
        cons += 'n';
      } else {
        if (zero > one) cons += '0';
        else if (one > zero) cons += '1';
        else cons += '-';
      }
    }

    cout << cons << endl;
  }
}


