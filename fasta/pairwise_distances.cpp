#include "fasta_stats.h"

void pairwise_distances(FaIdx& index, const cxxopts::Options& options) {
  size_t n = index.size();
  char out_format = options["dist-fmt"].as<char>();
  size_t min_overlap = options["dist-overlap"].as<size_t>();

  vector<size_t> from;
  if (options.count("dist-from")) {
    auto dist_from = options["dist-from"].as< vector<string> >();
    for (string name: dist_from) {
      size_t j = index.find(name);
      if (j < index.size()) {
        from.push_back(j);
        index.load(j);
      }
    }
  }

  vector<size_t> dmat;
  vector<size_t> tmat;

  int seed = options["dist-seed"].as<int>();
  size_t reps = options["dist-reps"].as<size_t>();

  mt19937 rng;
  if (reps) rng.seed(seed);

  if (options.count("verbose")) cerr << "Calculating pairwise distances:" << endl;

  size_t N = n*(n-1)/2;

  if (out_format == 'p' || out_format == 'm') {
    dmat.resize(N,0);
    tmat.resize(N,0);
  }

  int len = index.max_full_len();

  vector< pair<size_t,size_t> > ij;

  if (from.size() == 0) {
    for (size_t i = 1; i < index.size(); ++i) {
      for (size_t j = 0; j < i; ++j) {
        if (index.mark[i] && index.mark[j]) ij.push_back(make_pair(i,j));
      }
    }
  } else {
    for (size_t i: from) {
      for (size_t j = 0; j < index.size(); ++j) {
        if (j != i && index.mark[j]) ij.push_back(make_pair(i,j));
      }
    }
  }

  size_t m = 0;
#pragma omp parallel for shared(index,dmat,tmat,ij)
  for (size_t k = 0; k < ij.size(); ++k) {
    size_t i = ij[k].first;
    size_t j = ij[k].second;

    if (options.count("verbose")) {
#pragma omp critical
      cerr << "   " << m++ << "/" << ij.size() << "\r" << flush;
    }

    pair<double,uint64_t> cnt;
    if (options.count("binary")) {
      cnt = index.pairwise_distance_binary(i,j);
    } else if (options["type"].as<string>() == "aa") {
      cnt = index.pairwise_distance_char(i,j);
    } else {
      cnt = index.pairwise_distance(i,j);
    }

    if (out_format == 'l' || (out_format == 'L' && cnt.second)) {
      ostringstream prnt;
      prnt << index[i] << "\t" << index[j] << "\t" << len << "\t"
           << cnt.first << "\t" << cnt.second << "\t"
           << ((cnt.second > 0) ? 1.0*cnt.first/cnt.second : 0.0) << endl
           << index[j] << "\t" << index[i] << "\t" << len << "\t"
           << cnt.first << "\t" << cnt.second << "\t"
           << ((cnt.second > 0) ? 1.0*cnt.first/cnt.second : 0.0) << endl;
#pragma omp critical
      cout << prnt.str();
    } else if (out_format == 'p' || out_format == 'm') {
      dmat[k] = cnt.first;
      tmat[k] = cnt.second;
    }
  }

  if (options.count("verbose")) cerr << endl;

  if (out_format == 'p') {
    cout << n << endl;
    for (size_t i = 0; i < n; ++i) {
      if (! index.mark[i]) continue;
      cout << index[i] << " ";
      for (size_t j = 0; j < n; ++j) {
        if (! index.mark[j]) continue;
        size_t k = mat2lin(i,j,n);
        if (i == j) {
          cout << 0.0 << " ";
        } else if (tmat[k] >= min_overlap) {
          cout << 1.0*dmat[k]/tmat[k] << " ";
        } else {
          cout << -99.0 << " ";
        }
      }
      cout << endl;
    }
  } 
  
  else if (out_format == 'm') {
    for (size_t i = 0; i < n; ++i) {
      if (! index.mark[i]) continue;

      cout << index[i] << " ";

      for (size_t j = 0; j < n; ++j) {
        if (! index.mark[j]) continue;

        size_t k = mat2lin(i,j,n);

        if (i == j) {
          cout << 0.0 << " ";
        } else if (tmat[k] >= min_overlap) {
          cout << dmat[k] << " ";
        } else {
          cout << "NA ";
        }
      }
      cout << endl;
    }
  }
}


