#include "fasta_stats.h"

void informative(FaIdx& index, const cxxopts::Options& options) {
  vector<size_t> cnt;

  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  index.load_all(rbeg,rend);

  int all = options.count("informative-all");
  int len = index.max_full_len();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  for (int pos = rbeg; pos <= rend; ++pos) {
    if (pos % load_window == 0) {
      if (verbose) {
        cerr << "Loading sequences " 
             << pos+1 << "-" << pos+load_window << "..." << flush;
      }
      index.load_all(pos,pos+load_window);
      if (verbose)  cerr << "done." << endl;
    }

    size_t k = index.informative(pos);

    if (all) {
      cout << pos << " " << k << endl;
    } else {
      if (cnt.size() <= k) cnt.resize(k+1,0);
      cnt[k]++;
    }
  }
}


