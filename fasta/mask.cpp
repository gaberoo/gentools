#include "fasta_stats.h"

void mask(FaIdx& index, const cxxopts::Options& options) {
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  string mask_ref = options["mask-ref"].as<string>();
  size_t ref = 0;
  for (; ref < index.size(); ++ref) {
    if (mask_ref == index[ref]) {
      if (options.count("verbose")) {
        cerr << "Found masking reference: " << ref << endl;
      }
      break;
    }
  }

  if (ref >= index.size()) {
    cerr << "Couldn't find entry '" << mask_ref << "'" << endl;
    return;
  }

  string format = options["format"].as<string>();
  if (format.length() == 0) format = "f";

  index.load_all(rbeg,rend);

  if (options.count("augment")) {
    size_t min_overlap = options["dist-overlap"].as<size_t>();
    index.augment_nn(min_overlap);
  }

  if (options.count("revcomp")) {
    index.reverse_complement();
  }

  size_t ml = index.max_len();
  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;
    cout << ">" << index[i] << endl;
    for (size_t k = 0; k < ml; ++k) {
      if (char2int(index(ref,k)) & HTS_ANY) {
        cout << index(i,k);
      }
    }
    cout << endl;
  }
}


