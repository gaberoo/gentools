#include "fasta_stats.h"

void composition(FaIdx& index, const cxxopts::Options& options) {
  int window = options["window"].as<int>();
  int load_window = options["load"].as<int>();
  if (window == 0) return;

  if (load_window < window) load_window = window;
  load_window = load_window - (load_window % window);

  int len = index.max_full_len();
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  index.load(0);

  int gc = 0;
  int at = 0;
  int c;

  int pos = rbeg;
  for (; pos < window; ++pos) {
    c = index.get(0,pos);
    switch (toupper(c)) {
      case 'A': case 'T': at++; break;
      case 'G': case 'C': gc++; break;
      default: break;
    }
    cout << pos << " " << gc << " " << at << " " << 1.0*gc/(gc+at) << endl;
  }

  for (; pos < rend-window; ++pos) {
    c = index.get(0,pos);
    switch (toupper(c)) {
      case 'A': case 'T': at++; break;
      case 'G': case 'C': gc++; break;
      default: break;
    }

    c = index.get(0,pos-window);
    switch (toupper(c)) {
      case 'A': case 'T': at--; break;
      case 'G': case 'C': gc--; break;
      default: break;
    }

    cout << pos << " " << gc << " " << at << " " << 1.0*gc/(gc+at) << endl;
  }

  for (; pos < rend; ++pos) {
    c = index.get(0,pos-window);
    switch (toupper(c)) {
      case 'A': case 'T': at--; break;
      case 'G': case 'C': gc--; break;
      default: break;
    }
    cout << pos << " " << gc << " " << at << " " << 1.0*gc/(gc+at) << endl;
  }

}


