#include "fasta_stats.h"

void consensus(FaIdx& index, const cxxopts::Options& options) {
  int len = index.max_full_len();
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  int verbose = options.count("verbose");
  int distance = options.count("consensus-dist");
  double min_freq = options["consensus-min-freq"].as<double>();

  vector<size_t> idx;

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) {
      if (verbose) cerr << "[consensus] skipping " << index[i] << endl;
      continue;
    }
    index.load(i,rbeg,rend);
    idx.push_back(i);
  }

  vector<double> weights(index.size(),1.0);
  if (options.count("consensus-weights")) {
    vector<string> wstr = options["consensus-weights"].as< vector<string> >();
    for (auto ws: wstr) {
      size_t pos = ws.find('=');
      if (pos != string::npos) {
        string key = ws.substr(0,pos);
        string val = ws.substr(pos+1);
        size_t i = index.find(key);
        if (i < index.size()) {
          weights[i] = atof(val.c_str());
          if (verbose) {
            cerr << "  setting weight of '" << key << " (" << i << ") to " << weights[i] << endl;
          }
        }
      }
    }
  }

  string out = "";
  
  vector<int> cdist_cnt(idx.size(),0);
  vector<int> cdist_tot(idx.size(),0);

  for (int k = rbeg; k <= rend; ++k) {
    array<double,4> cnts(index.counts2(k,weights.data()));

    double total = cnts[0] + cnts[1] + cnts[2] + cnts[3];
    int major = get_major(cnts);

    if (verbose > 1) {
      cerr << k << " "
           << cnts[0] << " " << cnts[1] << " "
           << cnts[2] << " " << cnts[3] << " "
           << major << " " << int2char(major) << endl;
    }

    if (! distance) {
      if (total >= min_freq*idx.size()) {
        out += int2char(major);
      } else {
        out += '.';
      }
    } else {
      for (size_t j = 0; j < idx.size(); ++j) {
        int c = char2int(index.get(idx[j],k));
        if (c > 0) {
          cdist_tot[j]++;
          if (major > 0 && ! (c & major)) cdist_cnt[j]++;
        }
      }
    }
  }

  if (! distance) {
    if (options.count("consensus-output-all")) {
      for (size_t i = 0; i < index.size(); ++i) {
        if (index.mark[i]) cout << ">" << index[i] << endl << index(i) << endl;
      }
    }
    cout << ">" << options["consensus-name"].as<string>() << endl;
    cout << out << endl;
  } else {
    for (size_t j = 0; j < idx.size(); ++j) {
      cout << index[idx[j]] << "\t" << cdist_tot[j] << "\t" << cdist_cnt[j] << "\t" << 1.0*cdist_cnt[j]/cdist_tot[j] << endl;
    }
  }

  index.unload_all();
}


