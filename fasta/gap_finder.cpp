#include "fasta_stats.h"

void gap_finder(FaIdx& index, const cxxopts::Options& options) {
  int gap_length = options["gap-length"].as<int>();
  if (gap_length < 0) gap_length = 0;

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;
    index.load(i);

    int start = -1;
    int end = -1;
    int gap = -1;

    int last = 0;
    int cur;

    for (int k = 0; k < index.length(i); ++k) {
      cur = char2int(index(i,k));

      if (cur) {         // current base is informative
        if (!last) {     // last base was not informative
          if (gap < 0)   // there is no gap present -> new region
            start = k;  
          else           // extend an existing gap
            gap = -1;
        }
      } else {           // current base in uninformative
        if (last) {      // if the previous base was invormative
          gap = 1;       // set the gap counter
          end = k-1;     // set the end
        } else {
          if (gap > 0)   // if the gap was already opened
            gap++;       // extend the gap
        }
      }

      // if there is a gap, and it's longer than the acceptiable length,
      // terminate region
      if (gap > gap_length) {
        cout << index[i] << " " << start << " " << end << " " << end-start << endl;
        gap = -1;
      }

      last = cur;
    }
  }
}


