#include "fasta_stats.h"

#include "../duo.h"

void count_snps(FaIdx& index, const cxxopts::Options& options) {
  int window = options["window"].as<int>();
  int load_window = options["load"].as<int>();
  int verbose = options.count("verbose");
  int type = (options["type"].as<string>() == "aa") ? 2 : 1;
  int use_duo = options.count("snps-duo");

  int min_cnt = options.count("snps-min-cnt") ? options["snps-min-cnt"].as<int>() : -1;
  double min_freq = options.count("snps-min-freq") ? options["snps-min-freq"].as<double>() : -1.0;
  int min_type = (min_freq > 0.0) ? 2 : 1;
  if (min_type == 1 && min_cnt < 0) min_cnt = 1;

  if (window == 0) return;

  if (load_window < window) load_window = window;
  load_window = load_window - (load_window % window);

  int len = index.max_full_len();
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  if (rbeg < 1) rbeg = 1;
  if (rend < 1 || rend > len) rend = len;

  vector<int> total(window,0);
  vector<int> majfreq(window,0);
  vector<int> nalleles;
  nalleles.reserve(window);

  map<uint32_t,uint16_t> dcnt;

  if (! use_duo) {
    cout << "beg\tend\ttotal_mean\ttotal_var\tsnps\tnalleles_mean\tnalleles_var\tminor_cnt" << endl;
  }

  for (int beg = rbeg; beg <= rend; beg += window) {
    if ((beg-rbeg) % load_window == 0) {
      if (verbose) {
        cerr << "Loading sequences " 
             << beg << "-" << beg+load_window-1 << "..." << flush;
      }
      index.load_all(beg,beg+load_window-1);
      if (verbose)  cerr << "done." << endl;
    }

    int snps = 0;
    int end = (beg+window < rend) ? beg+window-1 : rend;
    int tlen = end-beg;

    fill(total.begin(),total.end(),0);
    nalleles.clear();

    int k, j;
    for (k = beg, j = 0; k <= end; ++k, ++j) {
      map<char,int> snp;

      auto cnts = index.counts2(k);
      int cnt_maj = get_major(cnts);

      for (size_t i = 0; i < index.size(); ++i) {
        if (! index.mark[i]) continue;

        char c = index.get(i,k);
        int ci = char2int(c);

        if (type == 2) {
          switch (c) {
            case '.':
            case '-':
            case 'X':
              break;

            default:
              total[j]++;                 // add total number of informative basis
              snp[c]++;
              break;
          }
        } else {
          if (ci != HTS_N && ci != HTS_ANY) {
            total[j]++;                 // add total number of informative basis
            snp[c]++;
          }
        }
      }

      // get majority frequency
      majfreq[j] = 0;
      if (type != 2) { // DNA
        for (const auto& s: snp) { 
          if (char2int(s.first) & cnt_maj) majfreq[j] += s.second;
        }
      } else { // AA
        for (const auto& s: snp) {
          if (s.second > majfreq[j]) majfreq[j] = s.second;
        }
      }

      int minor_cnt = total[j]-majfreq[j];

      if (use_duo) {
        uint32_t ix = duo(total[j],minor_cnt);
        dcnt[ix]++;
      } else {
        double rel_freq = 1.0-1.0*majfreq[j]/total[j];

        if (type == 2) { 
          if ((min_type == 2 && rel_freq >= min_freq) || (min_type == 1 && minor_cnt >= min_cnt)) {
            snps++;
          }
        } else {
          if (pureInt(cnt_maj) == 0) {
            if (verbose) cerr << " " << k << " " << int2char(cnt_maj) << " => ambig" << endl;
            snps++;
          } else {
            if ((min_type == 2 && rel_freq >= min_freq) || (min_type == 1 && minor_cnt >= min_cnt)) {
              snps++;
              if (verbose) cerr << " " << k << " " << int2char(cnt_maj) << " " << majfreq[j] << " " << total[j] << " " << rel_freq << endl;
            }
          }
        }

        int _nalleles = snp.size();
        if (_nalleles > 1) nalleles.push_back(_nalleles);
      }
    }

    if (use_duo) {
      cout << "#name\tduo\ttotal\tnsnps\tcnt" << endl;
      for (const auto& x: dcnt) {
        cout << beg << "\t" << end << "\t"
             << x.first << "\t"
             << duo1(x.first) << "\t" 
             << duo2(x.first) << "\t"
             << x.second << endl;
      }
    } else {
      double t_mean = 0.0, t_var = 0.0;
      t_mean = gsl_stats_int_mean(total.data(),1,tlen);
      if (total.size() > 1) {
        t_var  = gsl_stats_int_variance_m(total.data(),1,tlen,t_mean);
      }

      double a_mean = 0.0, a_var = 0.0;
      a_mean = gsl_stats_int_mean(nalleles.data(),1,nalleles.size());
      if (nalleles.size() > 1) {
        a_var  = gsl_stats_int_variance_m(nalleles.data(),1,nalleles.size(),a_mean);
      }

      double minority_total = 0.0;
      int minority_cnt = 0;
      for (size_t j = 0; j < majfreq.size(); ++j) {
        if (total[j] > 0 && majfreq[j] < total[j]) {
          minority_total += (total[j]-majfreq[j]);
          minority_cnt++;
        }
      }

      cout << beg << "\t" << end << "\t";
      cout << t_mean << "\t" << t_var << "\t" << snps << "\t"
           << a_mean << "\t" << a_var << "\t"
           << (minority_cnt > 0 ? minority_total/minority_cnt : 0.0) << endl;
    }
  }

}


