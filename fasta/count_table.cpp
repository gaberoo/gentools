#include "fasta_stats.h"

void count_table(FaIdx& index, const cxxopts::Options& options) {
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();
  int len = index.max_full_len();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  string filename = options["count-table-groups"].as<string>();

  vector<string> groups(index.size(),"");

  string next_name;
  string next_grp;
  ifstream in(filename);
  while (in >> next_name >> next_grp) {
    size_t p = index.find(next_name);
    if (p < index.size()) groups[p] = next_grp;
  }
  in.close();

  vector<string> uniq_grps(groups);
  sort(uniq_grps.begin(),uniq_grps.end());
  auto uniq_end = unique(uniq_grps.begin(),uniq_grps.end());
  uniq_end = remove(uniq_grps.begin(),uniq_end,"");
  uniq_grps.resize(distance(uniq_grps.begin(),uniq_end));

  int gidx = 0;
  map<string,int> grp_idx;
  for (auto g: uniq_grps) grp_idx[g] = gidx++;

  if (options.count("verbose")) cerr << "Loading FASTA..." << flush;
  index.load_all(rbeg,rend);
  if (options.count("verbose")) cerr << "done." << endl;

  int ncol = uniq_grps.size();
  int nrow = 4;

  vector<double> fact(index.size()+1,0.0);
  fact[0] = fact[1] = 0.0;
  for (size_t i = 2; i < fact.size(); i++) fact[i] = fact[i-1] + log(i);

  if (rng == NULL) rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,time(NULL));

  for (int k = rbeg; k <= rend; ++k) {
    if (options.count("verbose") == 1) {
      fprintf(stderr,"%8d | %d\r",k-rbeg,rend-rbeg+1);
    }
    vector<double> grp_cnt(nrow*ncol,0.0);

    for (size_t i = 0; i < index.size(); ++i) {
      if (!index.mark[i] || groups[i] == "") continue;
      int c = idx(index.get(i,k));
      if (c >= 0) grp_cnt[grp_idx[groups[i]]*nrow+c] += 1.0;
    }

    int col_cnt = 0;
    vector<int> col_sums(ncol,0);
    vector<int> row_sums(nrow,0);
    for (int j = 0; j < ncol; ++j) {
      col_sums[j] = grp_cnt[j*nrow  ] + grp_cnt[j*nrow+1] +
                    grp_cnt[j*nrow+2] + grp_cnt[j*nrow+3];
      if (col_sums[j] > 0) col_cnt++;
      row_sums[0] += grp_cnt[j*nrow];
      row_sums[1] += grp_cnt[j*nrow+1];
      row_sums[2] += grp_cnt[j*nrow+2];
      row_sums[3] += grp_cnt[j*nrow+3];
    }
    int row_cnt = (row_sums[0]>0) + (row_sums[1]>0) + (row_sums[2]>0) + (row_sums[3]>0);
    int ntot = accumulate(row_sums.begin(),row_sums.end(),0);

    double prt = 1.0;
    double pre = 1.0;

    if ((col_cnt > 1 && row_cnt > 1) || options.count("count-table-all")) {
      fexact_(&nrow,&ncol,grp_cnt.data(),&nrow,&expect,&percent,&emin,&prt,&pre);

      if (pre < 0.0) {   // error in fexact
        if (options.count("verbose") > 1) cerr << "Cannot calculate exact fisher. Using simulations..." << endl;
        pre = 0.0;
        double statistic = 0.0;
        for (int j = 0; j < nrow*ncol; ++j) statistic -= fact[grp_cnt[j]];

        vector<int> observed(nrow*ncol);
        vector<int> jwork(ncol);
        for (int iter = 0; iter < fexact_B; ++iter) {
          rcont2(&nrow,&ncol,row_sums.data(),col_sums.data(),
                 &ntot,fact.data(),jwork.data(),observed.data(), 
                 &unif_rand);
          /* Calculate log-prob value from the random table. */
          double ans = 0.0;
          for (int j = 0; j < nrow*ncol; ++j) ans -= fact[observed[j]];
          if (ans <= statistic) pre += 1.0;
        }
        pre = (pre+1.0)/(fexact_B+1.0);
      }

      printf("%-8d %-16g %-16g",k,prt,pre);
      for (int j = 0; j < ncol; ++j) {
        printf("%4d %4d %4d %4d",
               (int) grp_cnt[j*nrow  ],(int) grp_cnt[j*nrow+1],
               (int) grp_cnt[j*nrow+2],(int) grp_cnt[j*nrow+3]);
      }
      printf("\n");
    }
  }
  if (options.count("verbose") == 1) fprintf(stderr,"\n");

  gsl_rng_free(rng);
  rng = NULL;
}


