#include "fasta_stats.h"

void binSum(FaIdx& index, const cxxopts::Options& options) {
  int len = index.max_full_len();

  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  if (rbeg < 1) rbeg = 1;
  if (rend < 1) rend = len;

  index.load_all(rbeg,rend);

  for (size_t i = 0; i < index.size(); ++i) {
    if (! index.mark[i]) continue;

    int cnt = 0;

    for (int k = rbeg; k < rend; ++k) {
      if (index(i,k) == '1') cnt++;
    }

    cout << index[i] << " " << cnt << endl;
  }
}


