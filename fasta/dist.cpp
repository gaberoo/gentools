#include "fasta_stats.h"

void dist(FaIdx& index, const cxxopts::Options& options) {
  int rbeg = options["beg"].as<int>();
  int rend = options["end"].as<int>();

  /*
  if (options.count("dist-from")) {
    auto dist_from = options["dist-from"].as< vector<string> >();
    for (string from: dist_from) index.mark_seq(from);
  }
  */

  if (options.count("verbose")) cerr << "Loading sequences..." << flush;
  index.load_marked(rbeg,rend);
  if (options.count("verbose")) cerr << "done." << endl;

  pairwise_distances(index,options);
}


