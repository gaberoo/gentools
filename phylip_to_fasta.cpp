#include <cstdlib>
#include <iostream>
using namespace std;

#include "Phylip.h"

int main(int argc, char** argv) {
  if (argc < 2) return EXIT_FAILURE;

  Phylip phy;
  phy.read(argv[1]);
  phy.to_fasta();

  return 0;
}

