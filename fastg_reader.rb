#!/usr/bin/env ruby

require 'json'
require 'optparse'

options = {
  samtools: "samtools"
}

OptionParser.new do |opts|
  opts.banner = "Usage: fastg_reader.rb [options]"
  opts.on("-S", "--samtools [PATH]", "Path to samtools") { |s| options[:samtools] = s }
  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| options[:verbose] = v }
  opts.on("-t", "--type [TYPE]", "Output type") { |t| options[:type] = t }
end.parse!

file = ARGV.first

idx_name ="#{file}.fai"
`#{options[:samtools]} faidx #{file}` unless File.exists? idx_name

if options[:type] == "json"
  nodes = []
  File.open idx_name do |index|
    index.each_line do |line|
      row = line.split
      node = row[0].split /[:;]/

      main = node.shift
      nbr = node.shift

      info = main.split(/_/)
      hash = {}
      i = 0
      while i < info.length
        hash[info[i]] = info[i+1]
        i += 2
      end

      nid = hash["NODE"].to_i
      nodes[nid] ||= {}

      nodes[nid][:left] ||= []
      nodes[nid][:right] ||= []

      nodes[nid][:length] = hash["length"].to_i
      nodes[nid][:cov] = hash["cov"].to_i

      if nbr
        if main =~ /'$/
          nbr.split(/,/).each do |n|
            nodes[nid][:left] << [ n.split(/_/)[1].to_i, (n =~ /'$/).nil? ]
          end
        else
          nbr.split(/,/).each do |n|
            nodes[nid][:right] << [ n.split(/_/)[1].to_i, (n =~ /'$/).nil? ]
          end
        end
      end
    end
  end

  puts nodes.to_json

elsif options[:type] == "edgelist"
  File.open idx_name do |index|
    index.each_line do |line|
      row = line.split
      node = row[0].split /[:;]/

      main = node.shift
      nbr = node.shift

      info = main.split(/_/)
      hash = {}
      i = 0
      while i < info.length
        hash[info[i]] = info[i+1]
        i += 2
      end

      nid = hash["NODE"].to_i
      if nbr
        nbr.split(/,/).each do |n|
          puts "#{nid} #{n.split(/_/)[1].to_i}"
        end
      end
    end
  end
end
