#include <iostream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <fstream>
using namespace std;

#include "hts.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <gsl/gsl_statistics.h>
#include <gsl/gsl_statistics_int.h>

int seg_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) if (alleles[i] > 0) cnt++;
  return cnt;
}

int tot_sites(int n, int* alleles) {
  int cnt = 0;
  for (int i = 0; i < n; ++i) cnt += alleles[i];
  return cnt;
}

int main(int argc, char* argv[]) {
  string file1 = argv[1];   // BCF file
  string ATfile = argv[2];  // ???

  int min_depth = (argc > 3) ? atoi(argv[3]) : 3;
  int type = (argc > 4) ? atoi(argv[4]) : 0;
  int type_cnt = (argc > 5) ? atoi(argv[5]) : 0;

  vector<size_t> cnt;
  vector<size_t> seg;
  vector<size_t> dp;
  vector<double> prob;

  vector<int> id;
  vector<int> AT;

  vector<size_t> ATcnt(5,0);
  vector<size_t> ATseg(5,0);
  vector<size_t> ATdp(5,0);
  vector<double> ATprob(5,0.0);

  gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,time(NULL));

  int a, b;
  ifstream ATf(ATfile.c_str());
  while (ATf >> a >> b) {
    id.push_back(a);
    AT.push_back(b);
  }
  ATf.close();

  vector<bool> keep(AT.size(),true);

  vector<int> tid;
  vector<int> tid_all;

  if (type > 0) {
    tid_all.clear();
    for (size_t i = 0; i < AT.size(); i++) {
      if (AT[i] == type) tid_all.push_back(i);
    }
    tid.resize(type_cnt,0);
    gsl_ran_choose(rng,tid.data(),type_cnt,tid_all.data(),tid_all.size(),sizeof(int));
    keep.clear();
    keep.resize(AT.size(),false);
    for (size_t i = 0; i < tid.size(); ++i) keep[tid[i]] = true;
  } else if (type_cnt > 0) {
    keep.clear();
    keep.resize(AT.size(),false);
    for (int k = 1; k <= 4; ++k) {
      tid_all.clear();
      for (size_t i = 0; i < AT.size(); i++) { if (AT[i] == k) tid_all.push_back(i); }
      tid.clear();
      tid.resize(type_cnt,0);
      if (tid_all.size() > 0) {
        gsl_ran_choose(rng,tid.data(),type_cnt,tid_all.data(),tid_all.size(),sizeof(int));
        for (size_t i = 0; i < tid.size(); ++i) { keep[tid[i]] = true; }
      }
    }
  }

  // Open files for reading

  // cerr << "Opening files...";
  htsFile *bcf1 = hts_open(file1.c_str(),"r");
  // cerr << "done." << endl;
  if (bcf1 == NULL) { cerr << "Unable to open file 1." << endl; return 0; }

  bcf_hdr_t *header = bcf_hdr_read(bcf1);
  if (header == NULL) { cerr << "Unable to read header 1." << endl; return 0; }

  bcf1_t *record = bcf_init();

  int *ad = NULL;
  int ndst = -1;
  int ret = -1;

  int nsamp = bcf_hdr_nsamples(header);
  /*
  for (int i = 0; i < nsamp; ++i) {
    cout << i << " " << bcf_hdr_int2id(header,BCF_DT_SAMPLE,i) << endl;
  }
  */

  cnt.resize(nsamp,0);
  seg.resize(nsamp,0);
  prob.resize(nsamp,0.0);
  dp.resize(nsamp,0);

  int na;
  int ss = 0;
  int tot = 0;

  int AD[20];
  double p[20];

  while (read_next(bcf1,header,record) == 0) {
    na = record->n_allele;

    ret = bcf_get_format_int32(header,record,"AD",&ad,&ndst);
    if (! ret) continue;

    // cout << record->n_allele << " " << ndst << " :: ";

    memset(AD,0,20*sizeof(int));

    for (int i = 0; i < nsamp; ++i) {
      ss = seg_sites(na,ad+i*na);
      // tot = tot_sites(na,ad+i*na);
      tot = ptot(ad+i*na, p+AT[i]*4,record->n_allele,record->d.allele);

      if (keep[i]) {
        ctot(ad+i*na,AD+AT[i]*4,record->n_allele,record->d.allele);
      }

      if (tot >= min_depth) {
        double x = gsl_stats_max(p+AT[i]*4,1,4);
        if (ss > 1) seg[i]++;
        if (x < 2./3.) prob[i] += 1.0;
        cnt[i]++;
        dp[i] += tot;
        // double x = 1.0-pow_sum(p+AT[i]*4,4,min_depth);
        // prob[i] += x;
        // cerr << x << " " << 1*(ss>1) << endl;
      }
      /*
      cout << bcf_seqname(header,record) << " " << AT[i] << " "
           << i << " " << na << " " << tot << " " << ss << " "
           << endl;
      */
    }

    if (type > 0) {
      ss  = seg_sites(4,AD+type*4);
      tot = tot_sites(4,AD+type*4);

      if (tot >= min_depth) { 
        ATcnt[0]++; ATcnt[type]++; 
        if (ss > 1) { ATseg[0]++; ATseg[type]++; }
        ATdp[0] += tot; ATdp[type] += tot;
        double x = 1.0*gsl_stats_int_max(AD+type*4,1,4)/tot;
        if (x < 2./3.) ATprob[type] += 1.0;
        // x += gsl_pow_int(1.*AD[type*4]/tot,min_depth);
        // x += gsl_pow_int(1.*AD[type*4+1]/tot,min_depth);
        // x += gsl_pow_int(1.*AD[type*4+2]/tot,min_depth);
        // x += gsl_pow_int(1.*AD[type*4+3]/tot,min_depth);
        // ATprob[type] += (1.0-x);
      }
    } else {
      for (int k = 1; k <= 4; ++k) { 
        ss  = seg_sites(4,AD+k*4);
        tot = tot_sites(4,AD+k*4);
        if (tot >= min_depth) {
          ATcnt[0]++; ATcnt[k]++;
          ATdp[0] += tot; ATdp[k] += tot;
          if (ss > 1) { ATseg[0]++; ATseg[k]++; }

          double x = 1.0*gsl_stats_int_max(AD+k*4,1,4)/tot;
          if (x < 2./3.) ATprob[k] += 1.0;
          // double x = 0.0;
          // x += gsl_pow_int(1.*AD[k*4]/tot,min_depth);
          // x += gsl_pow_int(1.*AD[k*4+1]/tot,min_depth);
          // x += gsl_pow_int(1.*AD[k*4+2]/tot,min_depth);
          // x += gsl_pow_int(1.*AD[k*4+3]/tot,min_depth);
          // ATprob[k] += (1.0-x);
        }
      }
      double x = 1.0*gsl_stats_int_max(AD,1,4)/tot;
      if (x < 2./3.) ATprob[0] += 1.0;
    }

    // cout << endl;
  }

  if (type_cnt > 0) {
    if (type > 0) {
      cout << "C " << type_cnt << " " << type << " " << ATcnt[type] << " " << ATseg[type] << " " << 1.*ATseg[type]/ATcnt[type] << endl;
    } else {
      for (int k = 0; k <= 4; ++k) { 
        cout << "C " << type_cnt << " " << k << " " << ATcnt[k] << " " << ATseg[k] << " " << 1.*ATseg[k]/ATcnt[k] << endl;
      }
    }
  } else {
    for (int i = 0; i < nsamp; ++i) { 
      cout << "A " << id[i] << " " << AT[i] << " " << cnt[i] << " " << seg[i] << " "
           << prob[i]/cnt[i] << " " << dp[i] << endl;
    }
    for (int k = 0; k <= 4; ++k) { 
      cout << "B " << 0 << " " << k << " " << ATcnt[k] << " " << ATseg[k] << " "
           << ATprob[k]/ATcnt[k] << " " << ATdp[k] << endl;
    }
  }

  hts_close(bcf1);

  bcf_hdr_destroy(header);
  bcf_destroy(record); 

  gsl_rng_free(rng);

  return 0;
}
