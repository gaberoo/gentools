#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

int main(int argc, char** argv) {
  string filename = argv[1];
  string index = argv[2];
  string out_fn = argv[3];

  dna_tools::FastaFile f(filename,index);

  FILE* out = fopen(out_fn.c_str(), "w");
  FILE* in = fopen(filename.c_str(), "r");

  for (size_t i = 0; i < f.size(); ++i) {
    cerr << i+1 << "/" << f.size() << endl;
    fputs(">", out);
    fputs(f[i].name().c_str(), out);
    fputs("\n", out);
    f[i].open_file();
    f[i].read_seq();
    f[i].write_seq(out);
    fputs("\n", out);
    f[i].close_file();
  }

  fclose(in);
  fclose(out);

  return 0;
}

