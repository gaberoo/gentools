#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0], "Fragment a FASTA file");
  options.positional_help("[optional args]");

  options.add_options()
    ("f,input",   "Input file", cxxopts::value<string>(), "FILE")
    ("i,index",   "Index file", cxxopts::value<string>()->default_value(""))
    ("l,length",  "Fragment lengths", cxxopts::value<int>()->default_value("1000"))
    ("n,num",     "Number of fragments", cxxopts::value<int>()->default_value("100"))
    ("v,verbose", "Verbose")
    ("h,help",    "Print help")
  ;

  vector<string> _pos = { "input" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    cout << options.help({ "" }) << endl;
    exit(0);
  }

  // -------------------------------------------------------------------------

  string filename = options["input"].as<string>();
  string index = options["index"].as<string>();
  int length = options["length"].as<int>();
  int num = options["num"].as<int>();
  
  gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,time(NULL));

  dna_tools::FastaFile f(filename,index);
  if (options.count("verbose")) cerr << "Reading sequences...";
  f.read_seqs();
  if (options.count("verbose")) cerr << "done." << endl;

  vector<double> probs;
  for (size_t i = 0; i < f.size(); ++i) probs.push_back(1.0*f[i].length());
  gsl_ran_discrete_t* ran_dist = gsl_ran_discrete_preproc(f.size(),probs.data());

  size_t k;
  size_t pos;
  for (int i = 0; i < num; ++i) {
    k = gsl_ran_discrete(rng,ran_dist);
    pos = gsl_rng_uniform_int(rng,f[k].length());
    cout << ">Seq-" << i << endl;
    for (size_t j = pos; j < pos+length; ++j) {
      cout << f[k][j];
    }
    cout << endl;
  }

  gsl_rng_free(rng);

  return 0;
}

