#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
using namespace std;

#include "htslib/hts.h"
#include "htslib/sam.h"

#include <gsl/gsl_rng.h>

int get_id(double r, size_t n, const double p[]) {
  int k = r*n;
  double R = r*p[n-1];
  if (R < p[k]) {
    while (k > 0 && R < p[k-1]) --k;
  } else {
    while (k < n-1 && R > p[k]) ++k;
  }
  return k;
}

void adjust_prob(size_t i, double x, size_t n, double p[]) {
  if (x > p[i]) x = p[i];
  for (size_t k = i; k < n; ++k) p[k] -= x;
}

int choose_id(double x, size_t n, double p[], gsl_rng *rng) {
  double r = gsl_rng_uniform(rng);
  int k = get_id(r,n,p);
  adjust_prob(k,x,n,p);
  return k;
}

int main(int argc, char* argv[]) {
  if (argc < 3) return 0;

  string cnt_fn(argv[1]);
  string out_dir(argv[2]);
  long unsigned int seed = (argc > 3) ? atoi(argv[3]) : time(NULL);

  cout << "# SAM file randomization" << endl
       << endl
       << "cnts = " << cnt_fn << endl
       << "seed = " << seed << endl
       << "dir  = " << out_dir << endl
       << endl;

  string filename;

  int id;
  size_t nreads;
  uint64_t totlen;

  vector<int> ids;
  vector<uint64_t> counts;
  uint64_t totalCounts = 0;

  vector<double> probs(1,0.0);

  // cerr << "Reading counts (" << cnt_fn << ")..." << flush;
  ifstream cnt_in(cnt_fn.c_str());
  while (! cnt_in.eof()) {
    cnt_in >> id;
    if (cnt_in.eof()) break;
    cnt_in >> nreads >> totlen;

    ids.push_back(id);
    counts.push_back(totlen);
    totalCounts += totlen;
  }
  // cerr << "done." << endl;

  for (size_t i = 0; i < ids.size(); ++i) {
    probs.push_back(probs.back() + 1.0*counts[i]);
  }

  gsl_rng *rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,seed);

  getline(cin,filename);
  htsFile* in = hts_open(filename.c_str(), "r");
  bam_hdr_t* main_head = sam_hdr_read(in);
  hts_close(in);

  vector<htsFile*> bamOut;
  for (size_t i = 0; i < ids.size(); ++i) {
    ostringstream outFn;
    outFn << out_dir << "/" << ids[i] << ".bam";
    // cerr << "Opening " << outFn.str().c_str() << endl;
    bamOut.push_back(hts_open(outFn.str().c_str(),"wb"));
    sam_hdr_write(bamOut.back(),main_head);
  }

  while (cin) {
    // open BAM for reading
    htsFile* in = hts_open(filename.c_str(), "r");
    if (in == NULL) {
      cerr << "Unable to open BAM/SAM file: " << filename << endl;
      continue;
    }

    bam_hdr_t *header = sam_hdr_read(in);
    bam1_t *aln = bam_init1();

    int k;
    while (sam_read1(in,header,aln) >= 0) {
      k = choose_id(1.0*aln->core.l_qseq,probs.size(),probs.data(),rng);
      sam_write1(bamOut[k-1],main_head,aln);
    }

    bam_destroy1(aln);
    bam_hdr_destroy(header);
    hts_close(in);

    getline(cin,filename);
  }

  while (bamOut.size() > 0) {
    hts_close(bamOut.back());
    bamOut.pop_back();
  }

  bam_hdr_destroy(main_head);
  gsl_rng_free(rng);

  return 0;
}

