#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <fstream>
#include <regex>
using namespace std;

#include "hts.h"
#include "duo.h"

extern "C" {
#include "codon.h"
}

#include <gsl/gsl_statistics.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_sort_int.h>
#include <gsl/gsl_heapsort.h>

#include "cxxopts.hpp"

#include "gff/GFFFile.h"

/****************************************************************************/

#ifdef DEBUG
#define DEBUG_PRINT(fmt, args...) \
  fprintf(stderr, "DEBUG: %s:%d:%s(): " fmt, \
            __FILE__, __LINE__, __func__, ##args)
#define DEBUG_PRINT_APPEND(fmt, args...) \
  fprintf(stderr, fmt, ##args)
#else
#define DEBUG_PRINT(fmt, args...)
#define DEBUG_PRINT_APPEND(fmt, args...)
#endif

/****************************************************************************/

int coverage 
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int sliding_coverage 
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int coverage_scaled
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int consensus
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int stats
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int gff_stats
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int mean_hs
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int variable_sites_all
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int zib
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int zib_dp4
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int true_snps
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int check_pairs
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int pair_dist
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int count_table
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int fst
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

int diversity
(htsFile* bcf1, bcf_hdr_t* header, bcf_srs_t* sr, bcf1_t* record, 
 const vector<string>& sample_names, const cxxopts::Options& options);

/****************************************************************************/

/*
int seg_sites();
int seg_sites_multi();
int seg_sites_pl();
int seg_hist();
int seg_list();
int site_hist();
int coverage();
*/

/****************************************************************************/

size_t nchars(const string& seq);
int delta_pl(int n, int* pl);

/****************************************************************************/

template<typename T> T cnt_seg_sites(int n, T* alleles) {
  T cnt = 0;
  for (int i = 0; i < n; ++i) if (alleles[i] > 0) cnt++;
  return cnt;
}

/****************************************************************************/

template<typename T> T tot_sites(int n, T* alleles) {
  T cnt = 0;
  for (int i = 0; i < n; ++i) cnt += alleles[i];
  return cnt;
}

/****************************************************************************/

template<typename T>
T arr_max(T* p) {
  T m1 = (p[1] > p[0]) ? p[1] : p[0];
  T m2 = (p[3] > p[2]) ? p[3] : p[2];
  return (m1 > m2) ? m1 : m2;
}


