#include <stdio.h>

int prterr_(int* code, const char* message) {
  fprintf(stderr,"FEXACT error %d: %s\n",*code,message);
  return 0;
}
