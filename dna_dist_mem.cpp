#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0], "Calculate pairwise distances between sequences in a multi fasta file");

  string filename;
  string index = "";
  char out_format = 'l';
  int min_overlap = 1;

  options.add_options()
    ("f,filename", "Input file", cxxopts::value<string>(filename))
    ("i,index",    "Index file", cxxopts::value<string>(index))
    ("O,outFmt",   "Output format", cxxopts::value<char>(out_format))
    ("m,minOverlap", "Minimum base overlap", cxxopts::value<int>(min_overlap))
    ("v,verbose",  "Verbose")
  ;

  vector<string> _pos = { "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  dna_tools::FastaFile f(filename,index);
  if (options.count("verbose")) cerr << "Reading sequences...";
  f.read_seqs();
  if (options.count("verbose")) cerr << "done." << endl;

  size_t n = f.size();

  double* dmat = NULL;
  uint64_t* tmat = NULL;

  if (out_format == 'p') {
    dmat = new double[n*n];
    tmat = new uint64_t[n*n];
    memset(dmat, 0, n*n*sizeof(double));
    memset(tmat, 0, n*n*sizeof(uint64_t));
  }

  size_t m = 0;
  if (options.count("verbose")) cerr << "Calculating pairwise distances:" << endl;
#pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {

#pragma omp critical
    if (options.count("verbose")) cerr << "   " << m++ << "/" << n << "\r" << flush;

    for (size_t j = 0; j < i; ++j) {
      pair<double,uint64_t> cnt = f.pairwise_distance(i,j);
      if (out_format == 'l') {
#pragma omp critical
        {
          cout << f[i].name() << " " << f[j].name() << " " 
               << " " << cnt.first << " " << cnt.second << " ";
          if (cnt.second > 0) cout << 1.0*cnt.first/cnt.second << endl;
          else cout << "NA" << endl;
        }
      } else {
        dmat[i*n+j] = cnt.first;
        dmat[j*n+i] = cnt.first;
        tmat[i*n+j] = cnt.second;
        tmat[j*n+i] = cnt.second;
      }
    }
    if (out_format == 'l') {
#pragma omp critical
      cout << f[i].name() << " " << f[i].name() << " " 
           << 0 << " " << 0 << " " << 0.0 << endl;
    }
  }
  if (options.count("verbose")) cerr << endl;

  if (out_format == 'p') {
    cout << n << endl;
    for (size_t i = 0; i < n; ++i) {
      cout << f[i].name() << " ";
      for (size_t j = 0; j < n; ++j) {
        if (i == j) {
          cout << 0.0 << " ";
        } else if (tmat[i*n+j] >= min_overlap) {
          cout << dmat[i*n+j]/tmat[i*n+j] << " ";
        } else {
          cout << -99.0 << " ";
        }
      }
      cout << endl;
    }
  }

  if (dmat != NULL) delete[] dmat;
  if (tmat != NULL) delete[] tmat;

  return 0;
}

