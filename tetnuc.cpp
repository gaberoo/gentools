#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <string>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

int main(int argc, char** argv) {
  string filename = argv[1];
  string index = (argc > 2) ? argv[2] : "";

  dna_tools::FastaFile f(filename,index);
  // f.read_seqs();

  uint64_t* freqs = new uint64_t[256];
  memset(freqs,0,256*sizeof(uint64_t));

  for (size_t j = 0; j < f.size(); ++j) {
    memset(freqs,0,256*sizeof(uint64_t));
    f[j].open_file();
    f[j].tet_nuc_freq(freqs);
    for (size_t i = 0; i < 256; ++i) {
      if (freqs[i] > 0) {
        cout << f[j].name() << " " 
             << i << " " 
             << freqs[i] << " "
             << 1.0*freqs[i]/(f[j].length()-3) << endl;
      }
    }
    f[j].close_file();
  }

  return 0;
}

