# Various tools for parsing genomic data and extracting interesting information

Gabriel E Leventhal
gaberoo@mit.edu | gabriel@leventhal.ch
2017-2018

## get_genus.rb

Retrieve taxonomic information from NCBI.

## dna_stats / fasta_stats

Operates on FASTA files

## bamcnt

Operates on BAM/SAM files.
Actions:

1. `table`: Tabulate matches, potentially grouping by genus/category
   and normalizing by length.  
   Options:  
   a. `-c`: Threshold normalized mapping score for inclusion
   b. `-G`: Mapping file for genus/category. Columns:
      1-Chromosome id
      2-Chromosome name
      3-Taxonomy ID
      4-Genus name
   c. `-L`: Lengths for normalization. Columns:
      1-Chromosome name
      2-Empty
      3-Length
   d. `--plasmids`: Include entries that match "plasmid"
   e. `--ignore`  : Ignore chromosomes that don't have a corresponding
      entry in the genus file

  Output:
    a. Accession number
    b. Number of reads recruited (possibly normalized by length)
    c. ... relative to all reads
    d. ... relative to mapped reads
    e. H; Shannon entropy of mapping within the genus
    f. S; Total number of taxonomic IDs within the genus
    g. E; Evenness (H/log(S))
  }
}

    

## vcf_stats

Operates on VCF/BCF files.
