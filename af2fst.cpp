#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
using namespace std;

#include "AlleleFreqs.h"

int main(int argc, char* argv[]) {
  vector<dna_tools::AlleleFreqs> A;
  string filename;

  while (cin >> filename) {
    A.push_back(dna_tools::AlleleFreqs());
    dna_tools::AlleleFreqs& af = A.back();

    cerr << "Reading " << filename << "..." << endl;

    stringstream fn(filename);
    string id = "";
    while (getline(fn,id,'/'));
    stringstream name(id);
    vector<string> last;
    while (getline(name,id,'.')) last.push_back(id);
    if (last.size() > 1) last.pop_back();
    af.id = accumulate(last.begin(),last.end(),string(""));

    ifstream in(filename);
    af.read(in,1);
    in.close();
  }

  for (int k = 1; k < argc; ++k) {
    size_t min_depth = atoi(argv[k]);
    for (size_t i = 1; i < A.size(); ++i) {
      size_t j;
#pragma omp parallel for
      for (j = 0; j < i; ++j) {
        int hd;
        vector<double> fst;
        size_t cnt = A[i].FST(A[j],fst,hd,min_depth);
        double fst_tot = accumulate(fst.begin(),fst.end(),0.0);
#pragma omp critical
        cout << A[i].id << " " << A[j].id << " " << cnt << " " 
             << fst_tot << " " << fst_tot/cnt << " " 
             << hd << " " << 1.0*hd/cnt << " " << min_depth << endl;
      }
    }
  }


  return 0;
}

