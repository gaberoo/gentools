#!/usr/bin/env ruby

fai = ARGV.shift
bams = ARGV

contigs = {}
File.open(fai).each_line do |line|
  x = line.split /\s+/
  contigs[x[0]] = x[1].to_i
end

depths = []
bams.each do |bam|
  depth = {}
  depth2 = {}
  last_contig = nil
  last_cnt = 0
  last_cnt_2 = 0
  IO.popen(["samtools", "depth", bam]) do |io|
    io.each_line do |line|
      x = line.split /\s+/
      if last_contig != x[0]
        if last_contig
          depth[last_contig] = last_cnt
          depth2[last_contig] = last_cnt_2
        end
        last_contig = x[0]
        last_cnt = depth[x[0]] || 0
        last_cnt_2 = depth2[x[0]] || 0
      end
      last_cnt = last_cnt + x[2].to_i
      last_cnt_2 = last_cnt_2 + (x[2].to_i*x[2].to_i)
    end
  end
  if last_contig
    depth[last_contig] = last_cnt
    depth2[last_contig] = last_cnt_2
  end
  depths << { k1: depth, k2: depth2 }
end

contigs.keys.each do |contig|
  print contig

  depths.each do |depth|
    n = contigs[contig]
    m1 = 1.0*(depth[:k1][contig] || 0)/n
    m2 = 1.0*(depth[:k2][contig] || 0)/n
    v = m2-m1*m1 * n/(n-1)
    print "\t#{m1}\t#{v}"
  end

  print "\n"
end
