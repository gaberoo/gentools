#include "Sequence.h"

uint8_t dna_tools::set_nuc(const char aa) {
  uint8_t iaa = 0;
  switch (aa) {
    case 'A': case 'a': iaa = 0; break;
    case 'G': case 'g': iaa = 1; break;
    case 'C': case 'c': iaa = 2; break;
    case 'T': case 't': iaa = 3; break;
    default: break;
  }
  return iaa;
}

uint8_t dna_tools::tet_nuc(const char* dna) {
  uint8_t tn = 0;
  tn |= set_nuc(dna[0]); tn <<= 2;
  tn |= set_nuc(dna[1]); tn <<= 2;
  tn |= set_nuc(dna[2]); tn <<= 2;
  tn |= set_nuc(dna[3]);
  return tn;
}

void dna_tools::Sequence::read_seq() {
  // cerr << "Reading sequence from position " << _pos << "..." << flush;
  _seq.clear();
  fseek(_in,_pos,SEEK_SET);
  char c = 'n';
  while (c != '>') {
    c = get_next();
    if (c != '>') _seq.push_back(c);
  }
  // cerr << "done! " << _seq.size() << endl;
}

void dna_tools::Sequence::write_seq(FILE* _out) const {
  fwrite(_seq.data(),sizeof(char),_seq.size(),_out);
}

char dna_tools::Sequence::get_next() {
  char c;
  do { 
    c = fgetc(_in);
  } while (isspace(c) && ! feof(_in));
  if (feof(_in)) return '>';
  else return c;
}

string dna_tools::Sequence::to_str() const {
  string str;
  vector<char>::const_iterator c(_seq.begin());
  while (c != _seq.end()) str += *(c++);
  return str;
}

void dna_tools::Sequence::pad_seq(size_t new_len) {
  while (_seq.size() < new_len) _seq.push_back('n');
}

char dna_tools::Sequence::get(uint64_t pos) const {
  if (_seq.size() == 0) return get_from_file(pos);
  else return get_from_seq(pos);
}

char dna_tools::Sequence::get_from_seq(uint64_t pos) const {
  if (pos < _seq.size()) {
    return _seq.at(pos);
  } else {
    return 'n';
  }
}

char dna_tools::Sequence::get_from_file(uint64_t pos) const {
  char c = '(';
  if (pos >= _len) return 'n';
  uint64_t row = pos / _lineseq;
  uint64_t col = pos % _lineseq;
  uint64_t fp = _pos + row*_linelen + col;
  // cerr << _name << " " << pos << " " << _lineseq << " " << row << " " << col << " " << fp << endl;
  if (_in != NULL) {
    fseek(_in,fp,SEEK_SET);
    c = fgetc(_in);
  } else {
    c = '}';
  }
  return c;
}

void dna_tools::Sequence::tet_nuc_freq(uint64_t* counts) {
  if (_in == NULL) {
    cerr << "File not open." << endl;
    return;
  }

  // cerr << "Seeking front of file..." << endl;
  fseek(_in,_pos,SEEK_SET);

  char c = 'n';
  char mer[] = "AAAA";

  // get first 4-mer
  // cerr << "Reading first 4-mer..." << endl;
  c = get_next(); if (c != '>') mer[0] = c;
  c = get_next(); if (c != '>') mer[1] = c;
  c = get_next(); if (c != '>') mer[2] = c;
  c = get_next(); if (c != '>') mer[3] = c;

  // store first kmer
  uint8_t idx = tetnuc_table[tet_nuc(mer)];
  ++counts[idx];

  while (1) {
    c = get_next();
    if (c != '>') {
      memmove(mer,mer+1,3);
      mer[3] = c;
      // cout << mer << endl;
      idx = tetnuc_table[tet_nuc(mer)];
      ++counts[idx];
    } else {
      break;
    }
  }
}

void dna_tools::Sequence::base_cnts(size_t* cnts) const {
  cnts[0] = 0;
  cnts[1] = 0;
  cnts[2] = 0;
  cnts[3] = 0;
  for (size_t i = 0; i < size(); ++i) {
    switch (_seq[i]) {
      case 'a': case 'A': cnts[0]++; break;
      case 'g': case 'G': cnts[1]++; break;
      case 't': case 'T': cnts[2]++; break;
      case 'c': case 'C': cnts[3]++; break;
      default: break;
    }
  }
}



