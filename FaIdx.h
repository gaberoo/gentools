#ifndef __FAIDX_H__
#define __FAIDX_H__

#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <random>
#include <algorithm>
#include <regex>
#include <map>
using namespace std;

#include "hts.h"
#include "htslib/faidx.h"

int cmp(size_t i, const char* strA, const char* strB);

class FaIdx {
  public:
    FaIdx() {}
    FaIdx(string filename) : filename(filename) { open(filename); }
    inline virtual ~FaIdx() { unload_all(); }

    /************************************************************************/

    FaIdx& open(string filename);
    FaIdx& clear();
    FaIdx& unload(size_t i);
    FaIdx& unload_all();

    FaIdx& load(size_t i, int beg = 0, int end = 0);
    FaIdx& load_all(int beg = 0, int end = 0);
    FaIdx& load_marked(int beg = 0, int end = 0);
    FaIdx& load_all(regex pattern, int beg = 0, int end = 0);
    
    /* mark sequences for processing */
    inline FaIdx& mark_none() { mark.assign(name.size(),false); return *this; }
    inline FaIdx& mark_all() { mark.assign(name.size(),true); return *this; }
    inline FaIdx& mark_seq(string name, bool m = true) {
      size_t i = find(name);
      if (i < size()) mark[i] = m;
      return *this;
    }
    FaIdx& mark_seqs(string file);

    string info() const;

    char get1(size_t i, size_t pos) const;
    char rget(size_t i, int rpos) const;
    void rset(size_t i, int rpos, char a);

    inline char get(size_t i, size_t pos) const { return rget(i,pos-beg[i]); }
    inline void set(size_t i, size_t pos, char a) { rset(i,pos-beg[i],a); }

    array<int,4> counts(size_t pos) const;
    array<int,5> counts_n(size_t pos) const;
    array<double,4> counts2(size_t pos, const double* weights = NULL) const;
    map<char,size_t> count_aa(size_t pos) const;

    int cmp(size_t i, size_t a, size_t b) const;
    double cmp2(size_t i, size_t a, size_t b) const;

    pair<double,size_t> pairwise_distance(size_t a, size_t b) const;
    pair<size_t,size_t> pairwise_distance_pos(size_t a, size_t b, const vector<size_t>& pos) const;
    pair<size_t,size_t> pairwise_distance_pos(string a, string b, const vector<size_t>& pos) const;
    pair<size_t,size_t> pairwise_distance_binary(size_t a, size_t b) const;
    pair<size_t,size_t> pairwise_distance_char(size_t a, size_t b) const;
    vector<size_t> pairwise_distance_rng(size_t a, size_t b, size_t seed, size_t reps) const;

    int nucleotides(size_t i) const;
    array<int,4> composition(size_t i) const;
    map<int,int> gaps(size_t) const;
    int informative(size_t pos) const;

    /* Get alignment pattern
     * Rules:
     *   0: keep unknowns
     *   1: unknown becomes majority
     */
    string pattern(size_t pos, int rule = 0) const;
    string pattern_full(size_t pos) const;

    // Fill in missing data with nearest neighbor
    void augment_nn(size_t min_overlap = 1000);

    /************************************************************************/

    void reverse(size_t i);
    void complement(size_t i);
    inline void reverse_complement(size_t i) { complement(i); reverse(i); }
    void reverse_complement();

    /************************************************************************/

    inline string operator[](size_t i) const {
      if (i < name.size()) return name[i];
      else return "";
    }

    inline const char* operator()(size_t i) const {
      if (i < seq.size()) return seq[i];
      else return "";
    }

    inline char operator()(size_t i, size_t k) const {
      if (i < seq.size()) return get(i,k);
      else return '0';
    }

    inline size_t size() const { return n; }

    inline int length(size_t i) const {
      if (i < size()) {
        return faidx_seq_len(index,name[i].c_str());
      } else {
        return 0;
      }
    }

    inline int max_len() const {
      int m = 0;
      for (auto l: len) if (l > m) m = l;
      return m;
    }

    inline int max_full_len() const {
      int m = 0;
      for (size_t i = 0; i < size(); ++i) {
        int l = length(i);
        if (l > m) m = l;
      }
      return m;
    }

    inline size_t find(string nm) const {
      auto f = std::find(name.begin(),name.end(),nm);
      return distance(name.begin(),f);
    }

    /************************************************************************/

    string filename = "";
    faidx_t* index = NULL;
    size_t n = 0;

    vector<string> name;
    vector<char*> seq;
    vector<size_t> beg;
    vector<int> len;
    vector<bool> mark;
};

#endif

