#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "Sequence.h"

#include "cxxopts.hpp"

int parse_index(string filename, vector<size_t>& index, vector<string>& name) 
{
  ifstream fasta(filename.c_str());
  cerr << "Parsing indices..." << flush;
  char c;
  size_t j = 0;
  bool first = true;
  while (fasta.get(c)) {
    //++i;
    if (c == '>') {
      name.push_back("");
      first = true;
      //++j;
      while (fasta.get(c)) {
        //++i;
        if (c == '\n') {
          index.push_back(fasta.tellg());
          break;
        } else if (c == ' ') {
          first = false;
        }
        if (first) name.back() += c;
      }
    }
  }
  fasta.close();
  cerr << "done. " << index.size() << " sequences found in file." << endl;
  return j;
}

char get_next(ifstream& f) {
  char c;
  do { 
    f.get(c); 
  } while ((c == '\n' || c == '\r') && ! f.eof());
  if (f.eof()) return '>';
  else return c;
}

pair<double,size_t> 
pairwise_distance(string filename, int a, int b) 
{
  ifstream fa(filename.c_str());
  ifstream fb(filename.c_str());

  fa.seekg(a);
  fb.seekg(b);

  string strA;
  string strB;

  double dist = 0;
  size_t total = 0;
  size_t seq_len = 0;

  char ca = 'n';
  char cb = 'n';

  while (ca != '>' && cb != '>') {
    ca = toupper(get_next(fa));
    cb = toupper(get_next(fb));
    if (ca != '>' && cb != '>' && ca != 'N' && cb != 'N' && ca != '-' && cb != '-') {
      if (ca != cb) dist += 1.0;
      total++;
    }
    ++seq_len;
  }

  fa.close();
  fb.close();

  return make_pair(dist,total);
}

int main(int argc, char** argv) {
  cxxopts::Options options("dna_stats", "Calucluate alignment statistics");

  options.add_options()
    ("f,filename", "Input file", cxxopts::value<string>())
    ("v,verbose",  "Verbose")
  ;

  options.parse(argc, argv);

  vector<size_t> index;
  vector<string> name;

  string filename = options["filename"].as<string>();
  parse_index(filename,index,name);

  // double penalty = (argc > 2) ? atof(argv[2]) : 0.0;

#pragma omp parallel for
  for (size_t i = 0; i < index.size(); ++i) {
    for (size_t j = 0; j < i; ++j) {
      pair<double,size_t> cnt = pairwise_distance(filename,index[i],index[j]);
#pragma omp critical
      {
        cout << name[i] << " " << name[j] << " " << " " << cnt.first << " " << cnt.second << " ";
        if (cnt.second > 0) cout << 1.0*cnt.first/cnt.second << endl;
        else cout << "NA" << endl;
      }
    }
#pragma omp critical
    cout << name[i] << " " << name[i] << " " << 0 << " " << 0 << " " << 0.0 << endl;
  }

  return 0;
}

