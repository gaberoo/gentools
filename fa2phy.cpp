#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0], "Convert FASTA to PHYLIP format");

  options.add_options()
    ("f,filename", "Input file", cxxopts::value<string>())
    ("i,index",    "Index file", cxxopts::value<string>()->default_value(""))
    ("O,format",   "Output format", cxxopts::value<string>()->default_value("phylip"))
    ("v,verbose",  "Verbose")
    ("h,help",     "Print help")
  ;

  vector<string> _pos = { "filename", "index" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) { cout << options.help({ "" }) << endl; exit(0); }

  string filename = options["filename"].as<string>();
  string index = options["index"].as<string>();
  string format = options["format"].as<string>();

  dna_tools::FastaFile f(filename,index);

  if (format == "phylip") {
    f.read_seqs();

    int first = 1;
    int block_size = 50;
    size_t j = 0;

    size_t ml = f.max_len();
    cout << f.size() << " " << ml << endl;

    while (j < f.max_len()) {
      for (size_t i = 0; i < f.size(); ++i) {
        if (first) cout << i << " ";
        for (size_t k = j; k < ml && k < j+block_size; ++k) {
          cout << f[i][k];
        }
        cout << endl;
      }
      if (first) first = 0;
      cout << endl;
      j += block_size;
    }
  } else if (format == "fastme") {
    size_t ml = f.max_len();
    printf(" %lu %lu\n",f.size(),ml);
    for (size_t i = 0; i < f.size(); ++i) {
      printf("%10s ", f[i].name().substr(0,9).c_str());
      for (size_t k = 0; k < ml; ++k) printf("%c",f[i][k]);
      printf("\n");
    }
  }

  return 0;
}

