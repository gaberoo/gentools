#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
using namespace std;

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

void dirichlet_gibbs(int k, int n, int p, const unsigned* X,
                     const double* a, const double* b,
                     gsl_rng* rng, size_t maxit, size_t thin,
                     string prefix)
{
  cerr << "Gibbs sampler for Dirichlet Multinomial" << endl;
  cerr << "=======================================" << endl;
  cerr << endl;
  cerr << "Number of groups:   k = " << k << endl;
  cerr << "Number of species:  n = " << n << endl;
  cerr << "Number of samples:  p = " << p << endl;

  ofstream out_mix((prefix + "_mix.txt").c_str());
  ofstream out_theta((prefix + "_theta.txt").c_str());

  int thinit = maxit/thin;

  int* mix = new int[p];
  double* theta = new double[k*n];
  double* phi = new double[k];
  double* alpha = new double[n];
  double* lprob = new double[k];
  int* NX = new int[n*k];
  int* mix_cnt = new int[k];

  memset(mix_cnt,0,k*sizeof(int));

  // set initial mixture
  for (int l = 0; l < p; ++l) {
    mix[l] = gsl_rng_uniform_int(rng,k);
    mix_cnt[mix[l]]++;
  }

  out_mix << setw(8) << -1 << " ";
  for (int l = 0; l < p; ++l) {
    out_mix << setw(5) << mix[l] << " ";
  }
  out_mix << endl;

  for (size_t it = 0; it < maxit; ++it) {
    // sample phi
    for (int i = 0; i < k; ++i) alpha[i] = b[i] + mix_cnt[i];
    gsl_ran_dirichlet(rng,k,alpha,phi);

    // get total mixture counts
    memset(NX,0,n*k*sizeof(double));
    for (int l = 0; l < p; ++l) {
      int K = mix[l];
      for (int m = 0; m < n; ++m) {
        NX[K*n+m] += X[l*n+m];
      }
    }

    // get new thetas
    for (int i = 0; i < k; ++i) {
      for (int m = 0; m < n; ++m) alpha[m] = a[m] + NX[i*n+m];
      gsl_ran_dirichlet(rng,n,alpha,theta+i*n);
    }

    // get new theta
    memset(alpha,0,n*sizeof(double));
    for (int l = 0; l < p; ++l) {
      for (int i = 0; i < k; ++i) {
        alpha[i] = gsl_ran_multinomial_lnpdf(n,theta+i*n,X+l*n);
      }

      double max_alpha = -INFINITY;
      for (int i = 0; i < k; ++i) {
        max_alpha = GSL_MAX(max_alpha,alpha[i]);
      }
      // cerr << l << " " << max_alpha << endl;

      alpha[0] = phi[0]*exp(alpha[0]-max_alpha);
      for (int i = 1; i < k; ++i) {
        alpha[i] = alpha[i-1] + phi[i]*exp(alpha[i]-max_alpha);
      }

      double r = alpha[k-1]*gsl_rng_uniform(rng);
      int i = 0;
      while (r > alpha[i] && i < k) { ++i; }
      mix[l] = i;
    }

    // get mixture counts
    memset(mix_cnt,0,p*sizeof(int));
    for (int l = 0; l < p; ++l) {
      mix_cnt[mix[l]]++;
    }

    if (it % thin == 0) {
      out_mix << setw(8) << it << " ";
      for (int l = 0; l < p; ++l) {
        out_mix << setw(5) << mix[l] << " ";
      }
      out_mix << endl;

      out_theta << setw(8) << it << " ";
      for (int i = 0; i < k; ++i) {
        for (int m = 0; m < n; ++m) {
          out_theta << setw(12) << theta[i*n+m] << " ";
        }
      }
      out_theta << endl;
    }
  }

  delete[] phi;
  delete[] mix;
  delete[] mix_cnt;
  delete[] theta;
  delete[] alpha;
  delete[] lprob;
  delete[] NX;
}

