#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <vector>
#include <array>
#include <map>
using namespace std;

#include "cxxopts.hpp"

#include "duo.h"
#include "hts.h"
#include "FaIdx.h"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>

/****************************************************************************/

size_t mat2lin(size_t i, size_t j, size_t n) {
  if (i > j) { size_t k = i; i = j; j = k; }
  return (n*(n-1)/2) - (n-i)*((n-i)-1)/2 + j - i-1;
}

array<size_t,2> lin2mat(size_t k, size_t n) {
  array<size_t,2> ij;
  ij[0] = n-2 - floor(sqrt(-8*k + 4*n*(n-1)-7)/2.0 - 0.5);
  ij[1] = k+ij[0]+1 - n*(n-1)/2 + (n-ij[0])*((n-ij[0])-1)/2;
  return ij;
}

/****************************************************************************/

pair<double,size_t>
correlation(const FaIdx& index1, const FaIdx& index2, 
            cxxopts::Options& options,
            const vector<size_t>& pos1, const vector<size_t>& pos2,
            double* work);

pair<double,size_t> 
spearman(double* work, size_t N);

pair<double,size_t> 
spearman_shuffle(gsl_rng* rng, size_t n, double* work, size_t N);

/****************************************************************************/

vector<string> names;
vector<size_t> seq1;
vector<size_t> seq2;

/****************************************************************************/

int main(int argc, char** argv) {
  string fn1 = "";
  string fn2 = "";
  size_t seed = time(NULL);
  size_t reps = 10;
  size_t mantel_reps = 10;

  cxxopts::Options options(argv[0], "Correlation between two FASTA files");

  options.positional_help("[options] <input1.fasta[.gz]> <input2.fasta[.gz]");
  options.add_options()
    ("1,fn1", "Input file 1", cxxopts::value<string>(fn1), "FILE")
    ("2,fn2", "Input file 2", cxxopts::value<string>(fn2), "FILE")
    ("s,seed", "RNG seed", cxxopts::value<size_t>(seed))
    ("r,reps", "Number of bootstraps", cxxopts::value<size_t>(reps))
    ("M,mantel", "Number of Mantel reps", cxxopts::value<size_t>(mantel_reps))
    ("m,overlap", "Minimum overlap", cxxopts::value<size_t>()->default_value("1000"))
    ("names", "Names of sequences to include", cxxopts::value<string>()->default_value(""))
    ("h,help", "Show help")
    ("v,verbose", "Verbose")
  ;

  vector<string> _pos = { "fn1", "fn2" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    return 1;
  }

  if (options.count("help")) {
    cout << options.help({ "" }) << endl;
    return 1;
  }

  /**************************************************************************/

  gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);
  gsl_rng_set(rng,seed);

  FaIdx index1(fn1);
  FaIdx index2(fn2);

  if (options.count("names")) {
    string nm_fn = options["names"].as<string>();
    ifstream in(nm_fn.c_str());
    string name;
    while (in >> name) { names.push_back(name); }
  } else {
    names.assign(index1.name.begin(),index1.name.end());
  }

  for (size_t i = 0; i < names.size(); ++i) {
    auto itA = find(index1.name.begin(),index1.name.end(),names[i]);
    auto itB = find(index2.name.begin(),index2.name.end(),names[i]);

    int dA = distance(index1.name.begin(),itA);
    int dB = distance(index2.name.begin(),itB);

    if (options.count("verbose") >= 2) cerr << names[i] << " " << dA << " " << dB << endl;

    if (itA != index1.name.end() && itB != index2.name.end()) {
      seq1.push_back(dA);
      seq2.push_back(dB);
    }
  }

  names.clear();
  for (auto i: seq1) { names.push_back(index1.name[i]); }

  size_t n = seq1.size();
  size_t N = n*(n-1)/2;
  double* work = new double[6*N];

  if (options.count("verbose")) cerr << "Loading sequences..." << flush;
  index1.load_all();
  index2.load_all();

  if (options.count("verbose")) cerr << "done. Calculating correlation..." << flush;

  vector<size_t> pos1(index1.max_len(),0);
  vector<size_t> pos2(index2.max_len(),0);

  pair<double,size_t> rho;

  iota(pos1.begin(),pos1.end(),0);
  iota(pos2.begin(),pos2.end(),0);

  rho = correlation(index1,index2,options,pos1,pos2,work);

  if (options.count("verbose")) cerr << "done. Performing permutations..." << flush;

  cout << 0 << "\t" << rho.second << " " << rho.first;
  for (size_t m = 0; m < mantel_reps; ++m) {
    auto r2 = spearman_shuffle(rng,n,work,N);
    cout << "\t" << r2.second << " " << r2.first;
  }
  cout << endl;

  if (options.count("verbose")) cerr << "done." << endl;

  for (size_t r = 1; r <= reps; ++r) {
    if (options.count("verbose")) cerr << "[" << r << "] Sampling positions..." << flush;

    for (size_t i = 0; i < pos1.size(); ++i) pos1[i] = gsl_rng_uniform_int(rng,pos1.size());
    sort(pos1.begin(),pos1.end());

    for (size_t i = 0; i < pos2.size(); ++i) pos2[i] = gsl_rng_uniform_int(rng,pos2.size());
    sort(pos2.begin(),pos2.end());

    if (options.count("verbose")) cerr << "done. Calculating correlation..." << flush;
    rho = correlation(index1,index2,options,pos1,pos2,work);
    if (options.count("verbose")) cerr << "done. Performing permutations..." << flush;

    cout << r << "\t" << rho.second << " " << rho.first;
    for (size_t i = 0; i < mantel_reps; ++i) {
      auto r2 = spearman_shuffle(rng,n,work,N);
      cout << "\t" << r2.second << " " << r2.first;
    }
    cout << endl;

    if (options.count("verbose")) cerr << "done." << endl;
  }

  delete[] work;
  gsl_rng_free(rng);

  return 0;
}

/****************************************************************************/

pair<double,size_t>
correlation(const FaIdx& index1, const FaIdx& index2, 
            cxxopts::Options& options,
            const vector<size_t>& pos1, const vector<size_t>& pos2,
            double* work) 
{
  size_t min_overlap = options["overlap"].as<size_t>();

  size_t n = (seq1.size() > 0) ? seq1.size() : index1.size();
  size_t N = n*(n-1)/2;

  // Pairwise distances for FASTA A
#pragma omp parallel for shared(index1,work,seq1,seq2)
  for (size_t k = 0; k < N; ++k) {
    array<size_t,2> ij(lin2mat(k,n));
    pair<size_t,size_t> cnt(index1.pairwise_distance_pos(seq1[ij[0]],seq1[ij[1]],pos1));
    work[k] = (cnt.second > min_overlap) ? 1.0*cnt.first/cnt.second : -1.0;
  }
  if (options.count("verbose")) fprintf(stderr,"1) %6lu/%6lu\n",N,N);

  // Pairwise distances for FASTA B
#pragma omp parallel for shared(index2,work,seq1,seq2)
  for (size_t k = 0; k < N; ++k) {
    array<size_t,2> ij(lin2mat(k,n));
    pair<size_t,size_t> cnt(index2.pairwise_distance_pos(seq2[ij[0]],seq2[ij[1]],pos2));
    work[k+N] = (cnt.second > min_overlap) ? 1.0*cnt.first/cnt.second : -1.0;
  }
  if (options.count("verbose")) fprintf(stderr,"2) %6lu/%6lu\n",N,N);

  return spearman(work,N);
}

/****************************************************************************/

pair<double,size_t>
spearman(double* work, size_t N) 
{
  size_t l = 0;
#pragma omp parallel for shared(work,l)
  for (size_t k = 0; k < N; ++k) {
    if (work[k] >= 0.0 && work[k+N] >= 0.0) {
#pragma omp critical
      {
        work[l+2*N] = work[k];
        work[l+3*N] = work[k+N];
        ++l;
      }
    }
  }

  double r = -2.0;
  if (l > 0) r = gsl_stats_spearman(work+2*N,1,work+3*N,1,l,work+4*N);

  return make_pair(r,l);
}

/****************************************************************************/

pair<double,size_t>
spearman_shuffle(gsl_rng* rng, size_t n, double* work, size_t N) 
{
  size_t l = 0;

  vector<size_t> s(n);
  iota(s.begin(),s.end(),0);
  gsl_ran_shuffle(rng,s.data(),s.size(),sizeof(size_t));

#pragma omp parallel for shared(work,l)
  for (size_t i = 0; i < n; ++i) {
    for (size_t j = i+1; j < n; ++j) {
      size_t k1 = mat2lin(i,j,n);
      size_t k2 = mat2lin(s[i],s[j],n);
      if (work[k1] >= 0.0 && work[k2+N] >= 0.0) {
#pragma omp critical
        {
          work[l+2*N] = work[k1];
          work[l+3*N] = work[k2+N];
          ++l;
        }
      }
    }
  }

  double r = -2.0;
  if (l > 0) r = gsl_stats_spearman(work+2*N,1,work+3*N,1,l,work+4*N);

  return make_pair(r,l);
}
