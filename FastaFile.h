#ifndef __FASTAFILE_H__
#define __FASTAFILE_H__

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <numeric>
using namespace std;

#include <sys/stat.h>

#include "Sequence.h"

template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {
  // initialize original index locations
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) { return v[i1] < v[i2]; });

  return idx;
}

typedef struct { size_t cnts[4]; } basecnt_t;

namespace dna_tools {

  inline double plogp(double x) { return (x==0.0) ? 0.0 : -x*log(x); }

  inline char pmax(double* p) {
    char r = 'n';
    double max = p[0] + p[5];
    if (p[1] > max) { max = p[1]; r = 'A'; }
    if (p[2] > max) { max = p[2]; r = 'G'; }
    if (p[3] > max) { max = p[3]; r = 'C'; }
    if (p[4] > max) { max = p[4]; r = 'T'; }
    return r;
  }

  class FastaFile {
    public:
      FastaFile(string fasta = "", string index = "") 
        : _fn(fasta)
      { 
        if (index == "") {
          string idx = fasta + ".fai";
          struct stat buffer; 
          if (stat(idx.c_str(),&buffer) == 0) index = idx;
        }

        if (index != "") read_index(index);

        _in.open(_fn.c_str());

        if (index == "") build_index();
      }

      virtual ~FastaFile() { if (_in.is_open()) _in.close(); }

      int read_index(string filename);
      int build_index();

      inline size_t size() const { return _seq.size(); }
      inline Sequence& operator[](int i) { return _seq[i]; }
      inline const Sequence& operator[](int i) const { return _seq[i]; }

      string consensus() const;

      size_t max_len() const;
      void read_seqs();

      char get(int id, int pos) const;
      string diversity(size_t pos, double* p) const;
      int fdiversity(size_t pos, double* p);
      int count(size_t pos) const;

      void subseqs(vector<string>& seqs, vector<int>& pos, double min_div = 0.0);
      void minOverlap(vector<string>& seqs, vector<int>& pos, int min = 2);

      void allDiv(vector<double>& div, vector<int>& cnt);
      void maxFreq(vector<double>& freqs, vector<int>& cnt);
      void baseCounts(vector<basecnt_t>& cnts) const;

      char major(int pos) const;

      pair<double,uint64_t> 
      pairwise_distance(size_t a, size_t b) const;

      string _fn;
      ifstream _in;

      vector<Sequence> _seq;
  };
}

#endif

