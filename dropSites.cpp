#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#include <string>
using namespace std;

#include "FastaFile.h"
#include "Sequence.h"

#include "cxxopts.hpp"

#include <gsl/gsl_rng.h>

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0], "Drop uninformative sites from a multiple alignment");

  string action;
  string filename;
  string index = "";
  size_t keep_sites = 1000;
  string out_format = "phylip";

  double min_div = 0.0;
  double min_freq = 0.0;
  int min_overlap = 2;

  options.add_options()
    ("a,action",   "Action", cxxopts::value<string>(action))
    ("f,filename", "Input file", cxxopts::value<string>(filename))
    ("i,index",    "Index file", cxxopts::value<string>(index))
    ("d,minDiv",   "Minimum diversity", cxxopts::value<double>(min_div))
    ("p,minFreq",  "Minimum frequency", cxxopts::value<double>(min_freq))
    ("O,outFmt",   "Output format", cxxopts::value<string>(out_format))
    ("v,verbose",  "Verbose")
    ("h,help",     "Print help")
  ;

  options.add_options("overlap")
    ("minOverlap", "Minimum number of informative sequences", cxxopts::value<int>(min_overlap))
  ;

  options.add_options("random")
    ("keep", "How many sites to keep", cxxopts::value<size_t>(keep_sites))
  ;

  vector<string> _pos = { "action", "filename" };
  options.parse_positional(_pos);

  try {
    options.parse(argc, argv);
  } catch (const cxxopts::OptionException& e) {
    cout << "error parsing options: " << e.what() << endl;
    exit(1);
  }

  if (options.count("help")) {
    vector<string> show_help = { "", "overlap", "random" };
    if (options.count("action")) {
      show_help.push_back(options["action"].as<string>());
    }
    cout << options.help(show_help) << endl;
    exit(0);
  }

  int verbose = options.count("verbose");

  dna_tools::FastaFile f(filename,index);
  if (verbose) cerr << "Reading sequences...";
  f.read_seqs();
  if (verbose) cerr << "done." << endl;

  vector<string> subseq;
  vector<int> order;

  if (action == "overlap") {
    if (verbose) cerr << "Dropping sites with fewer than " << min_overlap << " sequences." << endl;
    f.minOverlap(subseq,order,min_overlap);
    if (verbose) cerr << "Retained sites: " << order.size() << "/" << f.max_len() << endl;
  } else if (action == "random") {
    subseq.resize(f.size());
    size_t n = f.max_len();

    gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);
    gsl_rng_set(rng,time(NULL));

    while (subseq[0].length() < keep_sites) {
      size_t k = gsl_rng_uniform_int(rng,n);
      for (size_t i = 0; i < f.size(); ++i) {
        subseq[i] += f[i][k];
      }
    }

    gsl_rng_free(rng);
  } else {
    // filter the alignment for informative sites
    f.subseqs(subseq,order,min_div);
  }

  if (out_format == "fasta") {
    for (size_t i = 0; i < subseq.size(); ++i) {
      cout << ">" << f[i].name() << endl;
      cout << subseq[i] << endl;
    }
  } else {
    cout << subseq.size() << " " << subseq[0].size() << endl;

    int first = 1;
    int block_size = 50;
    size_t j = 0;

    while (j < subseq[0].size()) {
      for (size_t i = 0; i < subseq.size(); ++i) {
        // if (first) cout << i << " ";
        cout << i << " ";
        cout << subseq[i].substr(j,block_size) << endl;
      }
      if (first) first = 0;
      cout << endl;
      j += block_size;
    }
  }

  // cout << f.get(0,4) << endl;
  // cout << f.get(1,4) << endl;

  // vector<dna_tools::Sequence> seq;
  // f.to_seq(seq);

  /*
  for (size_t i = 0; i < seq.size(); ++i) {
    seq[i].pad_seq(10);
    cout << seq[i].to_str() << endl;
  }
  */

  return 0;
}

