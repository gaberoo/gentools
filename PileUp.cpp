#include "PileUp.h"

/****************************************************************************/

int sum1(int* alleles) {
 return alleles[0]+alleles[1]+alleles[2]+alleles[3];
}

int max1(int* alleles) {
  return max(max(alleles[0],alleles[1]),max(alleles[2],alleles[3]));
}

int cnt1(int* alleles) { return sum1(alleles)-max1(alleles); }

int seg1(int* alleles) { 
  return (alleles[0]>0)+(alleles[1]>0)+(alleles[2]>0)+(alleles[3]>0);
}

void add1(int* a1, const int* a2) { 
  for (size_t i = 0; i < 4; ++i) a1[i] += a2[i];
}

void norm1(double* p, int* a) {
  int tot = sum1(a);
  for (size_t i = 0; i < 4; ++i) p[i] = 1.0*a[i]/tot;
}

/****************************************************************************/

istream& operator>>(istream& in, PileUpEntry& entry) {
  return entry.parse(in);
}

/****************************************************************************/

void PileUpEntry::parse(string line, bool mapQual) {
  istringstream in(line);
  parse(in,mapQual);
}

/****************************************************************************/

istream& PileUpEntry::parse(istream& in, bool mapQual) {
  in >> name >> pos >> ref >> depth >> match >> baseQual;
  if (mapQual) in >> mapQual;
  packed = 1;
  return in;
}

/****************************************************************************/

size_t Variant::parse_indel(size_t pos, string match) {
  string insert = "";
  size_t start = pos;

  while (isdigit(match[pos++]));
  int len = atoi(match.substr(start,pos).c_str());
  start = pos;
  pos += len-1;

  type = PU_INDEL;
  indel = match.substr(start,pos);

  return pos;
}

/****************************************************************************/

int PileUpEntry::num_alleles(int min_qual) {
  unpack();

  int alleles = 0;
  for (const auto& v: variants) {
    if (v.type == PU_ALLELE) {
      switch (v.base) {
        case 'A': alleles |= 1; break;
        case 'C': alleles |= 2; break;
        case 'T': alleles |= 4; break;
        case 'G': alleles |= 8; break;
          break;

        default:
          break;
      }
    }
  }
  return ((alleles & 1) > 0) + ((alleles & 2) > 0) \
       + ((alleles & 4) > 0) + ((alleles & 8) > 0);
}

/****************************************************************************/

void PileUpEntry::cnt(int* alleles, int min_qual) {
  unpack();
  for (const auto& v: variants) {
    if (v.type == PU_ALLELE && v.qual >= min_qual) {
      switch (v.base) {
        case 'A': alleles[0]++; break;
        case 'C': alleles[1]++; break;
        case 'T': alleles[2]++; break;
        case 'G': alleles[3]++; break;
        default: break;
      }
    }
  }
}

/****************************************************************************/

int PileUpEntry::is_segregating(int min_qual) {
  unpack();
  int alleles[] = { 0, 0, 0, 0 };
  cnt(alleles,min_qual);
  return seg1(alleles);
}

/****************************************************************************/

int PileUpEntry::max_cnt(int min_qual) {
  unpack();
  int alleles[] = { 0, 0, 0, 0 };
  cnt(alleles,min_qual);
  return cnt1(alleles);
}

/****************************************************************************/

double PileUpEntry::max_freq(int min_qual) {
  unpack();
  int alleles[] = { 0, 0, 0, 0 };
  for (const auto& v: variants) {
    if (v.type == PU_ALLELE) {
      switch (v.base) {
        case 'A': alleles[0]++; break;
        case 'C': alleles[1]++; break;
        case 'T': alleles[2]++; break;
        case 'G': alleles[3]++; break;
          break;

        default:
          break;
      }
    }
  }
  int sum = alleles[0]+alleles[1]+alleles[2]+alleles[3];
  int max_cnt = max(max(alleles[0],alleles[1]),max(alleles[2],alleles[3]));
  return 1.0*max_cnt/sum;
}

/****************************************************************************/

void PileUpEntry::unpack() {
  if (packed > 0) {
    variants.clear();

    size_t pos = 0;
    size_t qpos = 0;
    string indel;

    while (pos < match.length()) {
      switch (match[pos]) {
        case '.': case ',':
          variants.push_back(Variant(ref));
          variants.back().qual = baseQual[qpos++]-phred;
          ++pos;
          break;

        case 'A': case 'a':
        case 'G': case 'g':
        case 'T': case 't':
        case 'C': case 'c':
          variants.push_back(Variant(match[pos++]));
          variants.back().qual = baseQual[qpos++]-phred;
          break;

        case '+':
        case '-':
          ++pos;
          variants.push_back(Variant(PU_INDEL));
          pos = variants.back().parse_indel(pos,match);
          break;

        case '^':
          pos += 2;
          break;
          
        case '$':
        case '*':
        default:
          ++pos;
          break;
      }
    }

    packed = 0;
  }
}

/****************************************************************************/

ostream& operator<<(ostream& out, const PileUpEntry& entry) {
  out << entry.name << ", "
      << entry.pos << ", "
      << entry.ref << ", "
      << entry.depth << ", "
      << entry.match << ", "
      << entry.baseQual << ", "
      << entry.mapQual << endl;

  for (auto& v: entry.variants) {
    out << "  " << v.type << " > " << v.qual << ": " << v.base << "|" << v.indel << endl;
  }

  return out;
}

/****************************************************************************/

void MPileUp::addEntry(string name, int group) {
  push_back(PileUpEntry());
  back().phred = phred;
  names.push_back(name);
  groups.push_back(group);
}

/****************************************************************************/

void MPileUp::parse(string line, bool mapQual, bool grow) {
  istringstream in(line);
  size_t i = 0;

  string name;
  int pos;
  char ref;

  in >> name >> pos >> ref;

  while (in) {
    if (i >= size()) {
      if (grow) addEntry();
      else break;
    } 

    (*this)[i].pos = pos;
    (*this)[i].ref = ref;

    in >> (*this)[i].depth 
       >> (*this)[i].match
       >> (*this)[i].baseQual
       >> (*this)[i].mapQual;

    (*this)[i].packed = 1;

    ++i;
  }
}

/****************************************************************************/

int MPileUp::depth() const {
  int d = 0;
  for (size_t i = 0; i < size(); ++i) d += (*this)[i].depth;
  return d;
}

/****************************************************************************/

int MPileUp::max_cnt(int min_qual) {
  int alleles[] = { 0, 0, 0, 0 };
  vector<PileUpEntry>::iterator entry = begin();
  while (entry != end()) {
    entry->unpack();
    (entry++)->cnt(alleles,min_qual);
  }
  return cnt1(alleles);
}

/****************************************************************************/

int MPileUp::is_segregating(int min_qual) {
  int alleles[] = { 0, 0, 0, 0 };
  vector<PileUpEntry>::iterator entry = begin();
  while (entry != end()) {
    entry->unpack();
    (entry++)->cnt(alleles,min_qual);
  }
  return seg1(alleles);
}


