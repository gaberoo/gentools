/*
 * Generate contig correlation graph from depth data
 *
 * Current input type supported:
 *   1) MetaBAT2 depth file
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <regex>
#include <iomanip>
#include <ctime>
using namespace std;

#include <gsl/gsl_statistics_double.h>

#include "extract_length.h"
#include "pretty_bytes.h"

inline double get_secs(clock_t begin, clock_t end) {
  return 1.0*(end-begin)/CLOCKS_PER_SEC;
}

int main(int argc, char** argv) {
  istream& input = cin;
  string token;

  map<string, size_t> colid;
  vector<string> header;
  vector<bool> is_sample;
  vector<string> samples;

  vector<string> contig_names;
  vector<size_t> contig_lengths;
  vector<double> contig_avg_depth;

  vector<double> cov;

  regex sample_re("([\\w-_]+)\\.bam$");
  smatch match;

  size_t min_len = 2000;
  double graph_cutoff = 0.6;

  enum { SILENT, INFO, WARNING, ERROR } verbose = ERROR;

  // Read header
  while (input.peek() != '\n' && cin >> token) {
    header.push_back(token);

    if (regex_match(token, match, sample_re)) {
      samples.push_back(match[1]);
      is_sample.push_back(true);
      colid[match[1]] = header.size()-1;
    } else {
      colid[token] = header.size()-1;
      is_sample.push_back(false);
    }
  }

  size_t nsamp = samples.size();
  if (verbose >= INFO) cerr << "Found " << nsamp << " samples." << endl;

  string cname = "";
  size_t clen = 0;
  double cavg = 0.0;

  double depth = 0.0;
  double dvar = 0.0;

  size_t ncontigs = 0;
  string line;

  cin.get();

  while (getline(input,line)) {
    istringstream iss(line);
    iss >> cname >> clen >> cavg;

    if (clen >= min_len) {
      contig_names.push_back(cname);
      contig_lengths.push_back(clen);
      contig_avg_depth.push_back(cavg);

      cov.insert(cov.end(),nsamp,0.0);

      size_t i = 0;
      while (iss >> depth >> dvar) {
        cov[ncontigs*nsamp+i++] = depth;
      }

      if (verbose >= INFO) {
        cerr << ncontigs << " contigs. MEM usage approx. " 
             << pretty_bytes(1.0*nsamp*8*ncontigs)
             << "                        \r" << flush;
      }

      ++ncontigs;
    }
  }

  if (verbose >= INFO) cerr << endl;

  if (verbose >= INFO) cerr << "Calculating correlations..." << endl;

  const double* data = cov.data();

  size_t lastk = -1;
  #pragma omp parallel for shared(data,contig_names,lastk)
  for (size_t i = 1; i < ncontigs; ++i) {
    for (size_t j = 0; j < i; ++j) {
      double x = gsl_stats_correlation(data+i*nsamp,1,data+j*nsamp,1,nsamp);
      if (x >= graph_cutoff) {
        #pragma omp critical
        printf("%s\t%s\t%g\n",contig_names[i].c_str(),contig_names[j].c_str(),x);
      }

      #pragma omp critical
      if (verbose >= INFO) {
        if (i > lastk) {
          fprintf(stderr,"Contig %lu/%lu (%4.2f%%)         \r",
                  i+1,ncontigs,100.0*(i+1)/ncontigs);
          fflush(stderr);
          lastk = i;
        }
      }
    }
  }

  if (verbose >= INFO) fprintf(stderr,"\n");

  return 0;
}
